﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oPacker
{
    internal static class Class47
    {
        static byte[] smethod_143(byte[] byte_0)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (DeflateStream deflateStream = new DeflateStream((Stream)memoryStream, CompressionMode.Compress))
                    deflateStream.Write(byte_0, 0, byte_0.Length);
                return memoryStream.ToArray();
            }
        }

        static byte[] smethod_61(byte[] byte_0)
        {
            byte[] result;
            using (MemoryStream memoryStream = new MemoryStream(byte_0))
            {
                using (MemoryStream memoryStream2 = new MemoryStream())
                {
                    using (DeflateStream deflateStream = new DeflateStream(memoryStream, CompressionMode.Decompress))
                    {
                        deflateStream.CopyTo(memoryStream2);
                    }
                    result = memoryStream2.ToArray();
                }
            }
            return result;
        }

        static byte[] smethod_132(byte[] byte_0)
        {
            Func<byte, byte> arg_20_1;
            if ((arg_20_1 = Class47.Class48.func_1) == null)
            {
                arg_20_1 = (Class47.Class48.func_1 = new Func<byte, byte>(Class47.Class48.class48_0.method_1));
            }
            return byte_0.Select(arg_20_1).ToArray<byte>();
        }


        static byte[] smethod_34(byte[] byte_0)
        {
            Func<byte, byte> arg_20_1;
            if ((arg_20_1 = Class47.Class48.func_0) == null)
            {
                arg_20_1 = (Class47.Class48.func_0 = new Func<byte, byte>(Class47.Class48.class48_0.method_0));
            }
            return byte_0.Select(arg_20_1).ToArray<byte>();
        }

        internal sealed class Class48
        {
            public static readonly Class47.Class48 class48_0 = new Class47.Class48();

            public static Func<byte, byte> func_0;

            public static Func<byte, byte> func_1;

            internal byte method_0(byte byte_0)
            {
                return (byte)~byte_0;
            }

            internal byte method_1(byte byte_0)
            {
                return (byte)~byte_0;
            }
        }

        internal static byte[] Smethod_0(this byte[] byte_0) // compress
        {
            return smethod_34(smethod_143(smethod_34(smethod_143(byte_0))));
        }


        internal static byte[] Smethod_1(this byte[] byte_0)  // decompress
        {
            return smethod_61(smethod_132(smethod_61(smethod_132(byte_0))));
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            if(args.Count() < 1)
            {
                Console.WriteLine("Error:  Must supply an input file.");
                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine("Error:  {0} doesn't exist.");
            }

            var newfile = args[0] + ".aps";

            File.Delete(newfile);

            File.WriteAllBytes(newfile, Class47.Smethod_0(File.ReadAllBytes(args[0])));
        }
    }
}
