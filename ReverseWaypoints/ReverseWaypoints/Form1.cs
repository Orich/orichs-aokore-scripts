﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AOKore.Game;
using System.IO.Compression;

namespace ReverseWaypoints
{
    internal static class Class47
    {
        static byte[] smethod_143(byte[] byte_0)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (DeflateStream deflateStream = new DeflateStream((Stream)memoryStream, CompressionMode.Compress))
                    deflateStream.Write(byte_0, 0, byte_0.Length);
                return memoryStream.ToArray();
            }
        }

        static byte[] smethod_61(byte[] byte_0)
        {
            byte[] result;
            using (MemoryStream memoryStream = new MemoryStream(byte_0))
            {
                using (MemoryStream memoryStream2 = new MemoryStream())
                {
                    using (DeflateStream deflateStream = new DeflateStream(memoryStream, CompressionMode.Decompress))
                    {
                        deflateStream.CopyTo(memoryStream2);
                    }
                    result = memoryStream2.ToArray();
                }
            }
            return result;
        }

        static byte[] smethod_132(byte[] byte_0)
        {
            Func<byte, byte> arg_20_1;
            if ((arg_20_1 = Class47.Class48.func_1) == null)
            {
                arg_20_1 = (Class47.Class48.func_1 = new Func<byte, byte>(Class47.Class48.class48_0.method_1));
            }
            return byte_0.Select(arg_20_1).ToArray<byte>();
        }


        static byte[] smethod_34(byte[] byte_0)
        {
            Func<byte, byte> arg_20_1;
            if ((arg_20_1 = Class47.Class48.func_0) == null)
            {
                arg_20_1 = (Class47.Class48.func_0 = new Func<byte, byte>(Class47.Class48.class48_0.method_0));
            }
            return byte_0.Select(arg_20_1).ToArray<byte>();
        }

        internal sealed class Class48
        {
            public static readonly Class47.Class48 class48_0 = new Class47.Class48();

            public static Func<byte, byte> func_0;

            public static Func<byte, byte> func_1;

            internal byte method_0(byte byte_0)
            {
                return (byte)~byte_0;
            }

            internal byte method_1(byte byte_0)
            {
                return (byte)~byte_0;
            }
        }

        internal static byte[] Smethod_0(this byte[] byte_0) // compress
        {
            return smethod_34(smethod_143(smethod_34(smethod_143(byte_0))));
        }


        internal static byte[] Smethod_1(this byte[] byte_0)  // decompress
        {
            return smethod_61(smethod_132(smethod_61(smethod_132(byte_0))));
        }

    }



    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class PathData
        {
            public Vector3[] Path { get; set; }
        }

        public T FromXml<T>(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }

        public static string ToXml(object obj)
        {
            XmlSerializer s = new XmlSerializer(obj.GetType());
            using (StringWriter writer = new StringWriter())
            {
                s.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                string csharp = String.Empty;
                try
                {
                    var fileData = File.ReadAllText(file);

                    var xmlWP = FromXml<PathData>(fileData);

                    Console.Write("Length = " + xmlWP.Path.Count().ToString());

                    var pathArray = xmlWP.Path;

                    Array.Reverse(pathArray);

                    var rfilename = file + ".reversed";
                    var cfilename = file + "_cs";

                    if (File.Exists(rfilename))
                        File.Delete(rfilename);

                    if (File.Exists(cfilename))
                        File.Delete(cfilename);

                    //File.Create(rfilename);

                    var newPath = new PathData() { Path = pathArray };
                    var newxmlWP = ToXml(newPath);

                    File.WriteAllText(rfilename, newxmlWP);

                    foreach (var pt in xmlWP.Path)
                    {
                        csharp += String.Format("Path.Add(new oVector({0}f, {1}f, {2}f, false));\r\n", pt.X, pt.Y, pt.Z);
                    }

                    File.WriteAllText(cfilename, csharp);
                }
                catch (IOException ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }

        public static void Compress(FileInfo fi)
        {
            // Get the stream of the source file. 
            using (FileStream inFile = fi.OpenRead())
            {
                // Prevent compressing hidden and already compressed files. 
                if ((File.GetAttributes(fi.FullName) & FileAttributes.Hidden)
                    != FileAttributes.Hidden & fi.Extension != ".cmp")
                {
                    // Create the compressed file. 
                    using (FileStream outFile =
                            File.Create(fi.FullName + ".cmp"))
                    {
                        using (DeflateStream Compress =
                            new DeflateStream(outFile,
                            CompressionMode.Compress))
                        {
                            // Copy the source file into  
                            // the compression stream.
                            byte[] buffer = new byte[4096];
                            int numRead;
                            while ((numRead = inFile.Read(buffer,
                                    0, buffer.Length)) != 0)
                            {
                                Compress.Write(buffer, 0, numRead);
                            }
                            Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                            fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                        }
                    }
                }
            }
        }

        public static void Decompress(FileInfo fi)
        {
            // Get the stream of the source file. 
            using (FileStream inFile = fi.OpenRead())
            {
                // Get original file extension,  
                // for example "doc" from report.doc.cmp.
                string curFile = fi.FullName;
                string origName = curFile + ".unpacked";
                //string origName = curFile.Remove(curFile.Length
                //        - fi.Extension.Length);

                //Create the decompressed file. 
                using (FileStream outFile = File.Create(origName))
                {
                    using (DeflateStream Decompress = new DeflateStream(inFile,
                        CompressionMode.Decompress))
                    {
                        //Copy the decompression stream into the output file.
                        byte[] buffer = new byte[4096];
                        int numRead;
                        while ((numRead =
                            Decompress.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            outFile.Write(buffer, 0, numRead);
                        }
                        Console.WriteLine("Decompressed: {0}", fi.Name);
                    }
                }
            }
        }

        private void button2_click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            string file = openFileDialog1.FileName;

            if (result == DialogResult.OK) // Test result.
            {
                var fileData = File.ReadAllBytes(file);

                var rfilename = file + ".packed";

                File.WriteAllBytes(rfilename, Class47.Smethod_0(fileData));
            }
        }

        private void decompress_click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            string file = openFileDialog1.FileName;

            if (result == DialogResult.OK) // Test result.
            {
                var fileData = File.ReadAllBytes(file);

                var rfilename = file + ".unpacked";

                File.WriteAllBytes(rfilename, Class47.Smethod_1(fileData));
            }
        }


    }
}
