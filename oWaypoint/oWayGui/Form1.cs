﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO.Pipes;
using System.IO;

namespace oWayGui
{
    public partial class oWaypoint : Form
    {
        public oWaypoint()
        {
            InitializeComponent();

            recordButton.Enabled = false;
            removeButton.Enabled = false;

            log = new BindingList<string>();

            debugListbox.DataSource = log;

            _pipe = new NamedPipeServerStream("oWaypoint", PipeDirection.InOut);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e) // record
        {
            using (StreamWriter wrtr = new StreamWriter(_pipe, Encoding.ASCII))
            {
                wrtr.Write("RECORD");
                wrtr.Flush();
            }
            _pipe.WaitForPipeDrain();

        }

        private NamedPipeServerStream _pipe;
        private Thread _receiveThread;
        private BindingList<string> log;

        private void oWaypoint_Load(object sender, EventArgs e)
        {
            log.Add("oWaypoint Server Open.");

            _receiveThread = new Thread(new ThreadStart(receiveThread));
            _receiveThread.IsBackground = true;
            _receiveThread.Start();
        }

        private void receiveThread()
        {
            debugListbox.Invoke((MethodInvoker)delegate () { log.Add("Waiting for client to connect ..."); });

            _pipe.WaitForConnection();

            debugListbox.Invoke((MethodInvoker)delegate () { log.Add("Client Connection Established!"); });

            recordButton.Invoke((MethodInvoker)delegate () { recordButton.Enabled = true; });
            removeButton.Invoke((MethodInvoker)delegate () { removeButton.Enabled = true; });

            while(true)
            {
                using (StreamReader rdr = new StreamReader(_pipe, Encoding.ASCII))
                {
                    string message = rdr.ReadToEnd();
                    if (!string.IsNullOrEmpty(message))
                    {
                        debugListbox.Invoke((MethodInvoker)delegate () { log.Add(String.Format("[Received, Size={0}] {1}", message.Length, message)); });
                    }
                }

            }
        }

        private void oWaypoint_Leave(object sender, EventArgs e)
        {
            _pipe.Dispose();
        }
    }
}
