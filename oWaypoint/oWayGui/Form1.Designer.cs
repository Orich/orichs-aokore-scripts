﻿namespace oWayGui
{
    partial class oWaypoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wpListbox = new System.Windows.Forms.ListBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.recordButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.debugListbox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // wpListbox
            // 
            this.wpListbox.FormattingEnabled = true;
            this.wpListbox.Location = new System.Drawing.Point(12, 35);
            this.wpListbox.Name = "wpListbox";
            this.wpListbox.ScrollAlwaysVisible = true;
            this.wpListbox.Size = new System.Drawing.Size(311, 238);
            this.wpListbox.TabIndex = 0;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(181, 279);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(142, 36);
            this.removeButton.TabIndex = 1;
            this.removeButton.Text = "Remove Waypoint";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // recordButton
            // 
            this.recordButton.Location = new System.Drawing.Point(12, 279);
            this.recordButton.Name = "recordButton";
            this.recordButton.Size = new System.Drawing.Size(142, 36);
            this.recordButton.TabIndex = 2;
            this.recordButton.Text = "Record Waypoint";
            this.recordButton.UseVisualStyleBackColor = true;
            this.recordButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Waypoints";
            // 
            // debugListbox
            // 
            this.debugListbox.FormattingEnabled = true;
            this.debugListbox.Location = new System.Drawing.Point(10, 361);
            this.debugListbox.Name = "debugListbox";
            this.debugListbox.ScrollAlwaysVisible = true;
            this.debugListbox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.debugListbox.Size = new System.Drawing.Size(313, 108);
            this.debugListbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 336);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Debug / Log";
            // 
            // oWaypoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 481);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.debugListbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.recordButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.wpListbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "oWaypoint";
            this.Text = "oWaypoint";
            this.Load += new System.EventHandler(this.oWaypoint_Load);
            this.Leave += new System.EventHandler(this.oWaypoint_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox wpListbox;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button recordButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox debugListbox;
        private System.Windows.Forms.Label label2;
    }
}

