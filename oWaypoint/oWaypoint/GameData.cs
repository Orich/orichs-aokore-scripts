using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;

namespace Script
{

    public static class GameData
    {
        private static bool isonline = false;
        private static bool isknockeddown = false;
        private static bool ismounted = false;
        private static bool ismountcasting = false;
        private static bool ismountcasted = false;
        private static bool ismoving = false;
        private static float playerweight = 0f;
        private static bool isharvesting = false;
        private static bool isincity = false;
        private static bool isbankopen = false;
        private static Vector3 playerlocation = null;
        private static ItemInstance[] items = null;
        private static HarvestableInstance[] harvestables = null;
        private static Vector3 banklocation = null;
        private static MobInstance[] mobs = null;
        private static long? playerid = null;

        private static string territorytype = null;
        private static string territorytier = null;
        private static string instancetype = null;
        private static string mapname = null;

        public static string MapName
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { mapname = Map.Name; }); Thread.Sleep(10);
                return mapname;
            }
        }

        public static string InstanceType
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { instancetype = Map.InstanceType; }); Thread.Sleep(10);
                return instancetype;
            }
        }

        public static string TerritoryType
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { territorytype = Map.TerritoryType; }); Thread.Sleep(10);
                return territorytype;
            }
        }

        public static string TerritoryTier
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { territorytier = Map.TerritoryTier; }); Thread.Sleep(10);
                return territorytier;
            }
        }

        public static bool IsOnline
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isonline = (Client.ClientState.Contains("Connected") && Player.HasPlayer); }); Thread.Sleep(10);
                return isonline;
            }
        }

        public static Vector3 BankLocation
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { banklocation = Bank.Location; }); Thread.Sleep(10);
                return banklocation;
            }
        }

        public static bool IsKnockedDown
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isknockeddown = Player.IsKnockedDown; }); Thread.Sleep(10);
                return isknockeddown;
            }
        }

        public static bool IsMounted
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismounted = Mount.IsMounted; }); Thread.Sleep(10);
                return ismounted;
            }
        }

        public static bool IsMountCasting
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismountcasting = Mount.IsCasting; }); Thread.Sleep(10);
                return ismountcasting;
            }
        }

        public static bool IsMountCasted
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismountcasted = (bool)Mount.IsCasted; }); Thread.Sleep(10);
                return ismountcasted;
            }
        }

        public static bool IsMoving
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismoving = Player.IsMoving; }); Thread.Sleep(10);
                return ismoving;
            }
        }

        public static bool IsHarvesting
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isharvesting = Player.IsHarvesting; }); Thread.Sleep(10);
                return isharvesting;
            }
        }

        public static bool CanMove(Vector3 myvec)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Player.CanMove(myvec); }); Thread.Sleep(25);

            return ret;
        }

        public static bool IsInCity
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isincity = Player.IsPlayerInCity; }); Thread.Sleep(10);
                return isincity;
            }
        }

        public static bool IsBankOpen
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isbankopen = Npc.BankIsOpen; }); Thread.Sleep(10);
                return isbankopen;
            }
        }

        public static Vector3 PlayerLocation
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { playerlocation = Player.Location; }); Thread.Sleep(10);
                return playerlocation;
            }
        }

        public static float PlayerWeight
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { playerweight = (float)Player.Weight; }); Thread.Sleep(10);
                return playerweight;
            }
        }

        public static long PlayerID
        {
            get
            {
                if (playerid != null)
                    return (long)playerid;

                ConcurrentTaskManager.GameRun(() => { playerid = (long)Player.ID; }); Thread.Sleep(10);
                return (long)playerid;
            }
        }

        public static ItemInstance[] Items
        {
            get
            {
                lock (items)
                {
                    ConcurrentTaskManager.GameRun(() => { items = Item.Items; }); Thread.Sleep(10);
                }
                return items;
            }
        }

        public static HarvestableInstance[] Harvestables
        {
            get
            {
                lock (harvestables)
                {
                    ConcurrentTaskManager.GameRun(() => { harvestables = Harvest.Harvestables; }); Thread.Sleep(10);
                }
                return harvestables;
            }
        }

        public static MobInstance[] Mobs
        {
            get
            {
                mobs = null;
                ConcurrentTaskManager.GameRun(() => { mobs = Mob.Mobs; }); Thread.Sleep(25);
/*
                lock (mobs)
                {
                    ConcurrentTaskManager.GameRun(() => { mobs = Mob.Mobs; }); Thread.Sleep(25);
                }
                */
                return mobs;
            }
        }

        public static bool IsAttacked
        {
            get
            {
                var pid = PlayerID;

                var Hostile = Mobs
                    .Where(i => i.TargetId == pid)
                    .Where(i => !i.IsDead)
                    .FirstOrDefault();

                return (Hostile != null);
            }
        }

        public static bool IsMountEquipped
        {
            get
            {
                var Check_Mount = Items
                        .Where(i => (int)i.SlotOfInventory == 7)
                        .Where(i => !String.IsNullOrEmpty(i.Name))
                        .Where(i => (bool)i.IsEquiped)
                        .FirstOrDefault();


                return (Check_Mount != null);
            }
        }

        public static bool IsOverweight
        {
            get
            {
                return (playerweight > 99f);
            }
        }

    }


    public class oVector : Vector3
    {
        public bool InitiationPoint = false;

        public oVector()
            : base()
        {

        }

        public oVector(bool initiationpoint)
            : base()
        {
            InitiationPoint = initiationpoint;
        }

        public oVector(float x, float y, float z, bool initiationpoint)
            : base(x,y,z)
        {
            InitiationPoint = initiationpoint;
        }

        public static bool operator ==(oVector a, Vector3 b)
        {
            return ((Vector3)a == b);
        }

        public static bool operator !=(oVector a, Vector3 b)
        {
            return ((Vector3)a != b);
        }

    }

}
