using System;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.ComponentModel;
using AOKore.Game;


namespace Script
{


    public class Program
    {
        public static string ScriptName = "oWaypoint";

        public static void Main()
        {
            ClearLog();

            if (!GameData.IsOnline)
            {
                Log("You aren't logged in.");
                return;
            }

            // this doesnt work ... doesnt matter
            try
            {
                SafeFileHandle test = CreateNamedPipe("test", 0, 0, 0, 0, 0, 0, IntPtr.Zero);
            }
            catch(Exception ex)
            {
                Log(ex.Message);
            }


            try
            {
                FileStream tr = new FileStream(test, FileAccess.Read);
            }
            catch(Exception ex)
            {
                Log(ex.Message);
            }
            
            Log("Creating pipe.");
            //_pipe = new NamedPipeClientStream("oWaypoint");
            Log("Connecting ...");

        }


        [DllImport("Kernel32.dll", SetLastError = true)]
        extern static SafeFileHandle CreateNamedPipe(string name, UInt32 openMode, UInt32 pipeMode,
            UInt32 maxInstances, UInt32 outBufferSize, UInt32 inBufferSize, UInt32 defaultTimeOut,
            IntPtr securityAttributes);

        public static void Log(string message)
        {
            Output.Log(message, ScriptName);
        }

        public static void ClearLog()
        {
            Output.ClearLog(ScriptName);
        }

    }
}
