using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Runtime.CompilerServices;

using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;


namespace Script
{


    public class Program
    {
        //[Configuration("oExpedition")]
        //public static Configuration Settings { get; set; }

        public enum Intentions
        {
            ScriptExit,
            Initializing

        };

        public static string ScriptName = "oExpedition";


        public static void MyCastEventHandler(object sender, CastEventEventArgs e)
        {
            try
            {
                Type t = sender.GetType();

                Log(String.Format("> Cast Event Handler from {0} . {1} : ", t.Namespace, t.Name));
                Log(String.Format("  > Source: [{0}] Type  : [{1}]", e.Mob.SourceName, e.CastType));
                Log(String.Format("  > Spell : [{0}] Target: [{1}]", e.Mob.SpellCasted, e.Mob.TargetId));
                if(e.CastLocation != null)
                {
                    Log(String.Format("  > Loc   : [{0},{1},{2}]", e.CastLocation.X, e.CastLocation.Y, e.CastLocation.Z));
                    // 0, -44, 0  value for target
                }


                // OnCastStart
                // OnCastFinished
                // OnSpellAdded      off cooldown?
                // OnSpellHit
                // OnSpellRemoved    cooldown?
                // OnChannelingHit   (he hit me)
                // OnChannelingEnd
                // 
                // MOB_HERETIC_TANK_01
                // MOB_HERETIC_TANK_SHIELD          +12 physical armor, 2.5s
                // MOB_HERETIC_TANK_CHANNEL
                // HERETIC_TANK_CRUSH               crowdcontrol , range 5    (OnCastStart)
                // MOB_HERETIC_TANK_REMOVE_SHIELD   
                // 
            }
            catch(Exception ea)
            {
                Log("> Error: " + ea.Message);
            }
        }

        public static void Main()
        {            
            ClearLog();

            //Mob.ScheduleAttackVfxEventHandler
            Mob.MobCastEvent += MyCastEventHandler;

            //Mob.MobAttack += MyVfxEventHandler;

            var Timeout = DateTime.Now.AddMinutes(1);

            while (DateTime.Now < Timeout)
                Thread.Sleep(1000);

            //Mob.MobAttack -= MyVfxEventHandler;
            Mob.MobCastEvent -= MyCastEventHandler;

            Log("Exiting");
        }


        public static void Log(string message)
        {
            Output.Log(message, ScriptName);
        }

        public static void ClearLog()
        {
            Output.ClearLog(ScriptName);
        }


    }
}
