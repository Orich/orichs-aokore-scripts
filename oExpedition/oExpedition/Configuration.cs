﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;

namespace Script
{
    public class Configuration
    {
        public static string WaypointPath = System.Environment.CurrentDirectory + "\\Plugins\\Cache\\WayPoint\\";

        public class PathData
        {
            public Vector3[] Path { get; set; }
        }

        public static T FromXml<T>(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }



    }
}
