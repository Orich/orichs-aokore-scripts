﻿using System.Threading;

namespace Script
{
    public static class oThread
    {
        public static void EndScript()
        {
            Mover.Dispose();
            Thread.CurrentThread.Abort();
        }
    }


}
