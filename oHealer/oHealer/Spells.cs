﻿using AOKore.Game;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;



namespace Script
{
    public static class Spells
    {
        private static List<Spell> _allspells = new List<Spell>();
        private static List<Spell> _myspells = null;

        #region Public Methods
        private static bool _hasspells
        {
            get
            {
                if (_myspells == null || !_myspells.Any())
                    _getspells();

                return (_myspells != null && _myspells.Any());
            }
        }

        private static bool _getspells()
        {
            var spellnames = GameData.Spells;

            if (spellnames == null || !spellnames.Any())
                return false;

            _myspells = new List<Spell>();

            foreach(var spell in spellnames)
            {
                var found = _allspells.FirstOrDefault(x => x.Equals(spell));

                if (found != null)
                    _myspells.Add(found);
            }

            return true;
        }

        private static byte? _getslot(Spell spell)
        {
            if (!_hasspells)
                return null;

            var _slot = _myspells.FindIndex(x => x == spell);

            if (_slot == -1)
            {
                Log.Debug("Fatal:  Can't find slot for {0}", spell.Name);
                return null;
            }

            return (byte)_slot;
        }

        private static bool _cancast(Spell spell)
        {
            bool ret = false;

            var _slot = _getslot(spell);

            if (_slot == null)
                return false;

            return GameData.CanCast((byte)_slot);
        }

        private static bool _cast(Spell spell, long TargetID)
        {
            bool ret = false;

            var _slot = _getslot(spell);

            if (_slot == null)
                return false;

            byte Slot = (byte)_slot;

            if (spell.Target.Is(Target.Ground))
            {
                ret = GameData.CastSpell(Slot, GameData.GetMob(TargetID).Location);
                Log.Debug2("Cast: Casting {0}/Slot:{1} to Ground.", spell.Name, Slot);
            }
            else if (spell.Target.Is(Target.Self))
            {
                ret = GameData.CastSpellSelf(Slot);
                Log.Debug2("Cast: Casting {0}/Slot:{1} to Self.", spell.Name, Slot);
            }
            else
            {
                ret = GameData.CastSpell(Slot, TargetID);
                Log.Debug2("Cast: Casting {0}/Slot:{1} to Target {2}.", spell.Name, Slot, TargetID);
            }

            return ret;
        }

        #endregion

        #region Public Methods
        public static bool Recover()
        {
            if (!_hasspells || !GameData.IsOnline)
            {
                Log.Debug2("Recover: No spells or Offline");
                return false;
            }           

            var Defensives = _myspells
                        .Where(x => x.Category == Category.Heal)
                        //.Where(x => GameData.MyEP > x.EnergyCost)
                        .OrderByDescending(x => x.RecastDelay);

            if (Defensives == null || !Defensives.Any())
            {
                Log.Debug("Recovery: No defensive spells.");
                return false;
            }

            GameData.StopAllActions();

            while (GameData.MyHP < GameData.MyMaxHP && GameData.MyEP < (GameData.MyMaxEP * 0.75f))
            {
                foreach (var spell in Defensives)
                {
                    byte slot = (byte)_getslot(spell);

                    while (GameData.IsCastingOrChanneling || !_cancast(spell))
                        Thread.Sleep(250);

                    Log.Debug2("Recovery: Casting {0}/Slot:{1}", spell.Name, slot);
                    GameData.CastSpellSelf(slot);

                    while (GameData.IsCastingOrChanneling &&
                        (GameData.MyHP < GameData.MyMaxHP || GameData.MyEP < (GameData.MyMaxEP * 0.75f)))
                        Thread.Sleep(1000);

                }
                Thread.Sleep(250);
            }

            GameData.StopAllActions();
            return true;
        }

        public static bool CastWhenReady(Spell spell, long TargetID)
        {
            if (!_hasspells || GameData.IsMounted || !GameData.IsOnline)
            {
                Log.Debug2("Casting: No spells or Mounted or Offline");
                return false;
            }

            while (!_cancast(spell) || GameData.IsCastingOrChanneling)
            {
                Log.Debug2("Spell: Waiting to cast {0}.", spell.Name);
                Thread.Sleep(250);
            }

            var mob = GameData.GetMob(TargetID);

            if(spell.CastRange > 0)
                if (!Mover.MoveWithinRange(mob.Location, spell.CastRange))
                    return false;

            if (GameData.MyEP >= spell.EnergyCost)
                return _cast(spell, TargetID);

            return false;
        }

        public static bool CastNextAvailable(MobInstance Target)
        {
            Target = GameData.GetMob(Target.Id);

            if (Target == null || Target.IsDead)
                return false;

            return CastNextAvailable(Target.Id);
        }

        public static bool CastNextAvailable(RplayerInstance Target)
        {
            Target = GameData.GetRPlayer(Target.Id);

            if (Target == null || Target.IsDead)
                return false;

            return CastNextAvailable(Target.Id);
        }

        private static bool CastNextAvailable(long TargetID)
        {
            if (!_hasspells || GameData.IsMounted)
            {
                Log.Debug2("Casting: No spells or Mounted.");
                return false;
            }

            if (GameData.IsCastingOrChanneling)
            {
                Log.Debug2("Casting: Can't cast yet, already casting.");
                return false;
            }

            var Offensives = _myspells
                        .Take(3)
                        .Where(x => (GameData.MyEP - x.EnergyCost) > 0)
                        .OrderByDescending(x => x.RecastDelay);

            if (!Offensives.Any())
            {
                Log.Debug2("Casting: No offensive spells.");
                return false;
            }

            bool ret = false;

            

            foreach (var spell in Offensives)
            {
                if(_cancast(spell))
                {
                    ret = CastWhenReady(spell, TargetID);
                    break;
                }
            }

            return ret;
        }

        public static bool CastSequence(LinkedList<Spell> CastOrder, long TargetID)
        {
            var node = CastOrder.First;
            bool ret = true;

            while(node != null)
            {
                if (!CastWhenReady(node.Value, TargetID))
                    ret = false;

                float wait = 0;

                if (node.Next != null)
                    wait = node.Value.HitDelay - node.Next.Value.CastingTime;

                if (wait > 0)
                    wait = wait * 1000f * 0.95f;
                else
                    wait = 0;

                Thread.Sleep((int)Math.Floor(wait));

                node = node.Next;
            }

            return ret;
        }

        public static LinkedList<Spell> Build_Opener()
        {
            if (!_hasspells)
                return null;

            LinkedList<Spell> Order = new LinkedList<Spell>();

            var Buff = _myspells.Take(3).FirstOrDefault(x => x.Category == Category.Buff);

            if (Buff != null)
                Order.AddFirst(Buff);

            var Offensives = _myspells
                        .Take(3)
                        .Where(x => x.Target.Is(Target.Enemy) || x.Target.Is(Target.Ground))
                        .OrderByDescending(x => x.HitDelay)
                        .ThenByDescending(x => x.RecastDelay);

            foreach (var spell in Offensives)
            {
                Order.AddLast(spell);
            }

            return Order;
        }

        public static ReadOnlyCollection<Spell> AllSpells
        {
            get
            {
                return _allspells.AsReadOnly();
            }
        }
        public static ReadOnlyCollection<Spell> MySpells
        {
            get
            {
                if (_myspells == null || !_myspells.Any())
                    if (_getspells())
                        return _myspells.AsReadOnly();
                    else
                        return null;
                else
                    return _myspells.AsReadOnly();
            }
        }
        #endregion

        #region Classes
        public class Spell
        {
            public string Name = String.Empty;
            //public string Category = String.Empty;
            public Category Category = null;
            public float CastingTime = 0;
            public float HitDelay = 0;
            public float RecastDelay = 0;
            public int EnergyCost = 0;
            public float CastRange = 0;
            public string Type = String.Empty;
            public Target Target = Target.None;

            public override int GetHashCode()
            {
                int hash = 3;
                hash = hash * 486187739 + Name.GetHashCode();
                hash = hash * 486187739 + Type.GetHashCode();

                return hash;
            }

            public bool Equals(Spell other)
            {
                return (!ReferenceEquals(other, null)
                    && (ReferenceEquals(this, other) || (other.Name.Equals(Name, StringComparison.OrdinalIgnoreCase)))
                    );
            }

            public override bool Equals(object obj)
            {
                return Name.Equals(obj.ToString());
            }

            public bool Equals(string spellname)
            {
                return Name.Equals(spellname, StringComparison.OrdinalIgnoreCase);
            }

            public static bool operator !=(Spell a, Spell b)
            {
                return !(a == b);
            }

            public static bool operator ==(Spell a, Spell b)
            {
                return ReferenceEquals(a, b)
                    || (!ReferenceEquals(a, null) && a.Equals(b));
            }
        }

        [Flags]
        public enum Target
        {
            None = 0,
            Enemy = 1,
            Self = 1 << 2,
            Ground = 1 << 3,
            FriendOther = 1 << 4,
            KnockedDownPlayer = 1 << 5,
            Other = 1 << 6,
            AllMobs = 1 << 7,
            AllPlayers = 1 << 8,
            FriendOtherMobs = 1 << 9,
            FriendAll = 1 << 10,
            All = 1 << 11
        }

        private static bool Is(this Target current, Target value)
        {
            return (current & value) == value;
        }

        public class Category
        {
            private Category(string value) { Value = value; }

            public string Value { get; set; }

            public static Category Heal { get { return new Category("heal"); } }
            public static Category Damage { get { return new Category("damage"); } }
            public static Category Buff { get { return new Category("buff"); } }
            public static Category Debuff { get { return new Category("debuff"); } }
            public static Category Crowdcontrol { get { return new Category("Crowdcontrol"); } }
            public static Category Forcedmovement { get { return new Category("forcedmovement"); } }
            public static Category Instant { get { return new Category("instant"); } }
            public static Category Cheat { get { return new Category("cheat"); } }
            public static Category Matchbonus { get { return new Category("matchbonus"); } }
            public static Category Crimedebuff { get { return new Category("crimedebuff"); } }
            public static Category Crimeprotectionbuff { get { return new Category("crimeprotectionbuff"); } }
            public static Category Focusfire { get { return new Category("focusfire"); } }
            public static Category Healingsickness { get { return new Category("healingsickness"); } }
            public static Category Diminishingreturn { get { return new Category("diminishingreturn"); } }
            public static Category Mountbuff { get { return new Category("mountbuff"); } }
            public static Category Furniturebuff { get { return new Category("furniturebuff"); } }
            public static Category Foodbuff { get { return new Category("foodbuff"); } }
            public static Category Movementbuff { get { return new Category("movementbuff"); } }
            public static Category Buff_damageshield { get { return new Category("buff_damageshield"); } }
            public static Category Territorybuff { get { return new Category("territorybuff"); } }
            public static Category Portalbuff { get { return new Category("portalbuff"); } }
            public static Category Hellbuff { get { return new Category("hellbuff"); } }

            public bool Equals(Category other)
            {
                return (!ReferenceEquals(other, null)
                    && (ReferenceEquals(this, other) || (other.Value.Equals(Value, StringComparison.OrdinalIgnoreCase)))
                    );
            }

            public override bool Equals(object obj)
            {
                return Value.Equals(obj.ToString());
            }

            public bool Equals(string catname)
            {
                return Value.Equals(catname, StringComparison.OrdinalIgnoreCase);
            }

            public static bool operator !=(Category a, Category b)
            {
                return !(a == b);
            }

            public static bool operator ==(Category a, Category b)
            {
                return ReferenceEquals(a, b)
                    || (!ReferenceEquals(a, null) && a.Equals(b));
            }
        }
        #endregion


        public static void PopulateSpells()
        {
            if (_allspells.Any())
                return;

            _allspells = new List<Spell>();

            //<!--Auto Attack & Spell Cast Stacks 

            _allspells.Add(new Spell() { Name = "PASSIVE_AA_STACK", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "PASSIVE_SPELL_STACK", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Auto Attack Passives 


            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_DAGGER_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_DAGGER_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_SPEAR_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_SPEAR_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_BOW_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_AASPEEDCHANCE_BOW_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });











            _allspells.Add(new Spell() { Name = "PASSIVE_ENERGYCHANCE_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_ENERGYCHANCE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "heal", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_DAGGER_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_DAGGER_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_SPEAR_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_SPEAR_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_AXE_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_AXE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_SWORD_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_SWORD_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_QUARTERSTAFF_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_QUARTERSTAFF_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_HAMMER_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_HAMMER_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_MACE_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALTHCHANCE_MACE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });






            _allspells.Add(new Spell() { Name = "PASSIVE_CASTINGSPEED_CHANCE_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_CASTINGSPEED_CHANCE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_DAGGER_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_DAGGER_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_SPEAR_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_SPEAR_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_AXE_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_AXE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_BOW_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CHANCE_BOW_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });






            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CASTER_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PASSIVE_SPELLPOWER_CASTER_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "PASSIVE_FROST_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_FROST_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_ARMORCHANCE_AXE_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_ARMORCHANCE_AXE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_ARMORCHANCE_SWORD_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_ARMORCHANCE_SWORD_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });




            _allspells.Add(new Spell() { Name = "PASSIVE_ARMOR_CASTER_CONDITION", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_ARMOR_CASTER_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PASSIVE_STUNCHANCE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_STUNCHANCE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PASSIVE_HEALPOWERCHANCE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEALPOWERCHANCE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PASSIVE_SLOWPOISON_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SLOWPOISON_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SLOWPOISON_EFFECT_PLAYERS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SLOWPOISON_EFFECT_MOBS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });




            _allspells.Add(new Spell() { Name = "PASSIVE_BLEEDCHANCE_CONDITION", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_BLEEDCHANCE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_BURN_CONDITION", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_BURN_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_CURSE_CONDITION", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CURSE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACKCHANCE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACKCHANCE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACKCHANCE_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });




            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACK_CASTER_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACK_CASTER_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_KNOCKBACK_CASTER_EFFECT_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });




            _allspells.Add(new Spell() { Name = "PASSIVE_SILENCECHANCE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SILENCECHANCE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_QUARTERSTAFF_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_QUARTERSTAFF_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_HAMMER_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_HAMMER_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_MACE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CCDURATION_CHANCE_MACE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });




            _allspells.Add(new Spell() { Name = "PASSIVE_MOVESPEED_CHANCE_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_MOVESPEED_CHANCE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });







            _allspells.Add(new Spell() { Name = "PASSIVE_THREATBONUS_CONDITION", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_THREATBONUS_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });




            //<!--
            //#############################################################################################
            //################################### EQUIPMENT PASSIVES ######################################
            //#############################################################################################


            //<!--Cloth Armor




            //<!--Leather Armor






            //<!--Plate Armor







            //<!--Cloth Shoes & Helmets




            //<!--Leather Shoes & Helmets






            //<!--Plate Shoes & Helmets







            //<!--Gatherer Armor Passives 

            //<!--FIBER

            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_FIBER_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            //<!--HIDE

            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_HIDE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            //<!--ORE

            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ORE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--ROCK

            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_ROCK_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            //<!--WOOD

            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_YIELD_WOOD_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });




            //<!--Gatherer Helmet Passives 

            //<!--FIBER

            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_FIBER_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            //<!--HIDE

            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_HIDE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--ORE

            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ORE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--ROCK

            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_ROCK_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--WOOD

            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HEAD_YIELD_WOOD_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });





            //<!--Gatherer Shoes Passives 

            //<!--FIBER
            //<!--FIBER

            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_FIBER_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            //<!--HIDE

            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_HIDE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--ORE

            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ORE_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--ROCK

            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_ROCK_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!--WOOD

            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_EFFECT_T4", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_T4_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_EFFECT_T5", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_T5_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_EFFECT_T6", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_T6_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_EFFECT_T7", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_T7_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });


            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_EFFECT_T8", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_SHOES_YIELD_WOOD_T8_REMOVE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });





            //<!--Gatherer Backpack Passives 

            //<!--FIBER











            //<!--HIDE












            //<!--ORE












            //<!--ROCK












            //<!--WOOD















            //<!--Stat Passives







            //<!--################################################################################################################################
            //<!--============================================================== HELMET PASSIVES =================================================== 

            //<!--################################################################################################################################





            //<!--################################################################################################################################
            //<!--============================================================== ARMOR PASSIVES ==================================================== 

            //<!--################################################################################################################################











            //<!--################################################################################################################################
            //<!--============================================================== SHOES PASSIVES ==================================================== 

            //<!--################################################################################################################################




            //<!--============================================================== OLD ARMOR PASSIVES==================================================== 



            //<!--################################################################################################################################
            //<!--============================================================== GENERIC PASSIVES ==================================================== 

            //<!--################################################################################################################################


            //<!--################################################################################################################################
            //<!--============================================================== BOW PASSIVES ==================================================== 

            //<!--################################################################################################################################

            _allspells.Add(new Spell() { Name = "PASSIVE_SUNDERARMOR_EFFECT", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "debuff", Target = Target.All });
            //<!--################################################################################################################################
            //<!--============================================================== SPEAR PASSIVES ==================================================== 

            //<!--################################################################################################################################

            _allspells.Add(new Spell() { Name = "PASSIVE_MOVESPEEDCHANCE_EFFECT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            //<!--################################################################################################################################
            //<!--============================================================== DAGGER PASSIVES ==================================================== 

            //<!--################################################################################################################################

            _allspells.Add(new Spell() { Name = "PASSIVE_ATTACKSPEEDCHANCE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            //<!--########################################################################################################################################
            //<!--============================================================== NATURE STAFF PASSIVES==================================================== 

            //<!--#########################################################################################################################################

            _allspells.Add(new Spell() { Name = "PASSIVE_MINDSAP_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.All });
            //<!--#########################################################################################################################################
            //<!--============================================================== THROWN WEAPONS PASSIVES==================================================== 

            //<!--#########################################################################################################################################


            //<!--#########################################################################################################################################
            //<!--============================================================== FIRE STAFF WEAPONS PASSIVES ================================================ 

            //<!--#########################################################################################################################################


            //<!--============================================================== HOLY STAFF WEAPONS PASSIVES ================================================ 

            //<!--#########################################################################################################################################

            _allspells.Add(new Spell() { Name = "PASSIVE_HOLYPOWER_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_HOLYPOWER_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            //<!--#########################################################################################################################################
            //<!--============================================================== Old Weapon Passives==================================================== 




            //<!--******************************************************

            //<!--***************PORTAL RESTRICTIONS * ***************

            //<!--******************************************************

            _allspells.Add(new Spell() { Name = "PORTAL_RESTRICTION_BINDING_IMMUNITY", Category = Category.Portalbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PORTAL_RESTRICTION_BINDING", Category = Category.Portalbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PORTAL_RESTRICTION_LOW", Category = Category.Portalbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PORTAL_RESTRICTION_MEDIUM", Category = Category.Portalbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PORTAL_RESTRICTION_HIGH", Category = Category.Portalbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            //<!--mount buffs

            // ATTENTION:-category should be "mountbuff" for all mount-related buffs(otherwise they could be purged by other spells)

            _allspells.Add(new Spell() { Name = "T2_MOUNT_MULE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T2_MOUNT_MULE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Normal Horse
            _allspells.Add(new Spell() { Name = "T3_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T3_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T4_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T4_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Armored Horse
            _allspells.Add(new Spell() { Name = "T5_MOUNT_ARMORED_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_ARMORED_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_ARMORED_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_ARMORED_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_ARMORED_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_ARMORED_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_ARMORED_HORSE_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_ARMORED_HORSE_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Ox Mount
            _allspells.Add(new Spell() { Name = "T3_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T3_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T4_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T4_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_OX_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_OX_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Rare Mounts
            _allspells.Add(new Spell() { Name = "T4_MOUNT_GIANTSTAG_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T4_MOUNT_GIANTSTAG_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_GIANTSTAG_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_GIANTSTAG_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_GIANTSTAG_XMAS_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_GIANTSTAG_XMAS_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_DIREWOLF_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T6_MOUNT_DIREWOLF_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_DIREWOLF_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_DIREWOLF_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_DIREBOAR_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T7_MOUNT_DIREBOAR_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_DIREBEAR_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_DIREBEAR_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_MAMMOTH_TRANSPORT_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_MAMMOTH_TRANSPORT_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_MAMMOTH_COMMAND_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_MAMMOTH_COMMAND_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_SWAMPDRAGON_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_SWAMPDRAGON_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Faction Mounts
            _allspells.Add(new Spell() { Name = "T5_MOUNT_HORSE_UNDEAD_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_HORSE_UNDEAD_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "T8_MOUNT_HORSE_UNDEAD_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_HORSE_UNDEAD_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "T5_MOUNT_COUGAR_KEEPER_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_COUGAR_KEEPER_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "T8_MOUNT_COUGAR_KEEPER_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_COUGAR_KEEPER_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "T5_MOUNT_ARMORED_HORSE_MORGANA_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T5_MOUNT_ARMORED_HORSE_MORGANA_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "T8_MOUNT_ARMORED_HORSE_MORGANA_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "T8_MOUNT_ARMORED_HORSE_MORGANA_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Founder mounts
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_HORSE_FOUNDER_LEGENDARY_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_HORSE_FOUNDER_LEGENDARY_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_OX_FOUNDER_LEGENDARY_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_OX_FOUNDER_LEGENDARY_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_GIANTSTAG_FOUNDER_EPIC_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_GIANTSTAG_FOUNDER_EPIC_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_HORSE_STARTERPACK_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_HORSE_STARTERPACK_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_CART_STARTERPACK_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_CART_STARTERPACK_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Tell a friend mounts
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_DONKEY_UNIQUE_TELLAFRIEND_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_DONKEY_UNIQUE_TELLAFRIEND_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_TELLAFRIEND_MOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNIQUE_MOUNT_TELLAFRIEND_HALFMOUNTED", Category = Category.Mountbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Dismounted buff
            _allspells.Add(new Spell() { Name = "DISMOUNTED", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });



            //<!--Mount Active Spells 

            _allspells.Add(new Spell() { Name = "MOUNTSPELL_COUGARSPRINT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MOUNTSPELL_UNDEAD_STEALTH", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MOUNTSPELL_FLAMETRAIL", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOUNTSPELL_FLAMETRAIL_PULSE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOUNTSPELL_FLAMETRAIL_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            _allspells.Add(new Spell() { Name = "MOUNTSPELL_DRAGONBREATH", Category = Category.Damage, CastingTime = 0.6f, HitDelay = 0, RecastDelay = 20, EnergyCost = 17, CastRange = 11, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "MOUNTSPELL_DRAGONBREATH_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
















            //<!--furniture buffs // ATTENTION:-category should be "furniturebuff" for all furniture-related buffs(otherwise they could be purged by other spells) 
            _allspells.Add(new Spell() { Name = "UNIQUE_FURNITUREITEM_FOUNDER_LOOKINGGLASS", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Table buffs increased hitpoints regeneration 
            _allspells.Add(new Spell() { Name = "FURNITURE_BUFF_TABLE", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Bed buffs increased hitpoints
            _allspells.Add(new Spell() { Name = "FURNITURE_BUFF_BED", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Animal Trophy increased max load and movement speed
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_MOB", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Physical Armor Trophy, physicalarmor buff
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_PHYSICALARMOR", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Magical Armor Trophy, magical resistance buff   
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_MAGICALARMOR", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Boss heart
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_MORGANA_HEART", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_HARVESTER_HEART", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Physical Attack Trophy, physicalattackdamagebonus, physicalspelldamagebonus   
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_PHYSICALDAMAGE", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--Magical Attack Trophy, magicattackdamagebonus, magicspelldamagebonus   
            _allspells.Add(new Spell() { Name = "FURNITURE_TROPHY_BUFF_MAGICALDAMAGE", Category = Category.Furniturebuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 47, CastRange = 9, Type = "buff", Target = Target.Self });
            //<!--######################################
            //<!--########## Christmas Vanity ##########
            //<!--######################################
            _allspells.Add(new Spell() { Name = "VANITY_CONSUMABLE_FIREWORKS_BLUE", Category = Category.Buff, CastingTime = 0.5f, RecastDelay = 0, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "VANITY_CONSUMABLE_FIREWORKS_GREEN", Category = Category.Buff, CastingTime = 0.5f, RecastDelay = 0, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "VANITY_CONSUMABLE_FIREWORKS_RED", Category = Category.Buff, CastingTime = 0.5f, RecastDelay = 0, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "VANITY_CONSUMABLE_FIREWORKS_YELLOW", Category = Category.Buff, CastingTime = 0.5f, RecastDelay = 0, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "VANITY_CONSUMABLE_SNOWBALL", Category = Category.Buff, CastingTime = 0.8f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "VANITY_XMAS_HEAD", Category = Category.Buff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--######################################
            //<!--Test Spells
            _allspells.Add(new Spell() { Name = "POISONCLOUD", Category = Category.Damage, CastingTime = 5, RecastDelay = 3, EnergyCost = 10, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POISONCLOUD_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SUMMONWOLF", Category = Category.Buff, CastingTime = 1, RecastDelay = 2, EnergyCost = 10, CastRange = 5, Type = "damage", Target = Target.Ground });
            //<!--Potions
            _allspells.Add(new Spell() { Name = "POTIONHEAL", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 3, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONFLARE", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.20f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONTAR", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.20f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "TAREFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 23, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTIONFIRE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "FIREPOTEFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 23, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTIONSTEALTH", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONCLEANSE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONSPEED", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONRESET", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTIONENDURANCE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Food
            _allspells.Add(new Spell() { Name = "EATFOOD", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 0, EnergyCost = 0, CastRange = 9.9f, Type = "heal", Target = Target.Self });


            //<!--Global Effects
            _allspells.Add(new Spell() { Name = "PURGE", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CLEANSE", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "PURGE_DAMAGESHIELD", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });


            _allspells.Add(new Spell() { Name = "REVEAL_INVISIBILITY", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "REMOVE_BEAMS", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SELF_HEALINGSICKNESS_NATURE_STACK", Category = Category.Healingsickness, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 9999, CastRange = 0, Type = "debuff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SELF_HEALINGSICKNESS_HOLY_STACK", Category = Category.Healingsickness, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 9999, CastRange = 0, Type = "debuff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SELF_HEALINGSICKNESS", Category = Category.Healingsickness, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 9999, CastRange = 0, Type = "debuff", Target = Target.Self });


            //<!--Arcane Staff
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "SHIELDFRIENDLY", Category = Category.Buff_damageshield, CastingTime = 0, HitDelay = 0, RecastDelay = 5, EnergyCost = 6, CastRange = 9, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "ENERGYBOLT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 4, EnergyCost = 8, CastRange = 9, Type = "damage", Target = Target.FriendAll });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "FRAZZLE2", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0, RecastDelay = 0, EnergyCost = 5, CastRange = 9, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "EMPOWERBEAM", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 13, CastRange = 9, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "EMPOWERBEAM_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 13, CastRange = 9, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "PURGE_SINGLE", Category = Category.Debuff, CastingTime = 1, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 9, CastRange = 9, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CLEANSESPEED2", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 9, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "ENERGYBEAM", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 13, CastRange = 9, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "ENERGYBEAM_EFFECT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.All });


            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "ARCANEORB2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 15, CastRange = 25, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "ARCANEORB2_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "ARCANEORB2_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "POWER_FIELD", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 15, CastRange = 9, Type = "buff", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "POWER_FIELD_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "INVULNERABILITY", Category = Category.Buff_damageshield, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.FriendOther });
            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "BLACKHOLE", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 1, RecastDelay = 20, EnergyCost = 18, CastRange = 11, Type = "crowdcontrol", Target = Target.Ground });


            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "ARCANECORRIDOR", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 30, EnergyCost = 18, CastRange = 25, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "ARCANECORRIDOR_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "ARCANECORRIDOR_EFFECT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            //<!--Axe
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "RENDINGSTRIKE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 2, EnergyCost = 3, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "AXE_BLEED", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HEROICSTRIKEAXE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 2, EnergyCost = 2, CastRange = 3, Type = "damage", Target = Target.Enemy });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "AXESMASH", Category = Category.Damage, CastingTime = 0.5f, HitDelay = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "AXESMASH_EFFECT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 13, CastRange = 3, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "AXEBOOST", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AXEBOOST_BUFF", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "BATTLEFRENZY", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 8, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "INNERBLEEDING", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 8, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "INNERBLEEDING_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "INNERBLEEDING_BLEED", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "VAMPIRICSTRIKE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 18, CastRange = 3, Type = "damage", Target = Target.Enemy });
            //<!--Level 2
            _allspells.Add(new Spell() { Name = "AXEWHIRLWIND2", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 21, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AXEWHIRLWIND2_BUFF", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AXEWHIRLWIND2_AOE_DMG", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "RENDINGSWING", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 18, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "RENDINGSWING_DOT", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "RENDINGSWING_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });
            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SHOCKWAVE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 20, EnergyCost = 17, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SHOCKWAVE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SCYTHESWING", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 20, EnergyCost = 18, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SCYTHESWING_HIT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!--Artefact Level 3
            _allspells.Add(new Spell() { Name = "DUALAXE_CRAWLER", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 16, CastRange = 9, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DUALAXE_CRAWLER_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "crowdcontrol", Target = Target.Self });
            //<!--obsolete

            _allspells.Add(new Spell() { Name = "AXECLEAVE2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 5, CastRange = 3, Type = "damage", Target = Target.Enemy });


            //<!--Bow

            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "POISONARROW", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 3, EnergyCost = 5, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEADLYSHOT", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.1f, RecastDelay = 0, EnergyCost = 6, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MULTISHOT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 3, EnergyCost = 4, CastRange = 35, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MULTISHOT2_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 10, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "BURNINGARROWS", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 11, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BURNINGARROWS_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 18, CastRange = 0, Type = "buff", Target = Target.All });

            _allspells.Add(new Spell() { Name = "JUMPSHOT2", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 10, CastRange = 9, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "JUMPSHOT2_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SPEEDSHOT2", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 15, EnergyCost = 4, CastRange = 11, Type = "movement", Target = Target.Enemy });


            //<!--Slot 3

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SPEEDARCHER", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 19, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SPEEDARCHER_BUFF", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SPEEDARCHER_MAXSTACK", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SPEEDARCHER_PULSE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SPEEDARCHER_COUNT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Level 2
            _allspells.Add(new Spell() { Name = "SKILLSHOT_STUN", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 11, CastRange = 26, Type = "crowdcontrol", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "SKILLSHOT_STUN_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SKILLSHOT_STUN_DISTANCE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "SKILLSHOT_STUN_DAMAGE", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });




            //<!--Level 3
            _allspells.Add(new Spell() { Name = "ARROWRAIN", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 17, CastRange = 13, Type = "damage", Target = Target.Ground });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "UNDEADARROWS", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 14, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEADARROWS_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 2, EnergyCost = 5, CastRange = 4, Type = "buff", Target = Target.Enemy });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "HELL_ARROW", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 30, EnergyCost = 18, CastRange = 24, Type = "crowdcontrol", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "HELL_ARROW_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HELL_ARROW_BUFF", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            //<!--Crossbow
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "AUTOFIRE2", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 5, EnergyCost = 7, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "BOLTSHOT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 2, EnergyCost = 4, CastRange = 15, Type = "damage", Target = Target.Ground });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "KNOCKBACKSHOT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 8, CastRange = 8, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KNOCKBACKSHOT2_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 8, CastRange = 8, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CALTROPS", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 4, EnergyCost = 4, CastRange = 13, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CALTROPS_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "CALTROPS_DOT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SUNDERSHOT", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 7, EnergyCost = 9, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SUNDERSHOT_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 5, CastRange = 11, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SILENCINGBOLT", Category = Category.Debuff, CastingTime = 0.7f, HitDelay = 0, RecastDelay = 15, EnergyCost = 9, CastRange = 20, Type = "debuff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SILENCINGBOLT_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Slot 3

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SNIPESHOT_CROSSBOW", Category = Category.Damage, CastingTime = 3, HitDelay = 0, RecastDelay = 15, EnergyCost = 16, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Level 2
            _allspells.Add(new Spell() { Name = "EXPLODING_SHOT", Category = Category.Damage, CastingTime = 1, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 17, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "EXPLODING_SHOT_EFFECT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 15, CastRange = 9, Type = "damage", Target = Target.Enemy });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "CROSSBOW_ARMORPIERCER", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 15, CastRange = 22, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CROSSBOW_ARMORPIERCER_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "GROUNDMINE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 20, EnergyCost = 14, CastRange = 13, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "GROUNDMINE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "GROUNDMINE_EFFECT2", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.All });
            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "DUAL_RAPIDFIRE", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 13, CastRange = 13, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DUAL_RAPIDFIRE_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            //<!--Artefact Level 3
            _allspells.Add(new Spell() { Name = "CROSSBOW_CONE_ULTIMATE", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 8, EnergyCost = 15, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CROSSBOW_CONE_ULTIMATE_EFFECT", Category = Category.Damage, CastingTime = 3, RecastDelay = 15, EnergyCost = 15, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "NONE", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENFEEBLEBLADES_EFFECT2", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "ENFEEBLEBLADES_FOF", Category = Category.Damage, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SKULLCURSE", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 20, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SKULLCURSE_EFFECT", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "debuff", Target = Target.Enemy });


            //<!--Dagger

            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "SUNDERARMOR2", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 5, CastRange = 2, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "ASSASSINSPIRIT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 4, EnergyCost = 6, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ASSASSINSPIRIT_BUFF", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ASSASSINSPIRIT_FX", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "THROWINGBLADES", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 11, CastRange = 18, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "THROWINGBLADES_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "THROWINGBLADES_EFFECT2", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 9, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "THROWINGBLADES_EFFECT3", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 9, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "GROUNDDASH", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 8, CastRange = 11, Type = "movement", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "SNEAKYDASH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 1.5f, RecastDelay = 15, EnergyCost = 9, CastRange = 11, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SNEAKYDASH_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "SNEAKYDASH_IMMUNITY", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "crowdcontrol", Target = Target.All });

            _allspells.Add(new Spell() { Name = "DEEPCUTS", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 7, CastRange = 2, Type = "debuff", Target = Target.Enemy });



            //<!--Slot 3

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "POISON_COATING_SINGLE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 18, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POISON_COATING_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 18, CastRange = 0, Type = "damage", Target = Target.All });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER", Category = Category.Damage, CastingTime = 0.6f, HitDelay = 0, RecastDelay = 15, EnergyCost = 11, CastRange = 2, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER_EFFECT0", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER_EFFECT1", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER_EFFECT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER_EFFECT3", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "EXECUTEDAGGER_EFFECT_TOOLTIP", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "DISEMBOWEL", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 30, EnergyCost = 15, CastRange = 2, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DISEMBOWEL_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DISEMBOWEL_ROOT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "RAPIERSTAB", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 17, CastRange = 13, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "RAPIERSTAB_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "RAPIERSTAB_EFFECT2", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "RAPIERSTAB_END", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "damage", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "PUNCHCOMBO", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 20, CastRange = 2, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "PUNCHCOMBO_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "PUNCHCOMBO_FINISH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.05f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "PUNCHCOMBO_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.05f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "PUNCHCOMBO_FX", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Fire Staff


            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "FIRESTAFFBOLT2", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 5, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "FIRESTAFFBOLT_AOE", Category = Category.Damage, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 4, CastRange = 11, Type = "damage", Target = Target.Enemy });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "FIRESTAFFIGNITE2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 10, EnergyCost = 13, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "FIRECONE", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 10, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "FIRECONE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "FIRECONE_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "FIREWALL", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 15, CastRange = 3, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "FIREWALL_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "FIREWALL_FEAR", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SKILLSHOT_FIREBALL", Category = Category.Damage, CastingTime = 0.6f, HitDelay = 0, RecastDelay = 10, EnergyCost = 9, CastRange = 18, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SKILLSHOT_FIREBALL_FX", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SKILLSHOT_FIREBALL_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Slot 3


            //<!--Level 1
            _allspells.Add(new Spell() { Name = "PYROBLAST2", Category = Category.Damage, CastingTime = 2, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 15, CastRange = 12, Type = "damage", Target = Target.Enemy });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "FLAMEPILLAR", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 10, EnergyCost = 13, CastRange = 15, Type = "damage", Target = Target.Ground });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "SPREADING_FLAME_SINGLE", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.2f, RecastDelay = 30, EnergyCost = 19, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SPREADING_FLAME_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SPREADING_FLAME_DOT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SPREADING_FLAME_FOF", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "MAGMASPHERE", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 30, EnergyCost = 20, CastRange = 25, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MAGMASPHERE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "METEOR", Category = Category.Damage, CastingTime = 2, HitDelay = 1, RecastDelay = 30, EnergyCost = 24, CastRange = 15, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "METEOR_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 30, EnergyCost = 24, CastRange = 15, Type = "damage", Target = Target.Enemy });


            //<!--Frost Staff

            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "FROSTBOLT2", Category = Category.Crowdcontrol, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 4, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HOARFROST", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 5, CastRange = 11, Type = "damage", Target = Target.Enemy });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "FROSTBOMB", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 2, RecastDelay = 10, EnergyCost = 10, CastRange = 11, Type = "crowdcontrol", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "FROSTNOVA", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 13, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "FROSTBEAM", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 13, CastRange = 11, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "FROSTBEAM_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 6, CastRange = 9, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Slot 3

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "FREEZINGWIND", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 16, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "FREEZINGWIND_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 18, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Level 2
            _allspells.Add(new Spell() { Name = "HAIL", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 15, CastRange = 11, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HAIL_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.1f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "ICESTORM2", Category = Category.Crowdcontrol, CastingTime = 2, HitDelay = 0.2f, RecastDelay = 30, EnergyCost = 23, CastRange = 25, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "ICESTORM2_PULSINGSPELL", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "ICESTORM2_DMG", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "ICESTORM2_SLOW", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "ICEROCK", Category = Category.Crowdcontrol, CastingTime = 1, HitDelay = 0, RecastDelay = 30, EnergyCost = 17, CastRange = 26, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "ICEROCK_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "GLACIALFIELD", Category = Category.Damage, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 30, EnergyCost = 20, CastRange = 11, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "GLACIALFIELD_PULSING", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.1f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "GLACIALFIELD_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.1f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--Hammer
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "HAMSTRINGHAMMER", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "THREATENINGSTRIKE_HAMMER", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 5, CastRange = 3, Type = "damage", Target = Target.Enemy });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "HEAVYSWING", Category = Category.Damage, CastingTime = 1, HitDelay = 0.5f, RecastDelay = 15, EnergyCost = 14, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HEAVYSWING_EFFECT", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "GEYSER", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 10, EnergyCost = 10, CastRange = 11, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "CHARGESLOWAE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 11, CastRange = 12, Type = "movement", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "CHARGESLOWAE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.All });
            _allspells.Add(new Spell() { Name = "CHARGESLOWAE_DAMAGE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.All });
            _allspells.Add(new Spell() { Name = "CHARGESLOWAE_END", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.All });

            _allspells.Add(new Spell() { Name = "KNOCKOUT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 12, CastRange = 1.5f, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "HAMMERWHIRLWIND2", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 15, CastRange = 0, Type = "damage", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "GROUNDBREAKER2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 15, EnergyCost = 16, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "GROUNDBREAKER2_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "GROUNDBREAKER2_EFFECT2", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "HAMMERTACKLE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 15, EnergyCost = 16, CastRange = 11, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HAMMERTACKLE_PULSE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HAMMERTACKLE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HAMMERTACKLE_KNOCKBACK", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HAMMERTACKLE_END", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "damage", Target = Target.Self });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "UNDEADHAND", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 15, CastRange = 26, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEADHAND_VFX", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEADHAND_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "GIANTSTEPS", Category = Category.Buff, CastingTime = 0, RecastDelay = 30, EnergyCost = 20, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "GIANTSTEPS_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            //<!--Artefact Level 3
            _allspells.Add(new Spell() { Name = "RAM_CHARGE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 15, EnergyCost = 16, CastRange = 16, Type = "crowdcontrol", Target = Target.Ground });

            //<!--Holy Staff
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "FLASHHEAL2", Category = Category.Heal, CastingTime = 0.8f, HitDelay = 0, RecastDelay = 0, EnergyCost = 4, CastRange = 12, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "FLASHHEAL2_MAXSTACK", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "GENEROUSHEAL", Category = Category.Heal, CastingTime = 1, HitDelay = 0, RecastDelay = 2, EnergyCost = 6, CastRange = 12, Type = "heal", Target = Target.FriendAll });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "SMITE2", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 5, CastRange = 12, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HOLYHOT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 8, CastRange = 12, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "HEALINGBEAM", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 13, CastRange = 12, Type = "heal", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "HEALINGBEAM_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PULSINGHEAL", Category = Category.Heal, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 11, CastRange = 11, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "PULSINGHEAL_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PULSINGHEAL_FOF", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 1, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.All });

            _allspells.Add(new Spell() { Name = "ENLIGHTENMENT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 11, CastRange = 11, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "RESURRECTION", Category = Category.Heal, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 30, EnergyCost = 23, CastRange = 12, Type = "heal", Target = Target.KnockedDownPlayer });

            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "HOLYDESPERATEPRAYER2", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 12, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYDESPERATEPRAYER2_EFFECT1", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYDESPERATEPRAYER2_EFFECT2", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "heal", Target = Target.FriendAll });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "HOLYEXPLOSION", Category = Category.Heal, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 20, EnergyCost = 18, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HOLYEXPLOSION_EFFECT", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "HOLYEXPLOSION_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "HOLYSHIELD", Category = Category.Buff_damageshield, CastingTime = 0, RecastDelay = 30, EnergyCost = 18, CastRange = 12, Type = "buff", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYSHIELD_EFFECT", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYSHIELD_EFFECT2", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYSHIELD_PULSE", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYSHIELD_FX", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.FriendAll });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "HOLYTOUCH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 10, EnergyCost = 16, CastRange = 3, Type = "crowdcontrol", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HOLYTOUCH_EFFECT", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "HOLY_ULTIMATE", Category = Category.Heal, CastingTime = 1.5f, HitDelay = 3, RecastDelay = 30, EnergyCost = 24, CastRange = 11, Type = "heal", Target = Target.Ground });


            //<!--Maces
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "DEFENSIVESLAM", Category = Category.Buff, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEFENSIVESLAM_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "damage", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "THREATENINGSTRIKE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 5, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "STALLINGSLAM2", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 2, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "HEAVYSWING_MACE", Category = Category.Damage, CastingTime = 1, HitDelay = 0.5f, RecastDelay = 15, EnergyCost = 14, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HEAVYSWING_MACE_EFFECT", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CHARGE_ROOT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 9, CastRange = 11, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CHARGE_ROOT_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "INTERRUPT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 7, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "PBAOE_PULL", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 1, RecastDelay = 30, EnergyCost = 15, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SILENCE2", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 20, EnergyCost = 17, CastRange = 3, Type = "damage", Target = Target.Enemy });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "SHRIEKMACE", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 12, CastRange = 0, Type = "debuff", Target = Target.Self });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "ROOTFIELD", Category = Category.Crowdcontrol, CastingTime = 0.5f, HitDelay = 0, RecastDelay = 30, EnergyCost = 14, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "ROOTFIELD_ROOT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "PRIMALSLAM", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 17, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "PRIMALSLAM_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PRIMALSLAM_STUN", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PRIMALSLAM_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PRIMALSLAM_KNOCKBACK2", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PRIMALSLAM_END", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SHRINKINGSMASH", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 20, EnergyCost = 18, CastRange = 10, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SHRINKINGSMASH_EFFECT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            //<!--Nature Staves
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "REJUVENATION", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 1.5f, EnergyCost = 5, CastRange = 9, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "REJUVENATION_PENALTY", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 1.5f, EnergyCost = 5, CastRange = 11, Type = "heal", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "REJUVMUSHROOM", Category = Category.Heal, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 3, EnergyCost = 6, CastRange = 15, Type = "heal", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "REJUVMUSHROOM_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "REJUVMUSHROOM_CONDITION", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "POISONTHORNS2", Category = Category.Damage, CastingTime = 1, HitDelay = 0.3f, RecastDelay = 0, EnergyCost = 4, CastRange = 9, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CLEANSEHEAL", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 9, CastRange = 11, Type = "heal", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "REANIMATE", Category = Category.Heal, CastingTime = 1, HitDelay = 0, RecastDelay = 7, EnergyCost = 9, CastRange = 11, Type = "heal", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "NATURERESILIENCE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 12, CastRange = 11, Type = "buff", Target = Target.FriendAll });

            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "CIRCLEOFLIFE", Category = Category.Heal, CastingTime = 1, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 11, Type = "heal", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "CIRCLEOFLIFE_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });



            //<!--Level 2
            _allspells.Add(new Spell() { Name = "BRIEROFLIFE", Category = Category.Buff, CastingTime = 1, HitDelay = 0, RecastDelay = 20, EnergyCost = 14, CastRange = 11, Type = "buff", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "BRIEROFLIFE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });



            //<!--Level 3
            _allspells.Add(new Spell() { Name = "WELLOFLIFE2", Category = Category.Heal, CastingTime = 1, HitDelay = 0, RecastDelay = 30, EnergyCost = 20, CastRange = 15, Type = "heal", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "WELLOFLIFE2_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "NATURE_ULTIMATE_SINGLE", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 11, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "NATURE_ULTIMATE_SINGLE_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "ROTTENVINES", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 20, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ROTTENVINES_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.FriendAll });

            //<!--Spears

            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "SPEAR_LUNGE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 4, EnergyCost = 3, CastRange = 8, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SPEAR_LUNGE_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SPIRITSPEAR", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 2, EnergyCost = 3, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPIRIT_CHARGE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SPIRITSPEAR_PENALTY", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPIRITSPEAR_PENALTY_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SPIRITSPEAR_FX", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });



            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "FORESTOFSPEARS", Category = Category.Damage, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 15, EnergyCost = 12, CastRange = 11, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "FORESTOFSPEARS_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 9, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CHARGINGBLADE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 13, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CHARGINGBLADE_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "CHARGINGBLADE_EFFECT2", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "LEGBREAKER", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 6, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "LEGBREAKER_EFFECT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "DEFLECTINGSTANCE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 18, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "DEFLECTINGSTANCE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "DEFLECTINGSTANCE_BUFF", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });



            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "DASHDMG", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 11, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DASHDMG_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 7, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "DASHDMG_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 7, CastRange = 0, Type = "damage", Target = Target.All });


            //<!--Level 2
            _allspells.Add(new Spell() { Name = "HALBERDSMASH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 16, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "SHOVEL", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 15, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SHOVEL_PULL", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SPEARTHROW", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 17, CastRange = 22, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SPEARTHROW_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SPEARTHROW_VFX", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SKILLSHOT_PULL", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 16, CastRange = 22, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SKILLSHOT_PULL_EFFECT", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SKILLSHOT_PULL_EFFECT2", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Swords
            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "BEGINNERSTRIKE", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 2, CastRange = 3, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HEROICSTRIKE2", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 2, CastRange = 3, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "BEGINNERCLEAVE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "CLEAVE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HEROIC_STACK", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 2, CastRange = 3, Type = "buff", Target = Target.All });

            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "INTERRUPT2", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 10, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "INTERRUPT2_BUFF", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "HAMSTRINGSWORD", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 10, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEFENSERUN", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 9, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "CRESCENTSLASH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 17, CastRange = 3, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CRESCENTSLASH_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "MIGHTYBLOW", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 14, CastRange = 3, Type = "damage", Target = Target.Enemy });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "CLAYMORECHARGE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 16, CastRange = 9, Type = "movement", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "SPINATTACK", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 15, EnergyCost = 15, CastRange = 11, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "SPINATTACK_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });



            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "MIGHTYSWING", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 15, CastRange = 0, Type = "damage", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "CLAYMORESLASH", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 20, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CLAYMORESLASH_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CLAYMORESLASH_END", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });

            //<!--Quarter Staff

            //<!--Slot 1
            _allspells.Add(new Spell() { Name = "CARTWHEEL", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 6, EnergyCost = 5, CastRange = 4, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "CARTWHEEL_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CARTWHEEL_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CARTWHEEL_BUFF", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CARTWHEEL_FX", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "CARTWHEEL_END", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "CONCUSSIVEBLOW", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3, EnergyCost = 3, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "CONCUSSION", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Slot 2
            _allspells.Add(new Spell() { Name = "EMPOWEREDSLAM", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 7, EnergyCost = 9, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "STUNRUN", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "STUNRUN_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 7, CastRange = 0, Type = "crowdcontrol", Target = Target.All });

            _allspells.Add(new Spell() { Name = "HEAVYSWING_QS", Category = Category.Damage, CastingTime = 0.5f, HitDelay = 0.5f, RecastDelay = 15, EnergyCost = 10, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HEAVYSWING_QS_EFFECT", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "QS_WHIRLWIND2", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 16, CastRange = 4, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "QS_WHIRLWIND2_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });


            //<!--Slot 3
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SEPARATING_SLAM", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 17, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SEPARATING_SLAM_VFX", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 2, EnergyCost = 1, CastRange = 3, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "SEPARATING_SLAM_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });


            //<!--Level 2
            _allspells.Add(new Spell() { Name = "QSWHIRLWIND", Category = Category.Damage, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 20, EnergyCost = 14, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "QSWHIRLWIND_AOE_DMG", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "QSWHIRLWIND_AOE_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "DASH_KNOCKBACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 16, CastRange = 11, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DASH_KNOCKBACK_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "movement", Target = Target.All });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "COMBATSTAFF_SLASH", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 20, EnergyCost = 20, CastRange = 9, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "COMBATSTAFF_SLASH_PULSE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 20, EnergyCost = 18, CastRange = 15, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "COMBATSTAFF_SLASH_END", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "TORNADO", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 21, CastRange = 25, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "TORNADO_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "TORNADO_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "TORNADO_EFFECT2", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });


            //<!-- ####################################################################################### 
            //<!-- #################################### Armor Spells  #################################### 
            //<!-- ####################################################################################### 
            _allspells.Add(new Spell() { Name = "OUTOFCOMBATHEAL", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "OUTOFCOMBATHEAL_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "OUTOFCOMBATHEAL_EFFECT2", Category = Category.Heal, CastingTime = 0, HitDelay = 0.45f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.FriendAll });
            //<!-- ######## Cloth  Armor ######## 
            _allspells.Add(new Spell() { Name = "FROSTSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FROSTSHIELD_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SPEEDCASTER", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Level 2
            _allspells.Add(new Spell() { Name = "LIFESAVIOR", Category = Category.Buff, CastingTime = 0, RecastDelay = 60, EnergyCost = 15, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "LIFESAVIOR_EFFECT", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "BERSERK_MODE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 12, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BERSERK_MODE_BUFF", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--ROYAL
            _allspells.Add(new Spell() { Name = "MAGICCIRCLE", Category = Category.Heal, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 8, CastRange = 0, Type = "heal", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MAGICCIRCLE_PULSING", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "MAGICCIRCLE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SPELLRUSH", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPELLRUSH_CONDITION", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPELLRUSH_PULSE", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPELLRUSH_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "FEAR_AURA", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 10, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FEAR_AURA_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });


            //<!-- ######## Leather  Armor ######## 
            _allspells.Add(new Spell() { Name = "FLAMESHIELD", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLAMESHIELD_EFFECT", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "BLOODLUST", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BLOODLUST_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.2f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "BLOODLUST_COUNT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.2f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });


            //<!--Level 2
            _allspells.Add(new Spell() { Name = "HASTE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 9, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "AMBUSH", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 40, EnergyCost = 12, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AMBUSH_PULSE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AMBUSH_APPLYONHIT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "AMBUSH_BUFFREMOVAL", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "AMBUSH_BUFF", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AMBUSH_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            //<!--Royal
            _allspells.Add(new Spell() { Name = "ROYAL_BANNER", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 50, EnergyCost = 10, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ROYAL_BANNER_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.FriendAll });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "STORMSHIELD", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "STORMSHIELD_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "LIFESTEALAURA", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 9, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "LIFESTEALAURA_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "LIFESTEALAURA_EFFECT2", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Enemy });

            //<!-- ######## Plate Armor ######## 

            _allspells.Add(new Spell() { Name = "TAUNT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 9, CastRange = 11, Type = "damage", Target = Target.Enemy });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "ENRAGE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENRAGE_BUFF", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENRAGE_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "WINDWALL", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 14, CastRange = 3, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "WINDWALL_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "WINDWALL_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "ENFEEBLEAURA", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 10, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENFEEBLEAURA_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });


            //<!--Royal
            _allspells.Add(new Spell() { Name = "MANADRAIN", Category = Category.Buff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MANADRAIN_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MANADRAIN_EFFECT2", Category = Category.Heal, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MANADRAIN_EFFECT3", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.All });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "ARMORCHAIN", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 11, CastRange = 2, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "ARMORCHAIN_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 15, CastRange = 9, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ARMORCHAIN_VFX", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 15, CastRange = 9, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "REFLECTAREA", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 12, CastRange = 15, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "REFLECTAREA_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.FriendOther });





            //<!-- ####################################################################################### 
            //<!-- ################################### Helmet Spells  #################################### 
            //<!-- ####################################################################################### 

            _allspells.Add(new Spell() { Name = "ENERGYBURST_CHANNEL", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.2f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });


            //<!-- ######## Cloth Helmet ######## 

            _allspells.Add(new Spell() { Name = "PBAOE_KNOCKBACK", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PBAOE_KNOCKBACK_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "DISRUPTIONIMMUNITY", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Level 2
            _allspells.Add(new Spell() { Name = "ICEBLOCK2", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "WEAPON_DOT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "WEAPON_DOT_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            //<!--Royal
            _allspells.Add(new Spell() { Name = "PERPETUALENERGY", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "ENERGYFIELD", Category = Category.Heal, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENERGYFIELD_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "PURGE_HELMET", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 40, EnergyCost = 0, CastRange = 9, Type = "debuff", Target = Target.Enemy });


            //<!-- ######## Leather Helmet ######## 

            _allspells.Add(new Spell() { Name = "HOWL", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HOWL_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "SELF_CLEANSE", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "RETALIATE2", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "SUMMONER_CD_REDUCTION", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Self });


            //<!--Royal
            _allspells.Add(new Spell() { Name = "GROWING_RAGE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "GROWING_RAGE_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "GROWING_RAGE_PENALTY", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "GROWING_RAGE_REMOVAL", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 11, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SMELLOFBLOOD", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SMELLOFBLOOD_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.All });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SMOKEBOMB", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SMOKEBOMB_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            //<!-- ######## Plate Helmet ######## 
            _allspells.Add(new Spell() { Name = "SUMMONER_HEAL", Category = Category.Heal, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Self });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "ENERGYSHIELD2", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "BLOCK", Category = Category.Instant, CastingTime = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "STONESKIN", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Royal
            _allspells.Add(new Spell() { Name = "ARTILLERY_COMMAND", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 1.5f, RecastDelay = 40, EnergyCost = 0, CastRange = 11, Type = "crowdcontrol", Target = Target.Ground });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SACRIFICE_HEAL", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.FriendAll });
            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "WEAPON_SILENCE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "WEAPON_SILENCE_EFFECT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.All });
            //<!-- ####################################################################################### 
            //<!-- #################################### Shoe Spells  ##################################### 
            //<!-- ####################################################################################### 

            _allspells.Add(new Spell() { Name = "RUN", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Tier 1 only spell 
            _allspells.Add(new Spell() { Name = "SPRINT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 11, CastRange = 0, Type = "movement", Target = Target.Self });
            //<!-- ######## Cloth Shoes ######## 
            _allspells.Add(new Spell() { Name = "SPRINTEOT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            //<!--Level 1
            _allspells.Add(new Spell() { Name = "CHANNELED_RUN", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CHANNELED_RUN_BUFF", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "BLINK", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 7, Type = "movement", Target = Target.Ground });
            //<!--Level 3
            _allspells.Add(new Spell() { Name = "DELAYED_TELEPORT", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 20, EnergyCost = 0, CastRange = 15, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DELAYED_TELEPORT_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 2, RecastDelay = 0, EnergyCost = 0, CastRange = 15, Type = "movement", Target = Target.Self });

            //<!--Royal
            _allspells.Add(new Spell() { Name = "GLASS_MOVESPEED", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Artfact Level 1
            _allspells.Add(new Spell() { Name = "FROSTWALK", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "FROSTWALK_PULSE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "FROSTWALK_DASH", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "FROSTWALK_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "SWAP", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 8, Type = "movement", Target = Target.Other });


            //<!-- ######## Leather Shoes ######## 
            _allspells.Add(new Spell() { Name = "SPRINT_CD_REDUCTION", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "STEALTH_REVEAL", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "STEALTH_REVEAL_BUFF", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "OVERSPRINT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "DODGE", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DODGE_SHIELD", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 12, CastRange = 5, Type = "movement", Target = Target.Ground });

            //<!--Royal
            _allspells.Add(new Spell() { Name = "JUMP", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 12, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "JUMP_EFFECT", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "JUMP_END", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "DMG_BLINK", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "movement", Target = Target.Ground });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "DEATHMARK", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEATHMARK_DEBUFF", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEATHMARK_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 2, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEATHMARK_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEATHMARK_EFFECT2", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "DEATHMARK_FX", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });



            //<!-- ######## Plate Shoes ######## 
            _allspells.Add(new Spell() { Name = "SPRINTHOT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Level 1
            _allspells.Add(new Spell() { Name = "WANDERLUST", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "WANDERLUST_EFFECT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Level 2
            _allspells.Add(new Spell() { Name = "CHARGE_SHIELD", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 11, Type = "movement", Target = Target.Other });
            _allspells.Add(new Spell() { Name = "CHARGE_SHIELD_SHIELD", Category = Category.Buff_damageshield, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.All });

            //<!--Level 3
            _allspells.Add(new Spell() { Name = "MAXHEALTHBUFF", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.3f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--ROYAL
            _allspells.Add(new Spell() { Name = "ROYAL_MARCH", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 10, Type = "movement", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "ROYAL_MARCH_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "ROYAL_MARCH_EFFECT2", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.FriendAll });

            _allspells.Add(new Spell() { Name = "ROYAL_MARCH_END", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });


            //<!--Artefact Level 1
            _allspells.Add(new Spell() { Name = "SPRINT_ARMR", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            //<!--Artefact Level 2
            _allspells.Add(new Spell() { Name = "RUN_SUPPORT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 7, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "RUN_SUPPORT_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 7, Type = "buff", Target = Target.FriendAll });


            //<!--
            //##############################################################################################################################
            //###################################################### GATHERER GEAR #########################################################
            //##############################################################################################################################



            //<!--Armor
            _allspells.Add(new Spell() { Name = "WINDSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "WINDSHIELD_EFFECT", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "HOTSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 10, CastRange = 9, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HOTSHIELD_HOT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HOTSHIELD_HIDEMOB_BONUS", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "HOTSHIELD_HIDEMOB_BONUS_CONDITION", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HOTSHIELD_HIDEMOB_BONUS_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });


            _allspells.Add(new Spell() { Name = "ROOTSHIELD", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 10, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ROOTSHIELD_SHIELD", Category = Category.Buff_damageshield, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ROOTSHIELD_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ROOTSHIELD_EFFECT2", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PURGINGSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 8, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PURGINGSHIELD_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "FLOWSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 8, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLOWSHIELD_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });



            //<!--Helmet
            _allspells.Add(new Spell() { Name = "TREETRUNKS", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "TREETRUNKS_EFFECT", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "BEARTRAP", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BEARTRAP_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "BEARTRAP_HIDEMOB_BONUS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "CC_IMMUNITY", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "SLOWSHIELD", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SLOWSHIELD_EFFECT", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MAGICMUSHROOM", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 90, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MAGICMUSHROOM_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MAGICMUSHROOM_INVISIBILITY", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Shoes
            _allspells.Add(new Spell() { Name = "FLEE", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLEE_SILENCE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLEE_EXPLOIT_PREVENTION", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "ETHERIAL_PATH", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 14, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "ETHERIAL_PATH_INVISIBILITY", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ETHERIAL_PATH_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "FLEE_MOB", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLEE_MOB_SILENCE", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FLEE_MOB_EXPLOIT_PREVENTION", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "PAINSPRINT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 50, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PAINSPRINT_EFFECT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "PAINSPRINT_PULSE", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "movement", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MOVEMENTSHIELD", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOVEMENTSHIELD_SHIELD", Category = Category.Buff_damageshield, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOVEMENTSHIELD_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOVEMENTSHIELD_REMOVE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });



            //<!--Old Spells
            //<!--Medium
            _allspells.Add(new Spell() { Name = "INSTANTSTEALTH2", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 7, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "TREMBLINGAURA2", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 13, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "TREMBLINGAURA2_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CLEANSESHIELD", Category = Category.Instant, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 14, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!-- ####################################################################################### 
            //<!-- ############## Prototype Helmet Spells Please ignore forum trolls  #################### 
            //<!-- ####################################################################################### 
            //<!--Light
            _allspells.Add(new Spell() { Name = "FOCUS_SPELLDMG", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BERSERKERSTANCE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Medium
            _allspells.Add(new Spell() { Name = "SUMMONER_CCR", Category = Category.Buff, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SUMMONER_CCR_EFFECT", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Self });
            //<!-- ####################################################################################### 
            //<!-- ############ Prototype Armor Rework Spells Please ignore forum trolls  ################ 
            //<!-- ####################################################################################### 
            _allspells.Add(new Spell() { Name = "SUMMONER_ENERGY", Category = Category.Heal, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "ENERGETIC", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BERSERKERAURA", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 10, CastRange = 0, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BERSERKERAURA_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Self });
            //<!-- ####################################################################################### 
            //<!-- ################################# P A S S I V E S ##################################### 
            //<!-- ####################################################################################### 
            //<!-- ####################################################################################### 
            //<!-- ################################# P A S S I V E S ##################################### 
            //<!-- ################################# Shared Generic ###################################### 
            //<!-- ####################################################################################### 

            //<!--Animation Test
            _allspells.Add(new Spell() { Name = "FLAILATTACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 0, EnergyCost = 0, CastRange = 3, Type = "movement", Target = Target.Enemy });
            //<!-- ####################################################################################### 
            //<!-- ############# Prototype Offhand Passives Please ignore forum trolls  ################## 
            //<!-- ####################################################################################### 
            //<!--Shield - Warrior



            //<!--Outdated






            _allspells.Add(new Spell() { Name = "PASSIVE_SHIELD_ENERGY_DRAIN_EFFECT", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "debuff", Target = Target.Enemy });


            //<!--Torch - Ranger

            _allspells.Add(new Spell() { Name = "PASSIVE_OFFHAND_MOVEMENT_SPEED_EFFECT", Category = Category.Movementbuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Outdated


            //<!--Book - Mage



            //<!--Outdated



            _allspells.Add(new Spell() { Name = "PASSIVE_OFFHAND_ENERGY_CHANCE_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 12, Type = "buff", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "PASSIVE_OFFHAND_REDUCE_CD_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            //<!--EMPA SECRET SPELLZ

            //<!--



            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_1", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_2", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_3", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_4", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            _allspells.Add(new Spell() { Name = "BOOM", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.3f, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.All });



            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });

            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_1", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_2", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_3", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "PASSIVE_CHARGES_4", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.All });



            _allspells.Add(new Spell() { Name = "BOOM", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.3f, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "BOOM_1", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "BOOM_2", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "BOOM_3", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "BOOM_4", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.All });
            //<!--***********************************************************
            //<!--************Spells for Consumeables * *********************
            //<!--***********************************************************
            //<!--FOOD
            _allspells.Add(new Spell() { Name = "FOOD_HEALTH_REG_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_HEALTH_REG_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_HEALTH_REG_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_CRAFTING_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_CRAFTING_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_CRAFTING_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_LOAD_GATHER_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_LOAD_GATHER_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_LOAD_GATHER_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_CASTSPEED_CDR_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_CASTSPEED_CDR_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_CASTSPEED_CDR_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_MAXHP_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_MAXHP_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_MAXHP_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_DMG_1", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_DMG_2", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "FOOD_BUFF_DMG_3", Category = Category.Foodbuff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 3, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            //<!--POTIONS
            _allspells.Add(new Spell() { Name = "POTION_HEAL_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_1_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_HEAL_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_2_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_2_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });


            _allspells.Add(new Spell() { Name = "POTION_HEAL_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_3_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_HEAL_3_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });



            //<!--Energy Potion
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_ENERGY_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });

            //<!--Revive Potion
            _allspells.Add(new Spell() { Name = "POTION_POISON_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_1_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "POTION_POISON_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_1_LEVEL1_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "POTION_POISON_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_2_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "POTION_POISON_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_2_LEVEL1_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "POTION_POISON_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_3_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "POTION_POISON_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 11, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_POISON_3_LEVEL1_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });

            //<!--Armor Potion
            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_RESISTANCE_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });



            //<!--Max Load Potion

            //<!--Slow Potion
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_1_LEVEL1_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_2_LEVEL1_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 11, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3_LEVEL1_EFFECT", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POTION_AOE_SLOW_3_LEVEL1_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Enemy });


            //<!--Maxload Potion
            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_1_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_2_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_3", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_MAXLOAD_3_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });


            //<!--Invisibility Potion
            _allspells.Add(new Spell() { Name = "POTION_INVISIBILITY", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_INVISIBILITY_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.6f, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "POTION_INVISIBILITY_LEVEL1", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 1.0f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "POTION_INVISIBILITY_LEVEL1_EFFECT", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.6f, RecastDelay = 40, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });




            //<!--*************************************************************************************************************************************************************
            //<!--*************************************************************************************************************************************************************
            //<!--======= MOB SPELLS ======= 

            //<!--*************************************************************************************************************************************************************

            //<!--*************************************************************************************************************************************************************

            //<!--Buff that removes autoattacks for Mobs for the whole combat
            _allspells.Add(new Spell() { Name = "MOB_NO_AUTOATTACK", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });

            //<!--GUARD TOWER PROTOTYPE
            _allspells.Add(new Spell() { Name = "GUARD_TOWER_AOE", Category = Category.Damage, CastingTime = 0, RecastDelay = 4, EnergyCost = 0, CastRange = 70, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "GUARD_TOWER_AOE_TIMER", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "GUARD_TOWER_AOE_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });

            //<!--permanent spell effect area test spell
            _allspells.Add(new Spell() { Name = "HEALING_ZONE", Category = Category.Heal, CastingTime = 0, RecastDelay = 30, EnergyCost = 23, CastRange = 15, Type = "heal", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HEALING_ZONE_EFFECT", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });

            //<!---Boss Buff
            _allspells.Add(new Spell() { Name = "BOSS_ENRAGE_BUFF_1", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BOSS_ENRAGE_BUFF_2", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BOSS_ENRAGE_BUFF_3", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Mini Boss Spells 
            _allspells.Add(new Spell() { Name = "SHOCK_MINIBOSS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.15f, RecastDelay = 30, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "WELLOFLIFE_MINIBOSS", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "WELLOFLIFEEFFECT_MINIBOSS", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "DEVASTATE_MINIBOSS", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 9.9f, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DEVASTATE_UNDEAD_MINIBOSS", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 9.9f, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "AIMEDSHOT_MINIBOSS", Category = Category.Damage, CastingTime = 3.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "AIMEDSHOT_CROSSBOW_MINIBOSS", Category = Category.Damage, CastingTime = 3.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "AIMEDSHOT_AXE_MINIBOSS", Category = Category.Damage, CastingTime = 3.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "REGEN_MINIBOSS", Category = Category.Heal, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 30, EnergyCost = 0, CastRange = 9.9f, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HEAL_MINIBOSS", Category = Category.Heal, CastingTime = 1.5f, HitDelay = 0.3f, RecastDelay = 10, EnergyCost = 0, CastRange = 9.9f, Type = "heal", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "AGGROSWITCH_MINIBOSS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SLEEP_MINIBOSS", Category = Category.Crowdcontrol, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "POWERUP_MINIBOSS", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AOECHANNEL_MINIBOSS_FIRE", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "AOECHANNEL_MINIBOSS_FROST", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!--Miniboss Totems

            //<!--Spawn Spells

            //<!--T2
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T2_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T2_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T2_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T3
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T3_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T3_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T3_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--Identifier & Selectors
            _allspells.Add(new Spell() { Name = "MOB_MINIBOSS_IDENTIFIER", Category = Category.Buff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MINIBOSS_TOTEM_HEAL", Category = Category.Buff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MINIBOSS_TOTEM_SKILLSHOT", Category = Category.Buff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MINIBOSS_TOTEM_LASER", Category = Category.Buff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_IDENTIFIER", Category = Category.Buff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            //<!--Healing Beam
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_CHANNEL_HEAL", Category = Category.Heal, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 25, CastRange = 12, Type = "heal", Target = Target.FriendOther });
            //<!--Skill Shot
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_SKILLSHOT", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 5, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_SKILLSHOT_EFFECT", Category = Category.Forcedmovement, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_SKILLSHOT_HIGHTIER", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 5, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_SKILLSHOT_EFFECT_HIGHTIER", Category = Category.Forcedmovement, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Laser Beam
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_LASER", Category = Category.Damage, CastingTime = 2, RecastDelay = 15, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_LASER_EFFECT", Category = Category.Damage, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_LASER_HIGHTIER", Category = Category.Damage, CastingTime = 2, RecastDelay = 15, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_TOTEM_LASER_HIGHTIER_EFFECT", Category = Category.Damage, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });

            //<!--################################################################################################################################################################################ 
            //<!--ANIMALS
            _allspells.Add(new Spell() { Name = "BEAR_SUNDERARMOR", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 20, EnergyCost = 33, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "BEAR_BASH", Category = Category.Crowdcontrol, CastingTime = 1.2f, HitDelay = 0.1f, RecastDelay = 10, EnergyCost = 24, CastRange = 4, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "BOAR_RAGE", Category = Category.Buff, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 45, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--
            _allspells.Add(new Spell() { Name = "BOAR_BERSERK", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 50, CastRange = 0, Type = "buff", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "WOLF_REND", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0, RecastDelay = 10, EnergyCost = 20, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HIDE_WOLF_HOWL", Category = Category.Crowdcontrol, CastingTime = 1.5f, RecastDelay = 20, EnergyCost = 20, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            //<!--
            _allspells.Add(new Spell() { Name = "WOLF_RAVAGE", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 50, CastRange = 4, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_HIDE_ANCIENTMAMMOTH_NOATTACK", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HIDE_ANCIENTMAMMOTH_ATTACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.9f, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Ground });

            //<!--################################################################################################################################################################################ 
            //<!--***************************
            //<!--CRITTER & GUARDIANS
            //<!--***************************
            //<!--CRITTER
            //<!--Cougar
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_COUGAR_ALLOW_BLEED", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_COUGAR_ALLOW_BLEED_REMOVEL", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 4, Type = "debuff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_COUGAR_BLEED", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 3.5f, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Enemy });
            //<!--Dryad
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_DRYAD_TENDRILS", Category = Category.Crowdcontrol, CastingTime = 1.2f, RecastDelay = 10, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_DRYAD_TENDRILS_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });

            //<!--Ore Elemental
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_OREELEMENTAL_IMPLOSION", Category = Category.Forcedmovement, CastingTime = 2.1f, HitDelay = 0.3f, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_OREELEMENTAL_IMPLOSION_EFFECT", Category = Category.Crowdcontrol, CastingTime = 1.5f, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Rock Elemental
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_ROCKELEMENTAL_TREMOR", Category = Category.Damage, CastingTime = 1.8f, RecastDelay = 15, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_ROCKELEMENTAL_TREMOR_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Enemy });

            //<!--Forest Spirit
            _allspells.Add(new Spell() { Name = "MOB_CRITTER_FORESTSPIRIT_PIERCING_ROOT", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 10, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Ground });
            //<!--GUARDIANS
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ENRAGE", Category = Category.Instant, CastingTime = 1.5f, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Self });
            //<!--Swamp
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_DEBUFF_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 11, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_DEBUFF_APPLY", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 11, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_DEBUFF", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 11, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_CLEAVE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 2, RecastDelay = 19, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_CLEAVE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_VOIDZONE_PULSE", Category = Category.Damage, CastingTime = 0, RecastDelay = 60, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_VOIDZONE", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_SWAMP_VOIDZONE_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 12, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            //<!--Forest
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_ROOT", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_ROOT_SEA", Category = Category.Crowdcontrol, CastingTime = 1.2f, RecastDelay = 15, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_ROOT_EFFECT", Category = Category.Crowdcontrol, CastingTime = 1.2f, RecastDelay = 15, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_ROOT_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 12, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_PIERCING", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 30, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_PIERCING_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_PIERCING_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_FOREST_PIERCING_DOT", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            //<!--Highland

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_ATTACK", Category = Category.Instant, CastingTime = 1.2f, HitDelay = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_EARTHQUAKE_ALLOWED", Category = Category.Damage, CastingTime = 0, HitDelay = 1.3f, RecastDelay = 40, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_EARTHQUAKE_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_EARTHQUAKE_CHANNEL", Category = Category.Damage, CastingTime = 0.4f, RecastDelay = 5, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_EARTHQUAKE", Category = Category.Damage, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.AllMobs });

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_SUMMON_APPLY", Category = Category.Damage, CastingTime = 2, RecastDelay = 40, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_SUMMON", Category = Category.Instant, CastingTime = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_SUMMON_EFFECT", Category = Category.Instant, CastingTime = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_SUMMON_EFFECT_VFX", Category = Category.Instant, CastingTime = 0, HitDelay = 0.34f, RecastDelay = 0, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_SUMMON_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_ROCKGIANT_KICK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 1.2f, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Mountain
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_CD", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_RESONANCE_CD", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_RESONANCE", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_RESONANCE_PULL", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_CRYSTAL_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_PUNCH", Category = Category.Instant, CastingTime = 0, HitDelay = 1.2f, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_PUNCH_SEA", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 8, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_CRYSTALSWARM_CAST", Category = Category.Damage, CastingTime = 2.45f, RecastDelay = 10, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_CRYSTALSWARM", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_CRYSTALSWARM_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 2.5f, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.AllPlayers });

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_WINDSTORM", Category = Category.Damage, CastingTime = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_OREGIANT_WINDSTORM_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 11, Type = "debuff", Target = Target.Enemy });

            //<!--Steppe
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_ATTACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.9f, RecastDelay = 3, EnergyCost = 0, CastRange = 13, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HIDE_ANCIENTMAMMOTH_ATTACK_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_SUMMON", Category = Category.Forcedmovement, CastingTime = 2.64f, RecastDelay = 20, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_SUMMON_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 6, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_AVALANCHE", Category = Category.Damage, CastingTime = 3, RecastDelay = 17, EnergyCost = 0, CastRange = 40, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_AVALANCHE_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 3, RecastDelay = 17, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_EFFECT", Category = Category.Forcedmovement, CastingTime = 3, RecastDelay = 17, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_A", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_B", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_C", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_D", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_E", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_F", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_G", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_H", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_GUARDIAN_MAMMOTH_PROJECTILE_I", Category = Category.Damage, CastingTime = 1, RecastDelay = 1, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });


            //<!--################################################################################################################################################################################ 
            //<!--Castle Lord
            _allspells.Add(new Spell() { Name = "CASTLELORD_SUMMONGUARDS", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CASTLELORD_SUMMONELITEGUARDS", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 6.0f, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "CASTLELORD_SWIPE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 20.0f, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Ground });

            //<!--################################################################################################################################################################################ 
            //<!--********************************
            //<!--DEMONS
            //<!--********************************
            //<!--Demon Boss
            //<!--new
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_FLAMEPILLAR", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 26, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_FLAMEPILLAR_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_FLAMEPILLAR_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 10, EnergyCost = 20, CastRange = 15, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });

            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM_WEAK", Category = Category.Damage, CastingTime = 1.8f, RecastDelay = 10, EnergyCost = 20, CastRange = 15, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM_EFFECT_WEAK", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_BEAM_SEA_WEAK", Category = Category.Damage, CastingTime = 0, RecastDelay = 13, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_FIREBALL", Category = Category.Damage, CastingTime = 0.66f, HitDelay = 0.1f, RecastDelay = 2, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.AllPlayers });

            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_MAW_FIREBOLT_PULSE", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_MAW_FIREBOLT_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_MAW_FIREBOLT_APPLY", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_MAW_FIREBOLT_LEFT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_MAW_FIREBOLT_RIGHT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.AllPlayers });

            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_INFERNO", Category = Category.Forcedmovement, CastingTime = 2.25f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_AGGRO_SWITCH", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_LAVABOMB", Category = Category.Crowdcontrol, CastingTime = 0.5f, HitDelay = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 35, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_BOSS_LAVABOMB_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 9.9f, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Anti exploit
            _allspells.Add(new Spell() { Name = "AGGROSWITCH_DEMONBOSS", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 0, CastRange = 50, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Demon Imp
            _allspells.Add(new Spell() { Name = "MOB_DEMON_IMP_SHRINK", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 17, CastRange = 9.9f, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DEMON_IMP_DEATH_AOE", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DEMON_IMP_DEATH", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "DEMON_SOLDIER_DEATH_AOE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DEMON_SOLDIER_DEATH", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.All });
            //<!--Demon Gatekeeper
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_CAST", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.1f, RecastDelay = 10, EnergyCost = 17, CastRange = 9.9f, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_BREATH", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_BREATH_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_BREATH_IMMUNITY", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.All });
            //<!--Spiked Demon
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_FIRESLASH", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.1f, RecastDelay = 15, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Enemy });
            //<!--Safezone Hellgate Boss 
            _allspells.Add(new Spell() { Name = "DEMON_BOSS_SAFEZONE_HELLGATE_PHASE_2", Category = Category.Buff, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "DEMON_BOSS_SAFEZONE_HELLGATE_CLEAVE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DEMON_BOSS_SAFEZONE_FIRECONE", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DEMON_BOSS_SAFEZONE_FIRECONE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 11, CastRange = 15, Type = "damage", Target = Target.Enemy });

            //<!--################################################################################################################################################################################ 
            //<!--***************************
            //<!--Heretics
            //<!--***************************

            //<!--Mortar Spells
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_COOLDOWN", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_CONE_A", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_CONE_B", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_CONE_C", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_CONE_D", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_CONE_E", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MORTAR_STUN_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--ballista line
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_LINE", Category = Category.Damage, CastingTime = 0.7f, RecastDelay = 3.6f, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_KNOCKBACK_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 10, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_EXP_LINE", Category = Category.Damage, CastingTime = 0.7f, RecastDelay = 3.6f, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_EXP_KNOCKBACK_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 10, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });


            //<!--HERETIC ARCHER
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_SNIPESHOT", Category = Category.Damage, CastingTime = 2.5f, RecastDelay = 15, EnergyCost = 6, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_ARCHER_CRIPPLINGSHOT", Category = Category.Debuff, CastingTime = 3.0f, HitDelay = 0.15f, RecastDelay = 18, EnergyCost = 25, CastRange = 12, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_ARCHER_CRITICALSHOT", Category = Category.Damage, CastingTime = 3.0f, RecastDelay = 20, EnergyCost = 50, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_ARCHER_POISONARROW", Category = Category.Damage, CastingTime = 3.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 50, CastRange = 12, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_KNOCKBACK", Category = Category.Damage, CastingTime = 1.7f, RecastDelay = 8, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_KNOCKBACK_EFFECT", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_KNOCKBACK_EXP", Category = Category.Damage, CastingTime = 1.7f, RecastDelay = 8, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_KNOCKBACK_EXP_EFFECT", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_MINIBOSS_CONE", Category = Category.Damage, CastingTime = 1.4f, RecastDelay = 11, EnergyCost = 20, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_ARCHER_MINIBOSS_CONE_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 11, EnergyCost = 20, CastRange = 8, Type = "damage", Target = Target.Enemy });

            //<!--HERETIC HEALER
            _allspells.Add(new Spell() { Name = "HERETIC_HEALER_HEAL", Category = Category.Heal, CastingTime = 3.0f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 25, CastRange = 12, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HERETIC_HEALER_CLEANSE", Category = Category.Heal, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 25, CastRange = 12, Type = "heal", Target = Target.FriendAll });
            _allspells.Add(new Spell() { Name = "HERETIC_HEALER_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 25, CastRange = 12, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_CHANNEL_HEAL", Category = Category.Heal, CastingTime = 0, HitDelay = 0.15f, RecastDelay = 5, EnergyCost = 25, CastRange = 12, Type = "heal", Target = Target.FriendOther });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_SPAWN_TOTEM_T4", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_SPAWN_TOTEM_T5", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_TOTEM_HEAL", Category = Category.Heal, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 15, EnergyCost = 5, CastRange = 15, Type = "heal", Target = Target.FriendOther });

            //<!--HERETIC MAGE
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_INCINERATE", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.3f, RecastDelay = 10, EnergyCost = 27, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_SHIELD", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 38, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_FIREBALL", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 35, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_FIREBALL_EFFECT", Category = Category.Debuff, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 35, CastRange = 9, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_FIRE_AOE", Category = Category.Damage, CastingTime = 2.35f, HitDelay = 0.15f, RecastDelay = 15, EnergyCost = 25, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HERETIC_MAGE_AOE", Category = Category.Damage, CastingTime = 1.8f, HitDelay = 0.2f, RecastDelay = 5, EnergyCost = 25, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MAGE_MINIBOSS_CONE", Category = Category.Damage, CastingTime = 1.6f, RecastDelay = 7, EnergyCost = 25, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_MAGE_MINIBOSS_CONE_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 10, EnergyCost = 10, CastRange = 8, Type = "damage", Target = Target.Enemy });

            //<!--HERETIC THIEF
            _allspells.Add(new Spell() { Name = "THIEF_SHARPSHOT", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.3f, RecastDelay = 10, EnergyCost = 50, CastRange = 9, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_THIEF_DASH", Category = Category.Movementbuff, CastingTime = 0.0f, RecastDelay = 5, EnergyCost = 15, CastRange = 20, Type = "movement", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_THIEF_HAMSTRING", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 25, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_INTERRUPT", Category = Category.Debuff, CastingTime = 0.4f, HitDelay = 0.4f, RecastDelay = 5, EnergyCost = 17, CastRange = 3, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_THIEF_MINDNUMBINGPOISON", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 25, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_THIEF_POISON", Category = Category.Debuff, CastingTime = 1.3f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 25, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_VETERAN_THIEF_POISON", Category = Category.Debuff, CastingTime = 0.7f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 25, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_THIEF_CRUSH", Category = Category.Crowdcontrol, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 6, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "THIEF_SHARPENSWORDS", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 60, EnergyCost = 50, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_THIEF_THROWNDAGGER", Category = Category.Damage, CastingTime = 1.2f, HitDelay = 0.2f, RecastDelay = 11, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_EXP_THIEF_SHADOWSTEP", Category = Category.Damage, CastingTime = 0.1f, RecastDelay = 15, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_THIEF_SHADOWSTEP", Category = Category.Damage, CastingTime = 0.1f, RecastDelay = 5, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_THIEF_SHADOWSTEP_SEA", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 5, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_THIEF_SHADOWSTEP_SEA_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 5, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });

            //<!--Tank
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TANK_SHIELD", Category = Category.Instant, CastingTime = 0, RecastDelay = 15, EnergyCost = 25, CastRange = 12, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TANK_CHANNEL", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TANK_REMOVE_SHIELD", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 12, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HERETIC_TANK_SHIELDSLAM", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 12, EnergyCost = 50, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HERETIC_TANK_CRUSH", Category = Category.Crowdcontrol, CastingTime = 1.7f, HitDelay = 0, RecastDelay = 5, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HERETIC_TANK_CRUSH_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 5, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "HERETIC_TANK_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 5, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "HERETIC_TANK_ENRAGE", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0.1f, RecastDelay = 10, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.Self });

            //<!--GIBSON & STOOGE SOLO SPELLS
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_AVALANCHE", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 13, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_AVALANCHE_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_ENRAGE", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_ENRAGE_POSSIBLE", Category = Category.Instant, CastingTime = 0, HitDelay = 2, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_CONFUSED", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 20, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_TRIGGER_SHIELD", Category = Category.Instant, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_SHIELD", Category = Category.Instant, CastingTime = 0, HitDelay = 1, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_ALIBI_CHANNEL", Category = Category.Buff, CastingTime = 0, RecastDelay = 5, EnergyCost = 7, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_VETERAN_GIBSON_ALIBI_CHANNEL", Category = Category.Buff, CastingTime = 0, RecastDelay = 5, EnergyCost = 7, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_SHIELD_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_DEATH_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_BOSS_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_STOOGE_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 10, EnergyCost = 10, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_KNOCKBACK_TRIGGER", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.FriendOther });

            //<!--OVERSEER SOLO SPELLS
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_FLAGELLATE", Category = Category.Damage, CastingTime = 1.7f, HitDelay = 0, RecastDelay = 10, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_FLAGELLATE_EFFECT", Category = Category.Forcedmovement, CastingTime = 1.5f, RecastDelay = 5, EnergyCost = 10, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_HAUL", Category = Category.Crowdcontrol, CastingTime = 0.6f, RecastDelay = 5, EnergyCost = 10, CastRange = 12, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_HAUL_PULL", Category = Category.Crowdcontrol, CastingTime = 0.6f, RecastDelay = 5, EnergyCost = 10, CastRange = 12, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_SWIRL", Category = Category.Damage, CastingTime = 0, HitDelay = 0.15f, RecastDelay = 15, EnergyCost = 10, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_DEATHSPELL", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_OVERSEER_IDENTIFIER", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });

            //<!--Gibson Veteran Spells
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_ATTACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.96f, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_GIBSON_ATTACK_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 7, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_SPAWN_TOTEM_KNOCKBACK", Category = Category.Damage, CastingTime = 1, RecastDelay = 30, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_SPAWN_TOTEM_TRIPLELASER", Category = Category.Damage, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_SPAWN_TOTEM_AUTOFIRE", Category = Category.Damage, CastingTime = 1, RecastDelay = 21, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });

            //<!--Knockback
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TOTEM_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 1.1f, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            //<!--Skill Shot
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TOTEM_AUTOFIRE", Category = Category.Damage, CastingTime = 0.6f, RecastDelay = 0.9f, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TOTEM_AUTOFIRE_EFFECT", Category = Category.Forcedmovement, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Laser Beam
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TOTEM_TRIPLELASER", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_TOTEM_TRIPLELASER_EFFECT", Category = Category.Damage, CastingTime = 1, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Lumberjack Boss
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_LUMBERJACK_WHIRLWIND", Category = Category.Damage, CastingTime = 1, HitDelay = 0.0f, RecastDelay = 17, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_LUMBERJACK_WHIRLWIND_SLOW", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_LUMBERJACK_WHIRLWIND_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_LUMBERJACK_STRIKE", Category = Category.Crowdcontrol, CastingTime = 1.6f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            //<!--Ballista Boss
            _allspells.Add(new Spell() { Name = "IS_LOCATOR", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_SPAWN", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 25, Type = "damage", Target = Target.Other });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_SPAWN_VETERAN", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 25, Type = "damage", Target = Target.Other });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_MINES", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.2f, RecastDelay = 21, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_MINES_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Other });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_VOLLEY", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 11, EnergyCost = 0, CastRange = 25, Type = "damage", Target = Target.Ground });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_VOLLEY_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Other });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_CONE", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 18.5f, EnergyCost = 25, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_CONE_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 10, CastRange = 0, Type = "damage", Target = Target.Other });

            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_MORTAR", Category = Category.Damage, CastingTime = 1.1f, RecastDelay = 11, EnergyCost = 0, CastRange = 25, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_MORTAR_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_BALLISTA_BOSS_MORTAR_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Other });

            //<!--################################################################################################################################################################################ 
            //<!--***************************
            //<!--MORGANA
            //<!--***************************
            //<!--Soldier
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_WHIRLWIND", Category = Category.Damage, CastingTime = 0, RecastDelay = 16, EnergyCost = 10, CastRange = 10, Type = "damage", Target = Target.Self });

            //<!--Crossbowman
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_CONE", Category = Category.Damage, CastingTime = 1.1f, RecastDelay = 10, EnergyCost = 15, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_CONE_CHANNEL", Category = Category.Damage, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_CONE_REMOVAL", Category = Category.Damage, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_CONE_EFFECT", Category = Category.Damage, CastingTime = 3, RecastDelay = 15, EnergyCost = 15, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_JUMP", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 10, CastRange = 12, Type = "movement", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CROSSBOWMAN_JUMP_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Cultist
            _allspells.Add(new Spell() { Name = "MORGANA_CULTIST_INCINERATE", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 10, EnergyCost = 27, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_CULTIST_MEZZ", Category = Category.Crowdcontrol, CastingTime = 3.0f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 71, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_CURSE", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 15, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MORGANA_IMP_SUMMON", Category = Category.Damage, CastingTime = 2, HitDelay = 0.4f, RecastDelay = 60, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T4_IMPS", Category = Category.Buff, CastingTime = 3, RecastDelay = 10, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T4_IMPS_VETERAN", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T5_IMPS", Category = Category.Buff, CastingTime = 3, RecastDelay = 10, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T5_IMPS_VETERAN", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T6_IMPS", Category = Category.Buff, CastingTime = 3, RecastDelay = 10, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T6_IMPS_VETERAN", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T7_IMPS", Category = Category.Buff, CastingTime = 3, RecastDelay = 10, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T7_IMPS_VETERAN", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T7_IMPS_ELITE", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 2, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T8_IMPS", Category = Category.Buff, CastingTime = 3, RecastDelay = 10, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T8_IMPS_VETERAN", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SUMMON_T8_IMPS_ELITE", Category = Category.Buff, CastingTime = 1.5f, RecastDelay = 2, EnergyCost = 10, CastRange = 15, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_BOMBARDIER_SEA", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 5, EnergyCost = 5, CastRange = 24, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_BOMBARDIER_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 5, EnergyCost = 5, CastRange = 24, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_INFESTOR", Category = Category.Debuff, CastingTime = 1.2f, RecastDelay = 11, EnergyCost = 5, CastRange = 15, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_INFESTOR_EFFECT", Category = Category.Debuff, CastingTime = 2, RecastDelay = 10, EnergyCost = 5, CastRange = 15, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_INFESTOR_ONDEATH", Category = Category.Debuff, CastingTime = 2, RecastDelay = 10, EnergyCost = 5, CastRange = 15, Type = "debuff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CULTIST_COOLDOWN", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.FriendAll });

            //<!--Miniboss Spells
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CULTIST_BOSS_SACRIFICE", Category = Category.Damage, CastingTime = 3, RecastDelay = 15, EnergyCost = 10, CastRange = 15, Type = "damage", Target = Target.FriendOther });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CULTIST_BOSS_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 2, EnergyCost = 0, CastRange = 15, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_AFTERFIGHT_REFRESH", Category = Category.Buff, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 15, Type = "buff", Target = Target.Ground });

            //<!--Morgana Standard Knight
            _allspells.Add(new Spell() { Name = "MORGANA_KNIGHT_SHIELDSLAM", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 12, EnergyCost = 50, CastRange = 4, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_KNIGHT_SHIELDWALL", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 24, EnergyCost = 50, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_KNIGHT_CRUSH", Category = Category.Crowdcontrol, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 2, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MORGANA_KNIGHT_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 55, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_KNIGHT_ENRAGE", Category = Category.Movementbuff, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_KNIGHT_CRUSH", Category = Category.Crowdcontrol, CastingTime = 1.8f, RecastDelay = 8, EnergyCost = 12, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_KNIGHT_ENRAGE", Category = Category.Instant, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 2, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_KNIGHT_ENRAGE_CD", Category = Category.Instant, CastingTime = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });

            //<!--BOSS MORGANA CHAMPION
            _allspells.Add(new Spell() { Name = "MORGANA_CHAMPION_HELLSPIKES", Category = Category.Damage, CastingTime = 2.1f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MORGANA_CHAMPION_FIRENOVA", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_CHAMPION_SILENCE", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.56f, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_CHAMPION_FIREBEAM", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_CHAMPION_FIREBEAM_EFFECT", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            //<!--#################### BOSS MORGANA HALF DEMON PRINCE ###################
            //<!--Phase 1
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_CRUSH_SPIRIT", Category = Category.Debuff, CastingTime = 0, HitDelay = 0.8f, RecastDelay = 5, EnergyCost = 0, CastRange = 12, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_HELLRIFT", Category = Category.Damage, CastingTime = 1.75f, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_HELLRIFT_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Phase 2
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_SPAWN_DEMONGATE_PHASE_2", Category = Category.Damage, CastingTime = 2.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 21, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_PULL", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 2.2f, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            //<!--Phase 3
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_SPAWN_DEMONGATE_PHASE_3", Category = Category.Damage, CastingTime = 2.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_LASERSWEEP", Category = Category.Damage, CastingTime = 2.0f, HitDelay = 0.5f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_LASERSWEEP_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MORGANA_DEMONPRINCE_DEATH", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Ground });
            //<!--Spawner
            _allspells.Add(new Spell() { Name = "MORGANA_PORTAL_SPAWN_DEMONGATE_PHASE_2", Category = Category.Damage, CastingTime = 5.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_PORTAL_SPAWN_DEMONGATE_PHASE_3", Category = Category.Damage, CastingTime = 5.0f, HitDelay = 0.0f, RecastDelay = 40, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MORGANA_PORTAL_DEATH", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 1, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Ground });
            //<!--################################################################################################################################################################################ 
            //<!--***************************
            //<!--Undead Start

            //<!--Cursed Specter
            _allspells.Add(new Spell() { Name = "SPECTER_CURSE", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 1.0f, EnergyCost = 75, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SPECTER_PURGE", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 10, EnergyCost = 0, CastRange = 4, Type = "debuff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SHADE_SPECTRALTOUCH", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 10, EnergyCost = 75, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SHADE_SPECTRALTOUCH_CHANNEL", Category = Category.Damage, CastingTime = 0, RecastDelay = 15, EnergyCost = 75, CastRange = 4, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "SHADE_SPAWN_SPECTRALTOUCH", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.5f, RecastDelay = 5, EnergyCost = 0, CastRange = 3, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SHADE_AURA", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SHADE_AURA_EFFECT", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 3, Type = "buff", Target = Target.Self });

            //<!--Spells for the event 
            _allspells.Add(new Spell() { Name = "SHADE_SPAWN", Category = Category.Damage, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 5, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "HARVESTER_SPAWN", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 45, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SHADOW_WALK", Category = Category.Buff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SUMMON_TALK", Category = Category.Buff, CastingTime = 3.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_DEATH", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 2.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Ground });
            //<!--Harvester prototype
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_NIGHTMARE", Category = Category.Crowdcontrol, CastingTime = 1.0f, HitDelay = 0.4f, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_SWEEPINGBLOW", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_MASSFEAR", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_PHASE_2", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_PHASE_3", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_HARVEST", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 41, CastRange = 6, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_HARVESTER_GHASTLYDISCHARGE", Category = Category.Damage, CastingTime = 2.5f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Ground });
            //<!-- HARVESTER OLD
            _allspells.Add(new Spell() { Name = "HARVESTER_DOOMSCYTHE", Category = Category.Damage, CastingTime = 1.7f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "HARVESTER_SUMMONSHADES_CHANNELED", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!-- Undead Mage
            _allspells.Add(new Spell() { Name = "UNDEAD_MAGE_FREEZE", Category = Category.Debuff, CastingTime = 1.5f, HitDelay = 0.3f, RecastDelay = 12, EnergyCost = 24, CastRange = 10, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_MAGE_AOE_SLOW", Category = Category.Debuff, CastingTime = 1.7f, HitDelay = 0.3f, RecastDelay = 12, EnergyCost = 36, CastRange = 10, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_MAGE_FROSTEXPLOSION", Category = Category.Debuff, CastingTime = 2.0f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 25, CastRange = 10, Type = "damage", Target = Target.Ground });
            //<!-- Undead Archers
            _allspells.Add(new Spell() { Name = "UNDEAD_MULTISHOT", Category = Category.Damage, CastingTime = 1.3f, RecastDelay = 5, EnergyCost = 9, CastRange = 11, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_MULTISHOT_EFFECT", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!-- Undead Archer Miniboss 
            _allspells.Add(new Spell() { Name = "UNDEAD_ARCHER_MINIBOSS_VOLLEY", Category = Category.Damage, CastingTime = 2.1f, HitDelay = 0, RecastDelay = 15, EnergyCost = 30, CastRange = 11, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_ARCHER_MINIBOSS_VOLLEY_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_ARCHER_DOT_SKILLSHOT", Category = Category.Debuff, CastingTime = 2, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_ARCHER_DOT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 15, EnergyCost = 10, CastRange = 11, Type = "debuff", Target = Target.AllPlayers });
            //<!-- Undead Mage Miniboss 
            _allspells.Add(new Spell() { Name = "UNDEAD_MAGE_MINIBOSS_SUMMON_T4", Category = Category.Damage, CastingTime = 2, RecastDelay = 30, EnergyCost = 10, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!-- Undead Puller 
            _allspells.Add(new Spell() { Name = "UNDEAD_PULL", Category = Category.Damage, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 8, EnergyCost = 0, CastRange = 25, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_PULL_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 22, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Undead Cursed Skeleton - Explosion 
            _allspells.Add(new Spell() { Name = "UNDEAD_EXPLODE_ALLOWED", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 3, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_EXPLODE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 23, CastRange = 1, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_EXPLODE_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 1.2f, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_EXPLODE_DEATH", Category = Category.Damage, CastingTime = 0, HitDelay = 2, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            //<!-- Undead Soldier
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_GRIEVOUSWOUND", Category = Category.Debuff, CastingTime = 0, HitDelay = 1.4f, RecastDelay = 9, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_GRIEVOUSWOUND_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Enemy });
            //<!-- Undead Soldier Miniboss
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY", Category = Category.Damage, CastingTime = 1.3f, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 14, CastRange = 5, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY_RIGHTSWING", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY_LEFTSWING", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 15, EnergyCost = 14, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY_IS_SWINGING", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_SOLDIER_MINIBOSS_CHARGED_FRENZY_REMOVAL", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.All });

            //<!--Brittle Skeletons Hotfix
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_SKELETON_SPEEDBUFF", Category = Category.Instant, CastingTime = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            //<!--Undead Puller Rework
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_PULLER_PULL", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 8, EnergyCost = 0, CastRange = 25, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_PULLER_CRUSH", Category = Category.Crowdcontrol, CastingTime = 1.7f, RecastDelay = 4, EnergyCost = 12, CastRange = 4, Type = "crowdcontrol", Target = Target.Ground });
            //<!--Undead Ghoul Boss Spells
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_CONSUME_DASH", Category = Category.Damage, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 40, Type = "damage", Target = Target.FriendOtherMobs });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_CONSUME", Category = Category.Damage, CastingTime = 1.4f, RecastDelay = 10, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_PLAGUE", Category = Category.Debuff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_PLAGUE_SEA", Category = Category.Debuff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "debuff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_PLAGUE_EFFECT", Category = Category.Debuff, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_HURL", Category = Category.Damage, CastingTime = 0.9f, HitDelay = 0.3f, RecastDelay = 6, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_BARF", Category = Category.Damage, CastingTime = 0.8f, RecastDelay = 14, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_HURL_EFFECT", Category = Category.Damage, CastingTime = 1.6f, RecastDelay = 1, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_DISEMBOWEL_READY", Category = Category.Damage, CastingTime = 0, RecastDelay = 19, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_DISEMBOWEL_DASH", Category = Category.Damage, CastingTime = 0.7f, RecastDelay = 19, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_DISEMBOWEL", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BOSS_DISEMBOWEL_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BARF", Category = Category.Damage, CastingTime = 1.4f, RecastDelay = 14, EnergyCost = 0, CastRange = 8, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GHOUL_BARF_EFFECT", Category = Category.Damage, CastingTime = 1, HitDelay = 0.2f, RecastDelay = 1, EnergyCost = 0, CastRange = 2, Type = "damage", Target = Target.Enemy });
            //<!--***************************
            //<!--**** Undead Necromancer ***
            //<!--***************************
            //<!-- Phase 1
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_T4_SUMMON_LESSER", Category = Category.Damage, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_T6_SUMMON_LESSER", Category = Category.Damage, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 15, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.Ground });
            //<!-- Phase 2
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_AOE_SLOW", Category = Category.Crowdcontrol, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 20, CastRange = 2.5f, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_AOE_SLOW_EFFECT", Category = Category.Crowdcontrol, CastingTime = 1.0f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 20, CastRange = 2.5f, Type = "buff", Target = Target.Enemy });
            //<!-- Phase Spells! 
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_PHASE_2", Category = Category.Buff, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_PHASE_3", Category = Category.Buff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!-- Identifier
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_IS_LOCATOR_A", Category = Category.Buff, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_IS_LOCATOR_B", Category = Category.Buff, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            //<!--Activation Spell
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_ALLOW_PORTAL", Category = Category.Buff, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 40, Type = "buff", Target = Target.FriendAll });
            //<!--Summon Portal Spells
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_SPAWN_T4_PORTAL", Category = Category.Buff, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_NECROMANCER_SPAWN_T6_PORTAL", Category = Category.Buff, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            //<!-- Portal Spells
            _allspells.Add(new Spell() { Name = "UNDEAD_PORTAL_SPAWN_T4_SKELETONS", Category = Category.Damage, CastingTime = 5.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_PORTAL_SPAWN_T6_SKELETONS", Category = Category.Damage, CastingTime = 5.0f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_PORTAL_DEATH", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 1, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Ground });
            //<!--***************************
            //<!--Undead General
            //<!--***************************
            //<!-- General 
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            //<!--Phase 1
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_CHANNEL", Category = Category.Instant, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 10, EnergyCost = 0, CastRange = 15, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_CHANNEL_EFFECT", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.All });
            //<!--Phase 2
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_PHASE_2_ALLOWED", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_PHASE_2", Category = Category.Instant, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GENERAL_EXPLOSION_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GENERAL_EXPLOSION", Category = Category.Damage, CastingTime = 2.1f, HitDelay = 0.1f, RecastDelay = 11, EnergyCost = 15, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_SWEEP", Category = Category.Damage, CastingTime = 0, HitDelay = 1, RecastDelay = 17, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_SWEEP_SOLO", Category = Category.Damage, CastingTime = 1.8f, RecastDelay = 17, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_SWEEP_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_GRIP", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 11, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_GRIP_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 11, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_GRIP_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Add Spells
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_DEAD_ONE", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_DEAD_TWO", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_FRENZY", Category = Category.Damage, CastingTime = 1, HitDelay = 0.16f, RecastDelay = 11, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_SWITCH", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_FRENZY_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_FRENZY_SEA_B", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_GENERAL_ADD_FRENZY_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            //<!--***************************
            //<!--****  Undead Governor  ****
            //<!--***************************
            //<!--Undead Governor Fire Pillar Array
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GOVERNOR_FIREARRAY_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 7, EnergyCost = 5, CastRange = 25, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GOVERNOR_FIREARRAY", Category = Category.Damage, CastingTime = 0.7f, RecastDelay = 7, EnergyCost = 5, CastRange = 25, Type = "damage", Target = Target.Ground });
            //<!--Undead Governor Heavy Slash
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_HEAVY_SLASH", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 8, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Undead Governor Whirling Strike
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GOVERNOR_WHIRL", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 11, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            //<!--Undead Governor Enrage
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_ENRAGE", Category = Category.Damage, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 13, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!--Undead Governor Forced Movement
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_INDICATION", Category = Category.Instant, CastingTime = 0, HitDelay = 4.5f, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_TRIGGER_T4", Category = Category.Crowdcontrol, CastingTime = 0.5f, HitDelay = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_TRIGGER_T7", Category = Category.Crowdcontrol, CastingTime = 0.5f, HitDelay = 0, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_T4", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_T7", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "UNDEAD_GOVERNOR_FORCED_MOVEMENT_CASTED", Category = Category.Crowdcontrol, CastingTime = 5, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GOVERNOR_SUMMON_T4", Category = Category.Instant, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 1, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_GOVERNOR_SUMMON_T7", Category = Category.Instant, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 1, EnergyCost = 0, CastRange = 10, Type = "damage", Target = Target.All });
            //<!--***************************
            //<!--  Undead Harvester Spells  
            //<!--***************************
            //<!--Anti Tank
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_TANK_DEBUFF", Category = Category.Debuff, CastingTime = 0, HitDelay = 1.2f, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "debuff", Target = Target.Enemy });
            //<!--Anti Healer
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_ENERGY_DRAIN", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            //<!--Anti Front Camp
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_CLEAVE", Category = Category.Damage, CastingTime = 1.7f, HitDelay = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_CLEAVE_EFFECT", Category = Category.Damage, CastingTime = 1.1f, HitDelay = 0.05f, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            //<!--Anti DPS
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_AREA_OF_DECAY", Category = Category.Damage, CastingTime = 3, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_AREA_OF_DECAY_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_AREA_OF_DECAY_SPELL_AREA", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "UNDEAD_HARVESTER_AREA_OF_DECAY_SPELL_AREA_EFFECT", Category = Category.Damage, CastingTime = 3, HitDelay = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });

            //<!--################################################################################################################################################################################ 
            //<!--***************************
            //<!--KEEPERS
            //<!--Berserker
            _allspells.Add(new Spell() { Name = "BERSERKER_ENRAGE", Category = Category.Movementbuff, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "BERSERKER_SUNDER", Category = Category.Debuff, CastingTime = 1.5f, HitDelay = 0.1f, RecastDelay = 15, EnergyCost = 3, CastRange = 4, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "BERSERKER_INTERRUPT", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 21, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_SMASH", Category = Category.Crowdcontrol, CastingTime = 1.5f, RecastDelay = 6, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_SMASH_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Anti Kite
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_DASH_SLOW", Category = Category.Damage, CastingTime = 1, HitDelay = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 15, Type = "movement", Target = Target.Enemy });
            //<!-- 50% Random 
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_WHIRLWIND_STANCE", Category = Category.Buff, CastingTime = 1, HitDelay = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 3, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_EARTHQUAKE_STANCE", Category = Category.Buff, CastingTime = 0, HitDelay = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 3, Type = "debuff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_WHIRLWIND", Category = Category.Damage, CastingTime = 1, HitDelay = 0.0f, RecastDelay = 12, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_WHIRLWIND_SPEED", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_WHIRLWIND_AOE_DMG", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_EARTHQUAKE", Category = Category.Damage, CastingTime = 2, HitDelay = 0, RecastDelay = 8, EnergyCost = 9, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_BERSERKER_EARTHQUAKE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            //<!--Axe Thrower
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_STUN", Category = Category.Crowdcontrol, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_POWERTHROW", Category = Category.Crowdcontrol, CastingTime = 3, HitDelay = 0.15f, RecastDelay = 6, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_RELOCATE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_RELOCATE_SLOW", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_AXETHROWER_GROUNDBREAKER", Category = Category.Damage, CastingTime = 1.8f, HitDelay = 0.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_AXETHROWER_GROUNDBREAKER_EFFECT", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_AXETHROWER_GROUNDBREAKER_EFFECT2", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--T4 Standard Druid
            _allspells.Add(new Spell() { Name = "KEEPER_DRUID_CD", Category = Category.Instant, CastingTime = 0, RecastDelay = 0.1f, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "DRUID_CURSE_PULSE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 9, EnergyCost = 5, CastRange = 12, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "DRUID_CURSE_SEA", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 9, EnergyCost = 5, CastRange = 12, Type = "crowdcontrol", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "DRUID_CURSE", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 9, EnergyCost = 5, CastRange = 12, Type = "crowdcontrol", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "DRUID_ROTTEN_GROUND_CAST", Category = Category.Crowdcontrol, CastingTime = 2, HitDelay = 0.15f, RecastDelay = 11, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "DRUID_ROTTEN_GROUND_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, RecastDelay = 1, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "DRUID_ROOT", Category = Category.Crowdcontrol, CastingTime = 1.5f, HitDelay = 0.15f, RecastDelay = 20, EnergyCost = 0, CastRange = 9, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--KEEPER Giant 
            //<!--general
            _allspells.Add(new Spell() { Name = "SPECIAL_NOATTACK", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "SPECIAL_GCD_2", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            //<!--melee
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_WARSTOMP_ALLOWED", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 5, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_WARSTOMP", Category = Category.Crowdcontrol, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 6, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_KICK", Category = Category.Crowdcontrol, CastingTime = 1.2f, HitDelay = 0.6f, RecastDelay = 5, EnergyCost = 10, CastRange = 6, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_KICK_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_PUNCH", Category = Category.Instant, CastingTime = 0, HitDelay = 1, RecastDelay = 2.5f, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_PUNCH_STANDARD", Category = Category.Instant, CastingTime = 0, HitDelay = 1, RecastDelay = 2.5f, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });
            //<!--range
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_STONE", Category = Category.Damage, CastingTime = 1, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 5, CastRange = 14, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_ROAR", Category = Category.Crowdcontrol, CastingTime = 0.72f, HitDelay = 0, RecastDelay = 14, EnergyCost = 0, CastRange = 6, Type = "crowdcontrol", Target = Target.Self });
            //<!--Christmas special
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_SNOWBALL", Category = Category.Damage, CastingTime = 1.2f, HitDelay = 0.2f, RecastDelay = 5, EnergyCost = 5, CastRange = 14, Type = "damage", Target = Target.Ground });
            //<!--*****************
            //<!--Spells for Mortar/Turret Giants
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_CD", Category = Category.Instant, CastingTime = 1.5f, RecastDelay = 1, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_CHARGES", Category = Category.Instant, CastingTime = 1.5f, RecastDelay = 5, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_EFFECT", Category = Category.Forcedmovement, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 15, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_B", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 2, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_C", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 2, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_D", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 2, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_E", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 2, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_TURRET_SILENCE", Category = Category.Debuff, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "debuff", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_MORTAR", Category = Category.Damage, CastingTime = 2, HitDelay = 0.2f, RecastDelay = 5, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SPECIAL_MORTAR_EFFECT", Category = Category.Crowdcontrol, CastingTime = 2, RecastDelay = 4, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Enemy });

            _allspells.Add(new Spell() { Name = "BUFFBOT_SPEED", Category = Category.Movementbuff, CastingTime = 0, HitDelay = 0, RecastDelay = 1, EnergyCost = 1, CastRange = 9, Type = "buff", Target = Target.All });
            //<!-- Keeper Seer Spells
            _allspells.Add(new Spell() { Name = "KEEPER_SEER_SHOT", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.2f, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SEER_SHOT_EFFECT", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 0, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_SEER_BLACKHOLE", Category = Category.Crowdcontrol, CastingTime = 0, HitDelay = 0, RecastDelay = 30, EnergyCost = 14, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_SEER_BLACKHOLE_EFFECT", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 1, EnergyCost = 30, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_SEER_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 1.0f, HitDelay = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 0, Type = "crowdcontrol", Target = Target.Self });
            //<!-- Keeper giant boss spells 
            //<!-- Phase 1 
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_KICK", Category = Category.Forcedmovement, CastingTime = 0.0f, HitDelay = 0.4f, RecastDelay = 8, EnergyCost = 5, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_STONE_2", Category = Category.Forcedmovement, CastingTime = 1.2f, HitDelay = 0.15f, RecastDelay = 5, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_SELFBUFF", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_SELFBUFF_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.AllPlayers });
            //<!-- Phase 2 
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_WARSTOMP", Category = Category.Crowdcontrol, CastingTime = 2, RecastDelay = 20, EnergyCost = 10, CastRange = 6, Type = "crowdcontrol", Target = Target.Ground });
            //<!-- Phase 3 
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_GIANT_BOSS_PHASE_2", Category = Category.Forcedmovement, CastingTime = 1.2f, HitDelay = 0.0f, RecastDelay = 2, EnergyCost = 5, CastRange = 14, Type = "buff", Target = Target.Self });
            //<!-- Keeper chieftain boss spells 
            //<!-- Phase 1 
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_DASH_TARGET_SLOW", Category = Category.Crowdcontrol, CastingTime = 1, RecastDelay = 15, EnergyCost = 0, CastRange = 12, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_ENRAGE", Category = Category.Buff, CastingTime = 1, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_VOLLEY", Category = Category.Damage, CastingTime = 1.4f, HitDelay = 0.15f, RecastDelay = 7, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_VOLLEY_EFFECT", Category = Category.Crowdcontrol, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_WEAK_LIGHTNING", Category = Category.Crowdcontrol, CastingTime = 1.8f, HitDelay = 0.15f, RecastDelay = 10, EnergyCost = 0, CastRange = 16, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_WHIRLWIND", Category = Category.Damage, CastingTime = 1.9f, RecastDelay = 10, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_WHIRLWIND_SLOW", Category = Category.Damage, CastingTime = 0.0f, HitDelay = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_WHIRLWIND_EFFECT", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_SMASH", Category = Category.Crowdcontrol, CastingTime = 1.9f, HitDelay = 0.0f, RecastDelay = 6, EnergyCost = 5, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_SMASH_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_CHIEFTAIN_PHASE_3", Category = Category.Buff, CastingTime = 1, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Self });
            //<!--Earth mother prototype
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_CHARGE_EARTHMOTHER", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 15, Type = "movement", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_ARCANESTONE", Category = Category.Damage, CastingTime = 3, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 9, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_THUNDERSLAM_1", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_THUNDERSLAM_2", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 30, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_THUNDERSLAM_3", Category = Category.Forcedmovement, CastingTime = 2, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_FURY", Category = Category.Damage, CastingTime = 0, HitDelay = 0.4f, RecastDelay = 15, EnergyCost = 0, CastRange = 4, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_PHASE_2", Category = Category.Instant, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_EARTHMOTHER_PHASE_3", Category = Category.Instant, CastingTime = 1.5f, HitDelay = 0.0f, RecastDelay = 10, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            //<!--Trash
            _allspells.Add(new Spell() { Name = "UNPROVEN_FEMALE_RELOCATE", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.15f, RecastDelay = 7, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.All });
            //<!--Mushroom Gatherer
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_MUSHROOM_GATHERER_ATTACK", Category = Category.Damage, CastingTime = 1.52f, HitDelay = 0.1f, RecastDelay = 3, EnergyCost = 0, CastRange = 11, Type = "damage", Target = Target.Enemy });
            //<!--Keeper Seer
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_A", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_B", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 27, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_C", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_D", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_E", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_F", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_G", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_H", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_I", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_J", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_K", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_L", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STORM_HIT_M", Category = Category.Damage, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STRIKE", Category = Category.Damage, CastingTime = 1.2f, HitDelay = 0, RecastDelay = 7, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_LIGHTNING_STRIKE_EFFECT", Category = Category.Damage, CastingTime = 1.2f, HitDelay = 0, RecastDelay = 10, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_SEER_REGROWTH", Category = Category.Forcedmovement, CastingTime = 0.8f, HitDelay = 0, RecastDelay = 30, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.All });
            //<!--Keeper Axethrower
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_MINIBOSS_ENRAGE", Category = Category.Buff, CastingTime = 0, HitDelay = 0.9f, RecastDelay = 30, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_MINIBOSS_GROUNDBREAKER", Category = Category.Damage, CastingTime = 1.6f, HitDelay = 0.5f, RecastDelay = 7, EnergyCost = 10, CastRange = 15, Type = "crowdcontrol", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_MINIBOSS_GROUNDBREAKER_EFFECT", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "KEEPER_AXETHROWER_MINIBOSS_GROUNDBREAKER_EFFECT2", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Keeper Berserker
            _allspells.Add(new Spell() { Name = "MOB_BERSERKER_ENRAGE", Category = Category.Buff, CastingTime = 0, HitDelay = 0.96f, RecastDelay = 15, EnergyCost = 0, CastRange = 0, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_BERSERKER_WHIRLWIND", Category = Category.Damage, CastingTime = 1.1f, RecastDelay = 15, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_BERSERKER_WHIRLWIND_SLOW", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_KEEPER_BERSERKER_WHIRLWIND_EFFECT", Category = Category.Damage, CastingTime = 0.0f, RecastDelay = 7, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Self });



            //<!--################################################################################################################################################################################ 
            //<!--PROMO BOB
            _allspells.Add(new Spell() { Name = "MOB_UNIQUE_VOLUNTEER_HIT", Category = Category.Instant, CastingTime = 0, RecastDelay = 0.5f, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_UNIQUE_WEAPONMASTER_IDLE", Category = Category.Buff, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 20, Type = "buff", Target = Target.Self });

            //<!--#############################
            //<!--###    Miniboss Totems    ###
            //<!--#############################
            //<!--Spawn Spells
            //<!--MORGANA
            //<!-- T4
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_MORGANA_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_MORGANA_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_MORGANA_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T5
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_MORGANA_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_MORGANA_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_MORGANA_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T6
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_MORGANA_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_MORGANA_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_MORGANA_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T7
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_MORGANA_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_MORGANA_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_MORGANA_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T8
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_MORGANA_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_MORGANA_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_MORGANA_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--KEEPER
            //<!--T4
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_KEEPER_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_KEEPER_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_KEEPER_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T5
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_KEEPER_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_KEEPER_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 16, EnergyCost = 0, CastRange = 30, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_KEEPER_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 16, EnergyCost = 0, CastRange = 30, Type = "buff", Target = Target.Enemy });
            //<!--T6
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_KEEPER_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_KEEPER_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_KEEPER_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T7
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_KEEPER_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_KEEPER_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_KEEPER_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T8
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_KEEPER_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_KEEPER_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_KEEPER_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--UNDEAD
            //<!--T4
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_UNDEAD_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_UNDEAD_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T4_UNDEAD_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T5
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_UNDEAD_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_UNDEAD_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T5_UNDEAD_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T6
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_UNDEAD_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_UNDEAD_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T6_UNDEAD_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T7
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_UNDEAD_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_UNDEAD_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T7_UNDEAD_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            //<!--T8
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_UNDEAD_TOTEM_HEAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 40, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_UNDEAD_TOTEM_LASER", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_SPAWN_T8_UNDEAD_TOTEM_SKILLSHOT", Category = Category.Instant, CastingTime = 0, RecastDelay = 20, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.Enemy });

            //<!--new Morgana Boss Spells
            //<!--Bloodhound
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_BLOODHOUND_HOWL", Category = Category.Crowdcontrol, CastingTime = 1.5f, RecastDelay = 20, EnergyCost = 20, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });
            //<!--Knight
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_KNIGHT_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, HitDelay = 0.7f, RecastDelay = 10, EnergyCost = 5, CastRange = 6, Type = "crowdcontrol", Target = Target.Enemy });
            //<!--Soldier
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_WHIRLWIND_NEW", Category = Category.Damage, CastingTime = 0, RecastDelay = 16, EnergyCost = 10, CastRange = 10, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_WHIRLWIND_PULL", Category = Category.Crowdcontrol, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 15, Type = "crowdcontrol", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_MINIBOSS_DASH", Category = Category.Instant, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 3, Type = "movement", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_MINIBOSS_KNOCKBACK", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 3, Type = "movement", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_MINIBOSS_REMOVAL", Category = Category.Instant, CastingTime = 0, RecastDelay = 17, EnergyCost = 0, CastRange = 3, Type = "movement", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_SOLDIER_MINIBOSS_HEAL", Category = Category.Heal, CastingTime = 4, RecastDelay = 17, EnergyCost = 10, CastRange = 3, Type = "heal", Target = Target.AllMobs });
            //<!--Torturer
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_GCD", Category = Category.Instant, CastingTime = 0, HitDelay = 0.0f, RecastDelay = 20, EnergyCost = 0, CastRange = 20, Type = "crowdcontrol", Target = Target.Self });

            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_KNOCKBACK_EFFECT", Category = Category.Forcedmovement, CastingTime = 0, RecastDelay = 5, EnergyCost = 10, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_KNOCKBACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.81f, RecastDelay = 3, EnergyCost = 10, CastRange = 5, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_AURA", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_AURA_EFFECT", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_SEAL_PULL", Category = Category.Crowdcontrol, CastingTime = 0.95f, HitDelay = 0.2f, RecastDelay = 11, EnergyCost = 10, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_SEAL_DOT", Category = Category.Debuff, CastingTime = 2.5f, RecastDelay = 7, EnergyCost = 5, CastRange = 5, Type = "debuff", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_SEAL_SILENCE", Category = Category.Crowdcontrol, CastingTime = 2, RecastDelay = 9, EnergyCost = 5, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_SEAL_SILENCE_EFFECT", Category = Category.Crowdcontrol, CastingTime = 2, RecastDelay = 10, EnergyCost = 5, CastRange = 5, Type = "crowdcontrol", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_TORTURER_SEAL_COMPLETE", Category = Category.Crowdcontrol, CastingTime = 0.6f, RecastDelay = 5, EnergyCost = 10, CastRange = 15, Type = "crowdcontrol", Target = Target.Enemy });

            //<!--Morgana Royal Knight
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_ROYALKNIGHT_IDENTIFIER", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_ROYALKNIGHT_ALIVE", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_ROYALKNIGHT_CHANNEL", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 30, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_ROYALKNIGHT_SHIELD", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 30, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_ROYALKNIGHT_BUFF", Category = Category.Instant, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 30, Type = "buff", Target = Target.AllMobs });
            //<!--Fang
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_ONDEAD", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_ATTACK_TIMER", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_ATTACK_EFFECT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 2, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_ATTACK_RIGHT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 2, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_ATTACK_LEFT", Category = Category.Damage, CastingTime = 0, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_TARGETRESET", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_SPLITTINGCLAW_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 3, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_FANG_SPLITTINGCLAW", Category = Category.Damage, CastingTime = 0, HitDelay = 0.7f, RecastDelay = 9, EnergyCost = 0, CastRange = 30, Type = "damage", Target = Target.Ground });
            //<!--Magistra
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_ONDEAD", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_ATTACK", Category = Category.Damage, CastingTime = 1.5f, HitDelay = 0.3f, RecastDelay = 3, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_SHIELD", Category = Category.Buff, CastingTime = 2, RecastDelay = 8, EnergyCost = 5, CastRange = 15, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_PULSE", Category = Category.Instant, CastingTime = 0, RecastDelay = 8, EnergyCost = 5, CastRange = 1, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_TRIGGER", Category = Category.Instant, CastingTime = 0, RecastDelay = 8, EnergyCost = 5, CastRange = 1, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_MAGISTRA_EXPLOSION", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_VOID_SEA", Category = Category.Damage, CastingTime = 1.2f, RecastDelay = 12, EnergyCost = 5, CastRange = 24, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_VOID_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 5, EnergyCost = 5, CastRange = 24, Type = "damage", Target = Target.Enemy });
            //<!--Raven
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_RAVEN_ONDEAD", Category = Category.Instant, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 1, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_RAVEN_ATTACK", Category = Category.Damage, CastingTime = 0, RecastDelay = 5, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_RAVEN_DEBUFF_CD", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_RAVEN_DEBUFF", Category = Category.Debuff, CastingTime = 1.2f, RecastDelay = 11, EnergyCost = 5, CastRange = 15, Type = "debuff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_MORGANA_RAVEN_PULSING_EFFECT", Category = Category.Damage, CastingTime = 2, RecastDelay = 1, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.All });
            //<!--new hellgate demon spells
            _allspells.Add(new Spell() { Name = "MOB_DEMON_IMP_MAGMABALL", Category = Category.Damage, CastingTime = 1.3f, RecastDelay = 7, EnergyCost = 10, CastRange = 25, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_IMP_MAGMABALL_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 1, EnergyCost = 3, CastRange = 15, Type = "damage", Target = Target.AllPlayers });

            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_ATTACK_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 2.5f, EnergyCost = 0, CastRange = 6, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_ATTACK", Category = Category.Damage, CastingTime = 0, HitDelay = 0.2f, RecastDelay = 2.5f, EnergyCost = 0, CastRange = 5, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_GATEKEEPER_ATTACK", Category = Category.Buff, CastingTime = 0, HitDelay = 0.5f, RecastDelay = 5, EnergyCost = 0, CastRange = 5, Type = "buff", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_CONE", Category = Category.Damage, CastingTime = 1.5f, RecastDelay = 12.5f, EnergyCost = 25, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_SPIKED_CONE_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 0, EnergyCost = 25, CastRange = 0, Type = "damage", Target = Target.Other });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_VETERAN_MINIBOSS_ONDEATH", Category = Category.Heal, CastingTime = 0, HitDelay = 0, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_MINIBOSS_ONDEATH_PULSING", Category = Category.Heal, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_MINIBOSS_ONDEATH_EFFECT", Category = Category.Heal, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "heal", Target = Target.AllPlayers });
            _allspells.Add(new Spell() { Name = "MOB_DEMON_MINIBOSS_ONDEATH_BUFF", Category = Category.Buff, CastingTime = 0.0f, RecastDelay = 0, EnergyCost = 0, CastRange = 10, Type = "buff", Target = Target.All });
            //<!--new heretic expedition spells
            //<!--healer
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_SHIELD", Category = Category.Buff_damageshield, CastingTime = 1, RecastDelay = 3, EnergyCost = 6, CastRange = 15, Type = "buff", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_BIGHEAL", Category = Category.Heal, CastingTime = 3, RecastDelay = 7, EnergyCost = 5, CastRange = 9, Type = "heal", Target = Target.AllMobs });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_SPAWN_TOTEM_T6", Category = Category.Instant, CastingTime = 0, RecastDelay = 30, EnergyCost = 7, CastRange = 15, Type = "buff", Target = Target.All });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_HEALEXPLOSION", Category = Category.Heal, CastingTime = 2, RecastDelay = 11, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_HERETIC_HEALER_HEALEXPLOSION_EFFECT", Category = Category.Heal, CastingTime = 0, HitDelay = 1, RecastDelay = 0, EnergyCost = 0, CastRange = 0, Type = "heal", Target = Target.AllMobs });

            //<!--Undead Ranged Skeleton
            _allspells.Add(new Spell() { Name = "MOB_UNDEAD_SKELETON_RANGED_ATTACK", Category = Category.Damage, CastingTime = 1.25f, HitDelay = 0.2f, RecastDelay = 2.5f, EnergyCost = 0, CastRange = 12, Type = "damage", Target = Target.Enemy });
            //<!--Chest Alert Spell
            _allspells.Add(new Spell() { Name = "MOB_CHEST_ALERT", Category = Category.Instant, CastingTime = 0, RecastDelay = 0.5f, EnergyCost = 0, CastRange = 0, Type = "damage", Target = Target.Self });
            //<!--Watchtower Guard Spells
            _allspells.Add(new Spell() { Name = "MOB_WATCHTOWER_SOLDIER_WARCRY", Category = Category.Instant, CastingTime = 0, HitDelay = 0.6f, RecastDelay = 20, EnergyCost = 5, CastRange = 1, Type = "damage", Target = Target.Self });
            _allspells.Add(new Spell() { Name = "MOB_WATCHTOWER_ARCHER_POISON", Category = Category.Damage, CastingTime = 0.7f, HitDelay = 0.1f, RecastDelay = 5, EnergyCost = 5, CastRange = 20, Type = "damage", Target = Target.AllPlayers });
            //<!--Spell Zones
            //<!--Morgana Torture Chamber
            _allspells.Add(new Spell() { Name = "SPELLZONE_MORGANA_SPIKES", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 15, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "SPELLZONE_MORGANA_SPIKES_EFFECT", Category = Category.Instant, CastingTime = 0, RecastDelay = 1, EnergyCost = 0, CastRange = 1, Type = "damage", Target = Target.AllPlayers });
            //<!--Quick Spell Tests
            _allspells.Add(new Spell() { Name = "QUICK_TEST", Category = Category.Damage, CastingTime = 1, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Ground });
            _allspells.Add(new Spell() { Name = "QUICK_TEST_PULSE", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
            _allspells.Add(new Spell() { Name = "QUICK_TEST_SEA", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.All });
            _allspells.Add(new Spell() { Name = "QUICK_TEST_EFFECT", Category = Category.Damage, CastingTime = 0, RecastDelay = 3, EnergyCost = 0, CastRange = 20, Type = "damage", Target = Target.Enemy });
        }
    }

}

