using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Runtime.CompilerServices;

using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;



namespace Script
{
    public class Program
    {
        public static string ScriptName = "oHealer";

        [Configuration("oHealer")]
        public static Configuration Settings { get; set; }

        public enum Intentions
        {
            ScriptExit,
            Initializing,
            Follow,
            Heal
        };

        public static Intentions Intention;

        public static RplayerInstance fPlayer = null;
        public static long fPlayerID = 0;
        public static float fMaxDist = 0;
        public static float fHPPercent = 0.90f;

        public static void Main()
        {
            Log.Clear();
            Log.Write("Script has started.");

            Intention = Intentions.Initializing;

            if(String.IsNullOrEmpty(Settings.CharToFollow))
            {
                Log.Write("Error - Must supply character name to follow");
                return;
            }

            if(!GameData.IsOnline)
            {
                Log.Write("Error - Make sure you are logged into a character");
                return;
            }

            var Players = GameData.Players;

            Log.Write("Searching for character to follow");

            if(Players == null)
            {
                Log.Write("Fatal Error:  Players returned null / No Players Found");
                return;
            }

            if(!String.IsNullOrEmpty(Settings.HealPercentage))
            {
                Log.Write("Didn't configure Heal %.  Using 90%");
            }
            else
            {
                Log.Write("Only healing characters who fall below " + Settings.HealPercentage);
                fHPPercent = float.Parse(Settings.HealPercentage.TrimEnd(new char[] { '%', ' ' })) / 100f;                
            }

            try
            {
                foreach (var player in Players)
                {
                    if (String.Equals(player.Name, Settings.CharToFollow, StringComparison.OrdinalIgnoreCase))
                    {
                        fPlayerID = player.Id;
                        break;
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Write(ex.Message);
            }

            if(fPlayerID == 0)
            {
                Log.Write("Error - Can't find the player [" + Settings.CharToFollow + "] specified.");
                return;
            }
            
            fPlayer = GameData.GetRPlayer(fPlayerID);

            Log.Write(String.Format("Focus Character: [{0}]  ID: [{1}]", fPlayer.Name, fPlayerID));

            ConcurrentTaskManager.CreateService(() =>
            {
                if (Settings.TryInterrupt == true)
                {
                    var Mobs = GameData.Mobs;

                    var Interrupt = Mobs
                    .Where(x => x.IsChanneling == true)
                    .FirstOrDefault();

                    if (Interrupt != null)
                    {
                        Log.Write("Interrupting {0}", Interrupt.Name);
                        ConcurrentTaskManager.GameRun(() =>
                        {
                            if (Player.Location.DistanceXZ(Interrupt.Location) <= Player.AttackRange)
                            {
                                Player.Select(Interrupt.Id);
                                Player.DefaultAttack();
                            }
                        });

                        Thread.Sleep(150);

                        ConcurrentTaskManager.GameRun(() => { Player.StopAllActions(); }); Thread.Sleep(25);
                    }
                }

                if (Settings.HealAll == true)
                {
                    fPlayer = GameData.Players
                    .Where(x => x.IsInLocalPlayerParty == true)
                    .Where(x => x.CurrentHP < (x.MaxHP * fHPPercent))
                    .FirstOrDefault();
                }

                if (fPlayer == null && Intention == Intentions.Heal)
                    Intention = Intentions.Follow;

                if(Settings.HealAll == false || fPlayer == null)
                    fPlayer = GameData.GetRPlayer(fPlayerID);

                if (!GameData.IsOnline)
                {
                    Log.Write("Game is offline");
                    return;
                }

                if (fPlayer == null)
                {
                    Log.Write("Can't find players.");
                    return;
                }

                if (Intention == Intentions.Initializing)
                {
                    ConcurrentTaskManager.GameRun(() => { fMaxDist = (float)Spell.Range(0); }); Thread.Sleep(25);

                    Intention = Intentions.Follow;
                }

                else if(Intention == Intentions.Heal)
                {

                    if (GameData.PlayerLocation.DistanceXZ(fPlayer.Location) > fMaxDist)
                    {
                        Intention = Intentions.Follow;
                        return;
                    }

                    if (fPlayer.CurrentHP >= (fPlayer.MaxHP * fHPPercent) && GameData.MyHP >= (GameData.MyMaxHP * fHPPercent))
                    {
                        Intention = Intentions.Follow;
                    }
                    else
                    {
                        if(GameData.IsMounted)
                        {
                            ConcurrentTaskManager.GameRun(() =>
                            {
                                Mount.MountDismount();
                            });

                            Thread.Sleep(100);
                        }

                        if (fPlayer.CurrentHP < (fPlayer.MaxHP * fHPPercent))
                        {
                            ConcurrentTaskManager.GameRun(() =>
                            {
                                if (!Player.IsCasting && !Player.IsChanneling)
                                {
                                    //var obj = Player.GetObjectByID(fPlayerID);
                                    var obj = fPlayer.Object;

                                    if (Spell.CanCast(0) == true && Settings.UseQ == true)
                                        Spell.CastSlotObj(0, obj);
                                    else if (Spell.CanCast(1) == true && Settings.UseW == true)
                                        Spell.CastSlotObj(1, obj);
                                    if (Spell.CanCast(2) == true && Settings.UseE == true)
                                        Spell.CastSlotObj(2, obj);
                                }
                            });

                            Thread.Sleep(300);
                        }
                        else if(GameData.MyHP < (GameData.MyMaxHP * fHPPercent))
                        {
                            ConcurrentTaskManager.GameRun(() =>
                            {
                                if (!Player.IsCasting && !Player.IsChanneling)
                                {
                                    var obj = Player.Object;
                                    //var obj = Player.GetObjectByID((long)Player.ID);

                                    if (Spell.CanCast(0) == true && Settings.UseQ == true)
                                        Spell.CastSlotObj(0, obj);
                                    else if (Spell.CanCast(1) == true && Settings.UseW == true)
                                        Spell.CastSlotObj(1, obj);
                                    if (Spell.CanCast(2) == true && Settings.UseE == true)
                                        Spell.CastSlotObj(2, obj);
                                }
                            });
                        }

                    }

                }
                else if(Intention == Intentions.Follow)
                {
                    if (Settings.MountBeforeMove)
                        Mounter.Mount();

                    if(!Settings.MountBeforeMove || (Settings.MountBeforeMove == true && GameData.IsMounted == true))
                    {
                        var currDist = GameData.PlayerLocation.DistanceXZ(fPlayer.Location);
                        if (currDist > fMaxDist && Program.Settings.AutoMove)
                        {
                            var fLoc = fPlayer.Location;
                            ConcurrentTaskManager.GameRun(() =>
                            {
                                Vector3 newLoc = Vector3.DistanceAdjust(Player.Location, fLoc, currDist - fMaxDist);
                                //var newLoc = WorldPath.FindNextWalkPoint(Player.Location, fLoc, currDist - fMaxDist);
                                Player.Move(newLoc);
                            });

                            Thread.Sleep(100);

                            return;
                        }
                        else if (currDist <= fMaxDist)
                        {
                            Intention = Intentions.Heal;
                        }
                    }
                }

            }, ScriptName, 500);
            

        } // End Main()



    }
}
