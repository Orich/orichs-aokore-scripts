﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;

namespace Script
{
    public class Configuration
    {

        [Category("CONFIG")]
        [DisplayName("Char To Follow")]
        public string CharToFollow { get; set; }

        [Category("CONFIG")]
        [DisplayName("Auto Move?")]
        public bool AutoMove { get; set; }

        [Category("CONFIG")]
        [DisplayName("Mount before move?")]
        public bool MountBeforeMove { get; set; }

        [Category("CONFIG")]
        [DisplayName("Heal entire party?")]
        public bool HealAll { get; set; }

        [Category("CONFIG")]
        [DisplayName("Heal at %")]
        public string HealPercentage { get; set; }

        [Category("SPELLS")]
        [DisplayName("Use First Spell To Heal (Q)?")]
        public bool UseQ { get; set; }

        [Category("SPELLS")]
        [DisplayName("Use Second Spell To Heal (W)?")]
        public bool UseW { get; set; }

        [Category("SPELLS")]
        [DisplayName("Use Third Spell To Heal (E)?")]
        public bool UseE { get; set; }

        [Category("MISC")]
        [DisplayName("Attempt to interrupt?")]
        public bool TryInterrupt { get; set; }

        [Category("MISC")]
        [DisplayName("Enable Debug Log")]
        public bool Debugging { get; set; }



        public static string WaypointPath = System.Environment.CurrentDirectory + "\\Plugins\\Cache\\WayPoint\\";

        public class PathData
        {
            public Vector3[] Path { get; set; }
        }

        public static T FromXml<T>(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }



    }
}
