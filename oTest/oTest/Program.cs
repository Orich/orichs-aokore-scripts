using System.Linq;
using AOKore.Game;
using AOKore.Script;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System;
using System.Threading;
using AOKore.Concurrent;

namespace Script
{
    public class Program
    {
        public static Conf Settings { get; set; }


        public static void MyCastEventHandler(object sender, CastEventEventArgs e)
        {
            try
            {
                Type t = sender.GetType();
                Type t2 = null;

                if(e.Receiver != null)
                    t2 = e.Receiver.GetType();

                    if (t != null)
                        Log.Write("> Cast Event Handler from {0} . {1} : ", t.Namespace, t.Name);

                    if (t2 != null)
                        Log.Write("> Cast Receiver Obj  from {0} . {1} : ", t2.Namespace, t2.Name);

                    if (e != null)
                    {
                        Log.Write("  > Source: [{0}] Type  : [{1}]", e.Mob.SourceName, e.CastType);
                        Log.Write("  > Spell : [{0}] Target: [{1}]", e.Mob.SpellCasted, e.Mob.TargetId);
                        if (e.CastLocation != null)
                        {
                            Log.Write("  > Loc   : [{0},{1},{2}]", e.CastLocation.X, e.CastLocation.Y, e.CastLocation.Z);
                            // 0, -44, 0  value for target
                        }
                    }


                // OnCastStart
                // OnCastFinished
                // OnSpellAdded      off cooldown?
                // OnSpellHit
                // OnSpellRemoved    cooldown?
                // OnChannelingHit   (he hit me)
                // OnChannelingEnd
                // 
                // MOB_HERETIC_TANK_01
                // MOB_HERETIC_TANK_SHIELD          +12 physical armor, 2.5s
                // MOB_HERETIC_TANK_CHANNEL
                // HERETIC_TANK_CRUSH               crowdcontrol , range 5    (OnCastStart)
                // MOB_HERETIC_TANK_REMOVE_SHIELD   
                // 
            }
            catch (Exception ea)
            {
                Log.Write("> Error: " + ea.Message);
            }
        }
        /*
        public static void Main()
        {
            Log.Clear();
            Log.Write("Test v1.0");
            /*
            //Mob.ScheduleAttackVfxEventHandler
            Mob.MobCastEvent += MyCastEventHandler;

            //Mob.MobAttack += MyVfxEventHandler;

            var Timeout = DateTime.Now.AddSeconds(10);

            while (DateTime.Now < Timeout)
                Thread.Sleep(1000);

            //Mob.MobAttack -= MyVfxEventHandler;
            Mob.MobCastEvent -= MyCastEventHandler;


            ConcurrentTaskManager.GameRun(() =>
            {
                Log.Write("spell name: {0}", Spell.Name((byte)0));
            });
            ***

            ConcurrentTaskManager.GameRun(() =>
            {
                Log.Write("spell name: {0}", Spell.Name((byte)0));
            });

        }
        */

            /*
        public static void Main()
        {
            Log.Clear();

            Log.Write("TEST 1");
            ConcurrentTaskManager.GameRun(() =>
            {
                AOKore.Game.Mount.MountDismount();
            });

            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(250);
                ConcurrentTaskManager.GameRun(() =>
                {
                    Log.Write("{0}", AOKore.Game.Mount.IsCasting);
                });
            }

        }
        */

        public static void Main()
        {
            Log.Clear();
            Log.Write("TEST 2");
            Spells.PopulateSpells();

            Mounter.Dismount();
            
            var spells = GameData.Spells;

            foreach(var spell in spells)
            {
                Log.Write("Spell: [{0}]", spell);
            }

            var myspells = Spells.MySpells;

            Log.Write("MySpells # = {0}", myspells.Count());

            foreach(var spl in myspells)
            {
                Log.Write("SPELL: {0}", spl.Name);
            }
        }
    }
}