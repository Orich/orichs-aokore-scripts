﻿using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Script
{
    public static class Combat
    {
        public static CombatConfigFlags ConfigFlags = 0;

        private static volatile bool _isattackedbyplayers = false;
        private static volatile bool _isattackedbymobs = false;
        private static volatile bool _ishuntmobs = false;

        private static volatile bool _killThread = false;

        private static List<MobInstance> _attackermobs = new List<MobInstance>();
        private static object _attackmobslock = new object();

        private static List<MobInstance> _huntmobs = new List<MobInstance>();
        private static object _huntmobslock = new object();

        private static List<RplayerInstance> _playermobs = new List<RplayerInstance>();
        private static object _playermobsblock = new object();

        //private static object _currTarget = null;

        private static void _attack(long id)
        {
            GameData.SelectTarget(id);

            var mob = GameData.GetMob(id);

            if (mob == null)
                return;

            if (GameData.PlayerLocation.Distance(mob.Location) > GameData.AttackRange)
            {
                var vect = Vector3.DistanceAdjust(GameData.PlayerLocation, mob.Location, GameData.AttackRange * 0.95f);
                Mover.MoveTo(vect);
            }

            GameData.Attack();
        }

        private static void _kill(long id)
        {
            MobInstance target = GameData.GetMob(id);
            GameData.SelectTarget(target.Id);

            do
            {
                if (target == null || target.IsDead)
                    break;

                if (GameData.IsCastingOrChanneling)
                {
                    Thread.Sleep(100);
                    continue;
                }

                if ((target.CurrentHP <= 100f || GameData.MyEP < 10) && !target.IsDead)
                    _attack(target.Id);
                else if(!target.IsDead)
                {
                    Log.Debug2("Hunter: > Casting next available.");
                    if (!Spells.CastNextAvailable(target))
                        _attack(target.Id);
                }

                target = GameData.GetMob(target.Id);
            } while (target != null && !target.IsDead);
        }

        public static void Hunt()
        {
            if (!GameData.IsOnline || !_ishuntmobs)
                return;

            var curLoc = GameData.PlayerLocation;
            MobInstance target;

            GameData.StopAllActions();

            do
            {
                if (_isattackedbyplayers || !GameData.IsOnline)
                    return;

                lock (_huntmobslock)
                {
                    target = _huntmobs
                                .OrderBy(x => curLoc.DistanceXZ(x.Location))
                                .ThenBy(x => x.CurrentHP)
                                .ThenBy(x => x.MaxHP)
                                .FirstOrDefault();
                }

                if (target == null)
                {
                    lock(_attackmobslock)
                    {
                        if (_attackermobs.Any())
                        {
                            Log.Debug("Hunter:  Hunt mobs are gone, dealing with attackers.");
                            target = _attackermobs.First();
                        }
                        else
                            return;
                    }
                }

                target = GameData.GetMob(target.Id);
                if (target == null || target.IsDead)
                {
                    Thread.Sleep(250);
                    continue;
                }

                if (!_isattackedbymobs)
                {
                    if(!GameData.IsMounted)
                        Spells.Recover();
                    
                    if (GameData.AttackRange > GameData.PlayerLocation.DistanceXZ(target.Location))
                    {
                        Mounter.Mount();
                        if (!Mover.MoveWithinRange(target.Location, GameData.AttackRange * 0.90f))
                            return;

                        while (GameData.IsMoving)
                            Thread.Sleep(100);

                        Mounter.Dismount();
                    }
                    else if(GameData.IsMounted)
                    {
                        Mounter.Dismount();
                        Spells.Recover();
                    }

                    Thread.Sleep(500);

                    Log.Write("Hunter: Executing opener, Target {0}, ID:{1}", target.Name, target.Id);
                    var SpellOrder = Spells.Build_Opener();

                    Spells.CastSequence(SpellOrder, target);
                }
                else
                {
                    Mounter.Dismount();
                }

                Log.Write("Hunter: Killing {0}, ID:{1}.", target.Name, target.Id);

                _kill(target.Id);

            } while (_ishuntmobs);

            Spells.Recover();

        }

        public static void Defend()
        {
            if (!GameData.IsOnline)
                return;

            var curLoc = GameData.PlayerLocation;
            MobInstance target;

            GameData.StopAllActions();

            lock (_attackmobslock)
            {
                target = _attackermobs
                            .OrderBy(x => curLoc.DistanceXZ(x.Location))
                            .ThenBy(x => x.CurrentHP)
                            .ThenBy(x => x.MaxHP)
                            .FirstOrDefault();
            }

            if (target == null)
                return;

            Mounter.Dismount();

            _kill(target.Id);
        }

        public static void FlushMobs()
        {
            lock (_attackmobslock)
            {
                _attackermobs.Clear();
            }
            lock(_playermobsblock)
            {
                _playermobs.Clear();
            }
            lock(_huntmobslock)
            {
                _huntmobs.Clear();
            }
        }


        #region Scan Service
        public static void CreateScanService()
        {
            ConcurrentTaskManager.CreateService(() =>
            {
                if (!_killThread && GameData.IsOnline)
                {
                    try
                    {
                        Scanner();
                    }
                    catch (Exception ex)
                    {
                        Log.Debug("Combat Scanner: {0}", ex.Message);
                    }
                }
            }, Constants.ScriptName + "_CombatScanner", 250);
        }

        private static void Scanner()
        {
            if (_killThread || !GameData.IsOnline || GameData.IsInCity)
            {
                //Log.Debug2("Scanner Idle: killThread={0},IsOnline={1},IsInCity={2}", _killThread, GameData.IsOnline, GameData.IsInCity);
                return;
            }

            var currLoc = GameData.PlayerLocation;
            var currID = GameData.PlayerID;

            var mobs = GameData.Mobs
                //.Where(m => m.IsAttacking && m.TargetId == currID)
                .Where(m => m.TargetId == currID)
                .Where(m => !m.IsDead)
                .OrderBy(m => m.CurrentHP)
                .ThenBy(m => m.MaxHP);

            if (mobs == null || !mobs.Any())
            {
                _isattackedbymobs = false;
                lock(_attackmobslock)
                {
                    _attackermobs.Clear();
                }
            }
            else
            {
                _isattackedbymobs = true;

                lock (_attackmobslock)
                {
                    _attackermobs.Clear();
                    _attackermobs.AddRange(mobs);
                }
            }

            var players = GameData.Players
                .Where(m => m.TargetId == currID || m.LastCastTarget == currID)
                .Where(m => !m.IsDead);

            if (players == null || !players.Any())
            {
                lock (_playermobsblock)
                    _playermobs.Clear();
                _isattackedbyplayers = false;
            }
            else
            {
                lock (_playermobsblock)
                {
                    _playermobs.Clear();
                    _playermobs.AddRange(players);
                    _isattackedbyplayers = true;
                }
            }

            if (Program.Settings.HideHuntCondition)
            {
                var huntmobs = GameData.Mobs
                    .Where(m => m.Location.DistanceXZ(currLoc) <= Program.Settings.RangeSearch)
                    .Where(m => m.SourceName.Contains("HIDE"))
                    .Where(m => !m.SourceName.Contains("GUARD"))
                    .Where(m => !m.IsDead)
                    .Where(m => !m.IsAttacking || (m.IsAttacking && m.TargetId == GameData.PlayerID))
                    .Where(m => Program.Settings.HideHarvestTier.Contains((float)(m.Tier + (float)(m.RareState / 10f))))
                    .OrderBy(m => m.Location.DistanceXZ(currLoc))
                    .ThenByDescending(m => m.MaxHP);

                if (huntmobs.Any())
                {
                    lock (_huntmobslock)
                    {
                        _huntmobs.Clear();
                        _huntmobs.AddRange(huntmobs);
                        _ishuntmobs = true;
                    }
                }
                else
                {
                    lock(_huntmobslock)
                    {
                        _huntmobs.Clear();
                    }
                    _ishuntmobs = false;
                }
            }

        }
#endregion

        public static bool IsAttacked
        {
            get
            {
                return (_isattackedbymobs || _isattackedbyplayers);
            }
        }

        public static bool IsMobsAttacking
        {
            get
            {
                return _isattackedbymobs;
            }
        }

        public static bool IsPlayersAttacking
        {
            get
            {
                return _isattackedbyplayers;
            }
        }

        public static bool IsHuntmobs
        {
            get
            {
                return _ishuntmobs;
            }
        }


        [Flags]
        public enum CombatConfigFlags
        {
            None = 0,
            Defense = 1,
            Hunt = 1 << 2,
            All = Defense | Hunt
        }

        private static bool Is(this CombatConfigFlags current, CombatConfigFlags value)
        {
            return (current & value) == value;
        }


#region Dispose
        public static void Dispose()
        {
            _killThread = true;

            _attackermobs.DisposeAll();
            _attackermobs = null;

            _huntmobs.DisposeAll();
            _huntmobs = null;

            _playermobs.DisposeAll();
            _playermobs = null;
        }

        private static void DisposeAll(this IEnumerable set)
        {
            foreach (Object obj in set)
            {
                IDisposable disp = obj as IDisposable;
                if (disp != null) { disp.Dispose(); }
            }
        }
#endregion

    }

}
