using System;
using System.Collections.Generic;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using System.Linq;
using System.Collections;

namespace Script
{
    public enum PathLabels
    {
        HarvestPath = 100,    // Label, Priority ... less = higher priority
        ToHarvestPath = 200,
        ToCityPath = 300
    };


    public static class Mover
    {
        private static volatile bool _killThread = false;

        private static float _minDist = 3.0f;       // Minimum distance from last waypoint to consider successfully moved
        private static int _timeout = 1500;         // How many milliseconds after move is considered timeout

        private static DropOutStack<oVector> _pathhistory = new DropOutStack<oVector>(10);
        private static Object _lockpathhistory = new Object();

        private static List<KeyValuePair<PathLabels, wpPath>> _WP_Paths = new List<KeyValuePair<PathLabels, wpPath>>();
        private static wpPath currPath = null;
        private static PathLabels _Focused_Path;
        private static bool temporaryPath = false;
        private static int? _tmpPathIndex = null;

        public static void CreateScanService()
        {
            ConcurrentTaskManager.CreateService(() =>
            {
                if (!_killThread && GameData.IsOnline)
                {
                    try
                    {
                        Scanner();
                    }
                    catch (Exception ex)
                    {
                        Log.Debug(ex.Message);
                    }
                }
                else if (_killThread)
                {
                    Thread.CurrentThread.Abort();
                }
            }, Constants.ScriptName + "_MoveScanner", 125);
        }

        private static void Scanner()
        {
            if (_killThread)
                return;

            var pLoc = GameData.PlayerLocation;

            lock(_lockpathhistory)
            {
                var prev = _pathhistory.Peek();

                if (prev == null)
                {
                    _pathhistory.Push(new oVector(pLoc, GameData.MapName));
                    return;
                }

                if(prev.DistanceXZ(pLoc) > 0.50f || prev.Distance(pLoc) > 0.75f)
                {
                    _pathhistory.Push(new oVector(pLoc, GameData.MapName));
                }
            }
        }

        // The Dispose() and DisposeAll() functions are here to help Mono/Unity/AOKore along with garbage collection
        public static void Dispose()
        {
            _killThread = true;
            foreach (var path in _WP_Paths)
                path.Value.Dispose();
            _WP_Paths.DisposeAll();
            _pathhistory.DisposeAll();
            _WP_Paths = null;
            _pathhistory = null;
        }

        private static void DisposeAll(this IEnumerable set)
        {
            foreach (Object obj in set)
            {

                IDisposable disp = obj as IDisposable;
                if (disp != null) { disp.Dispose(); }
            }
        }

        [Flags]
        private enum TimeoutFlags
        {
            None        = 0,
            Reverse     = 1     ,
            Pathfind    = 1 << 1,
            Next        = 1 << 2,
            Clickables  = 1 << 3,
            ReversePath = 1 << 4,

            All = Reverse | Pathfind | Next | ReversePath
        }

        static bool Is(this TimeoutFlags current, TimeoutFlags value)
        {
            return (current & value) == value;
        }

        private class wpPath
        {
            public List<oVector> Path = new List<oVector>();
            private oVector _currWP;
            private int _currIndex = 0;
            private oVector _prevWP;
            private bool _forward = true;
            private float? _origdist = null;
            private bool _isLoop = false;
            public TimeoutFlags StuckFlags = 0;

            private DateTime? _lastMoveAttempt;

            #region Constructors
            public wpPath(List<Vector3> path, List<String> pathmap, bool isloop)
            {
                for(int i=0; i < path.Count; i++)
                {
                    Path.Add(new oVector(path[i], pathmap[i]));
                }

                _isLoop = isloop;

                init();
            }

            public wpPath(List<Vector3> path, string mapname)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    Path.Add(new oVector(path[i], mapname));
                }

                _isLoop = false;

                init();
            }

            public wpPath(List<oVector> path)
            {
                Path = new List<oVector>(path);

                _isLoop = false;

                init();
            }

            private void init()
            {
                var loc = GameData.PlayerLocation;
                _forward = true;
                _currWP = Path[0];
                _prevWP = new oVector(loc, GameData.MapName);
                _currIndex = 0;
                _origdist = Distance(loc, _currWP);

                _lastMoveAttempt = DateTime.Now;
            }
            #endregion

            public void Dispose()
            {
                Path.DisposeAll();
                Path = null;
            }

            public oVector PeekNext()
            {
                int tmpIndex = _currIndex;

                if (_forward)
                    tmpIndex++;
                else
                    tmpIndex--;

                if (tmpIndex < 0)
                    tmpIndex = 1;
                else if (tmpIndex == Path.Count)
                    tmpIndex = Path.Count - 2;

                return Path[tmpIndex];
            }

            public oVector PeekPrevious()
            {
                int tmpIndex = _currIndex;

                if (_forward)
                    tmpIndex--;
                else
                    tmpIndex++;

                if (tmpIndex < 0)
                    tmpIndex = 1;
                else if (tmpIndex == Path.Count)
                    tmpIndex = Path.Count - 2;

                return Path[tmpIndex];
            }

            public oVector PeekForward()
            {
                int tmpIndex = _currIndex + 1;

                if (tmpIndex == Path.Count)
                    return null;

                return Path[tmpIndex];
            }

            public oVector PeekBackward()
            {
                int tmpIndex = _currIndex - 1;

                if (tmpIndex < 0)
                    return null;

                return Path[tmpIndex];
            }

            public bool Next()
            {
                if (!_isLoop)
                    return Forward();

                if(_forward)
                {
                    if (!Forward())
                    {
                        _forward = false;
                        return Backward();
                    }
                }
                else
                {
                    if (!Backward())
                    {
                        _forward = true;
                        return Forward();
                    }

                }

                return true;
            }

            public bool Previous()
            {
                if (!_isLoop || _forward)
                    return Backward();

                return Forward();
            }

            public bool Forward()
            {
                _currIndex++;

                if(_currIndex == Path.Count())
                {
                    _currIndex--;
                    _lastMoveAttempt = null;
                    return false;
                }

                _prevWP = _currWP;
                _currWP = Path[_currIndex];
                _lastMoveAttempt = DateTime.Now;
                _origdist = Distance(GameData.PlayerLocation, _currWP);

                return true;
            }

            public bool Backward()
            {
                _currIndex--;

                if (_currIndex < 0)
                {
                    _currIndex++;
                    _lastMoveAttempt = null;
                    return false;
                }

                _prevWP = _currWP;
                _currWP = Path[_currIndex];
                _lastMoveAttempt = DateTime.Now;
                _origdist = Distance(GameData.PlayerLocation, _currWP);

                return true;
            }

            public bool Reached(Vector3 currLocation)
            {
                return (Distance(currLocation, _currWP) <= _minDist);
            }

            public bool IsTimeout
            {
                get
                {
                    return (_lastMoveAttempt != null && DateTime.Now > ((DateTime)_lastMoveAttempt).AddMilliseconds(_timeout));
                }
            }

            public bool IsLoop
            {
                get
                {
                    return _isLoop;
                }
            }

            public bool IsAtEnd
            {
                get
                {
                    return ((_currIndex >= Path.Count-1) && !_isLoop);
                }
            }

            public bool IsNextDiffMap
            {
                get
                {
                    return !(PeekNext().Map.Equals(CurrentWP.Map));
                }
            }

            public bool IsCloser(Vector3 loc)
            {
                var mindist = 1.0f;
                if (temporaryPath && _WP_Paths.Single(x => x.Key == _Focused_Path).Value.StuckFlags.Is(TimeoutFlags.ReversePath))
                    mindist = 0.4f;

                return ((_origdist - Distance(loc, _currWP)) >= mindist);
            }

            public void Reset()
            {
                init();
            }

            public bool SetClosest(oVector position)
            {
                _prevWP = _currWP;
                _currWP = null;

                this._currWP = this.Path
                    .Where(x => x.Map.Equals(position.Map))
                    .OrderBy(x => x.DistanceXZ(position))
                    .FirstOrDefault();

                if (_currWP == null)
                {
                    Log.Debug2("FATAL: Can't find a waypoint nearby.");
                    return false;
                }

                _currIndex = Path
                    .FindIndex(x => x.Equals(_currWP));

                _lastMoveAttempt = DateTime.Now;
                _origdist = Distance(GameData.PlayerLocation, _currWP);

                Log.Debug2("Closest: I={0},W={1},D={2},M={3},F={4},FWD={5}", _currIndex, _currWP.ToString(), _origdist, _currWP.Map, _Focused_Path, _forward);

                return true;
            }

            public oVector CurrentWP
            {
                get
                {
                    return _currWP;
                }
            }

            public void ResetStuck()
            {
                currPath.StuckFlags = 0;
                ResetTimeout();
            }
            public void ResetTimeout()
            {
                _lastMoveAttempt = DateTime.Now;
                _origdist = Distance(GameData.PlayerLocation, _currWP);
            }

        }

        public static PathLabels CurrentPath
        {
            get
            {
                return _Focused_Path;
            }
        }

        public static bool IsCompleted
        {
            get
            {
                return currPath.IsAtEnd;
            }
        }

        public static bool IsTemporary
        {
            get
            {
                return temporaryPath;
            }
        }

        public static bool MoveFar()
        {
            if(_tmpPathIndex == null)
            {


            }
            return false;
        }

        public static bool MoveNext()
        {
            if (!GameData.IsOnline)
                return false;

            if (IsCompleted)
            {
                if(temporaryPath == true)
                {
                    var thispath = _WP_Paths.Single(x => x.Key == _Focused_Path).Value;
                    if (thispath.StuckFlags.Is(TimeoutFlags.ReversePath))
                    {
                        Log.Debug("Anti-Stuck: Reversing path completed.");
                        EndTemporaryPath();
                        currPath.ResetTimeout();
                    }
                    else
                    {
                        Log.Debug2("Temporary path completed.");
                        SetFocusPath(_Focused_Path);
                    }
                    return true;
                }

                return false;
            }

            var currLoc = GameData.PlayerLocation;
            if (!currPath.CurrentWP.Map.Equals(GameData.MapName))
            {
                Log.Write("Mover: New Map Detected.");

                while (!GameData.HasPlayer)
                    Thread.Sleep(2000);
                Thread.Sleep(3000);

                if (temporaryPath)
                {
                    SetFocusPath(_Focused_Path);
                    return false;
                }
                currPath.SetClosest(new oVector(GameData.PlayerLocation, GameData.MapName));
                return false;
            }

            if (currPath.Reached(currLoc)) // if we've reached the last waypoint
            {
                currPath.StuckFlags = 0;
                Log.Debug("Mover: Waypoint Reached.");
                if (currPath.IsNextDiffMap)                // and the next waypoint is a different map
                {
                    if (GameData.IsMoving)
                        return false;

                    Log.Debug("No Exit: The next waypoint is a map change.");
                    var prevWP = currPath.PeekBackward();
                    var currWP = currPath.CurrentWP;
                    var nextWP = currPath.PeekForward();

                    /*
                    if (!temporaryPath)
                    {
                        // PortalInstance exitloc = GameData.FindMapExit(nextWP.Map);
                        var Portal = GameData.Portals
                                    .Where(x => x.DestinationName.Equals(nextWP.Map))
                                    .FirstOrDefault();

                        
                        if (Portal != null)
                        {
                            var wp = new Vector3(Portal.Location.X, currLoc.Y, Portal.Location.Z);
                            // Log.Debug("No Exit: Next WP = {0}", wp);
                            //GameData.SetMapWaypoint(wp);
                            _move(new Vector3(Portal.Location.X, currLoc.Y, Portal.Location.Z));

                            Thread.Sleep(500);
                            oThread.EndScript();

                            return true;
                        }
                        else
                        {
                            Log.Debug("No Exit: Can't seem to locate the portal.");
                        }
                    }
                    */

                    if (prevWP == null || currWP == null)
                    {
                        Log.Debug("Fatal Error: prevWP or currWP are Null.");
                        return false;
                    }

                    oVector newWP = null;

                    // TODO:  Add support for exits and Portal.QuickListLocation

                    for (float dist = -12f; dist < 0; dist += 1.0f)
                    {
                        newWP = new oVector(Vector3.DistanceAdjust(prevWP, currWP, dist), GameData.MapName);
                        if (GameData.CanMove(newWP))
                            break;
                    }

                    Log.Write("No Exit: Trying new trajectory into last waypoint.");

                    bool ret = _move(newWP);

                    var timeout = DateTime.Now.AddSeconds(3);
                    while (!GameData.MapName.Equals(nextWP.Map) && DateTime.Now < timeout)
                    {
                        ret = _move(newWP);
                        Thread.Sleep(1000);
                    }

                    return ret;
                }
                else
                {
                    bool stuckflag = _WP_Paths.Single(x => x.Key == _Focused_Path).Value.StuckFlags.Is(TimeoutFlags.ReversePath);
                    bool ret = false;

                    if (temporaryPath && stuckflag)
                    {
                        do
                        {
                            ret = _move();

                            if (currPath.IsNextDiffMap)
                            {
                                Log.Debug2("Mover: Next Waypoint is Different Map. [1]");
                                return false;
                            }

                        } while (currPath.Next());

                        return ret;
                    }
                    else
                    {
                        do
                        {
                            if (!currPath.Next())
                            {
                                Log.Debug2("Mover: No Next Available.");
                                break;
                            }

                            if (currPath.IsNextDiffMap)
                            {
                                Log.Debug2("Mover: Next Waypoint is Different Map. [2]");
                                return false;
                            }

                        } while (currPath.CurrentWP.DistanceXZ(currLoc) < 3f);

                        return _move();
                    }

                    //Log.Debug2("Mover: Next Waypoint.");
                }
            }
            else                                                // if we haven't reached the current waypoint
            {
                if (currPath.IsTimeout)                // and we've timed out
                {
                    if (currPath.IsCloser(currLoc))    // but we've gotten closer
                    {
                        Log.Debug2("Mover: Getting closer");
                        currPath.ResetStuck();           // reset timeout (resets stuckflag)
                        return _move();
                    }
                    else                                                    // and we aren't closer
                    {
                        if (!currPath.StuckFlags.Is(TimeoutFlags.Reverse))       // and we haven't tried reverse yet
                        {
                            if (temporaryPath)  // and we're in a temporary path (used by reverse typically)
                            {
                                currPath.StuckFlags |= TimeoutFlags.Reverse;

                                var prev = currPath.PeekPrevious();

                                if (currLoc.DistanceXZ(prev) < 1f)
                                {
                                    Log.Debug("Anti-Stuck: Too close to reverse, skipping.");
                                }
                                else if (prev.Map.Equals(currPath.CurrentWP.Map) && currPath.Previous()) // if backwards is possible
                                {
                                    Log.Write("Anti-Stuck: Let's try stepping backwards.");
                                    return _move();
                                }
                            }
                            else if (!currPath.StuckFlags.Is(TimeoutFlags.ReversePath))  // or we're in a main path AND we haven't tried to go backwards
                            {
                                currPath.StuckFlags |= TimeoutFlags.ReversePath;

                                lock (_lockpathhistory)
                                {
                                    if (_pathhistory.Any())
                                    {
                                        Log.Write("Anti-Stuck: Let's try reversing our steps.");
                                        var newpath = _pathhistory.ToList();
                                        currPath.Next();
                                        SetTemporaryPath(newpath);
                                    }
                                    else
                                    {
                                        Log.Debug("Anti-Stuck: Path history is empty.");
                                        return false;
                                    }
                                }
                                return true;
                            }
                        }
                        if (!currPath.StuckFlags.Is(TimeoutFlags.Clickables))
                        {
                            currPath.StuckFlags |= TimeoutFlags.Clickables;

                            var clicker = GameData.Harvestables
                                                .OrderBy(x => x.Location.DistanceXZ(currPath.CurrentWP))
                                                .FirstOrDefault();

                            if (clicker == null)
                                return _move();

                            Log.Write("Anti-Stuck: Let's try using the environment to get un-stuck.");

                            var timeout = DateTime.Now.AddMilliseconds(1500);

                            var random = new Random();
                            do
                            {
                                GameData.Interact(clicker.ObjectView);
                                Thread.Sleep(random.Next(40, 80));
                            } while (DateTime.Now < timeout);

                            return _move();
                        }
                        if (!currPath.StuckFlags.Is(TimeoutFlags.Pathfind))
                        {
                            currPath.StuckFlags |= TimeoutFlags.Pathfind;

                            Log.Write("Anti-Stuck: Let's try Pathfinding our way out.");
                            return _pathfind();
                        }
                        if (!currPath.StuckFlags.Is(TimeoutFlags.Next))
                        {
                            currPath.StuckFlags |= TimeoutFlags.Next;

                            Log.Write("Anti-Stuck: Last ditch effort, trying next waypoint.");
                            currPath.Next();
                            currPath.Next();

                            return _move();
                        }

                        Log.Write("Anti-Stuck: Looks like we're stuck for good. Exiting {0}.", Constants.ScriptName);

                        oThread.EndScript();
                        return false;
                    }
                }
                else                                                        // and we haven't timed out
                {
                    if (currPath.IsCloser(currLoc))
                    {
                        currPath.ResetStuck();
                    }

                    //Log.Debug2("Mover: Moving closer.");
                    return _move();
                }
            }
        }

        private static bool _move(oVector destination)
        {
            bool ret = false;

            if (!destination.Map.Equals(GameData.MapName))
                return false;

            ConcurrentTaskManager.GameRun(() =>
            {
                var destiny = destination;
                ret = Player.Move(destiny);
            });

            Thread.Sleep(50);

            return ret;
        }

        private static bool _move()
        {
            float dist = Distance(currPath.CurrentWP, GameData.PlayerLocation);

            if (dist > 40f)
            {
                Log.Debug2("Far: Waypoint distance is {0}. Pathfinding.", dist);
                return _pathfind();
            }

            return _move(currPath.CurrentWP);
        }

        private static bool _pathfind()
        {
            bool ret = false;
            Vector3 wp = null; // currPath.CurrentWP;

            // Log.Debug("OR: WP={0},ret={1},dist={2},cm={3}", wp.ToString(), ret.ToString(), GameData.PlayerLocation.DistanceXZ(wp), GameData.CanMove(wp).ToString());

            Log.Debug2("> Pathfinding");
            do
            {
                ConcurrentTaskManager.GameRun(() =>
                {
                    var cwp = currPath.CurrentWP;
                    wp = WorldPath.FindNextWalkPoint(Player.Location, Helpers.NoZeroes(cwp), 5.0f);
                    ret = Player.Move(wp);
                });

            } while (wp == null);
            Log.Debug2("< Pathfinding");

            //ret = _move(wp);
            Thread.Sleep(50);
            // Log.Debug("PF: WP={0},ret={1},dist={2},cm={3}", wp.ToString(), ret.ToString(), GameData.PlayerLocation.DistanceXZ(wp), GameData.CanMove(wp).ToString());

            return ret;
        }

        public static bool PathfindTo(Vector3 target)
        {
            Vector3 wp = null;

            bool ret = false;

            ConcurrentTaskManager.GameRun(() =>
            {
                var twp = target;
                wp = WorldPath.FindNextWalkPoint(Player.Location, Helpers.NoZeroes(twp), 5.0f);
                ret = Player.Move(wp);
            });

            Thread.Sleep(50);

            return ret;
        }

        public static bool MoveTo(Vector3 target)
        {
            return _move(new oVector(target, GameData.MapName));
        }

        public static bool MoveWithinRange(Vector3 target, float Range)
        {
            Vector3 curLoc;
            float curDist;

            var Timeout = DateTime.Now.AddSeconds(2);

            do
            {
                curLoc = GameData.PlayerLocation;
                curDist = curLoc.DistanceXZ(target);

                var vect = Vector3.DistanceAdjust(curLoc, target, Range);

                if(curDist > Range)
                    MoveTo(vect);

                Thread.Sleep(100);

                if (DateTime.Now > Timeout)
                    break;
            } while (curDist > Range);


            curDist = curLoc.DistanceXZ(target);

            return (curDist <= Range);
        }

        public static bool SetClosest(oVector loc)
        {
            bool ret = false;

            try
            {
                ret = currPath.SetClosest(loc);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                oThread.EndScript();
            }
            return ret;
        }

        public static bool SetClosest()
        {
            return SetClosest(new oVector(GameData.PlayerLocation, GameData.MapName));
        }

        public static bool SetClosestPath()
        {
            // Loops are priority (harvesting/hunting) .. then order by key priority value
            foreach (var path in _WP_Paths.OrderByDescending(x => x.Value.IsLoop).ThenBy(x => x.Key))
            {
                SetFocusPath(path.Key);
                if (SetClosest())
                    return true;
            }
            return false;
        }

        private static float Distance(Vector3 a, Vector3 b)
        {
            return a.DistanceXZ(b);
        }

        public static void AddPath(PathLabels label, Vector3[] path, String[] pathmaps, bool isloop)
        {
            AddPath(
                label,
                path.ToList(),
                pathmaps.ToList(),
                isloop
                );
        }

        public static void AddPath(PathLabels label, List<Vector3> path, List<String> pathmaps, bool isloop)
        {
            _WP_Paths.Add(
                new KeyValuePair<PathLabels, wpPath>(label, new wpPath(path, pathmaps, isloop))
                );
        }

        public static void SetTemporaryPath(List<oVector> path, float mindist)
        {
            SetTemporaryPath(path);
            _minDist = mindist;
        }

        public static void SetTemporaryPath(List<oVector> path)
        {
            currPath = new wpPath(path);
            _tmpPathIndex = null;
            temporaryPath = true;            

            SetClosest();
        }

        public static void SetTemporaryPath(List<Vector3> path, string mapname)
        {
            var ovPath = path.Select(x => new oVector(x, mapname)).ToList();
            SetTemporaryPath(ovPath);
        }

        public static void EndTemporaryPath()
        {
            if(temporaryPath == true)
                SetFocusPath(_Focused_Path, true);
        }

        public static void SetFocusPath(PathLabels focus)
        {
            SetFocusPath(focus, true);
        }

        public static void SetFocusPath(PathLabels focus, bool retainLastWP)
        {
            _minDist = 3.0f;
            _Focused_Path = focus;
            temporaryPath = false;
            _tmpPathIndex = null;
            currPath = _WP_Paths.Single(kvp => kvp.Key.Equals(focus)).Value;
            currPath.StuckFlags = 0;

            if (!retainLastWP)
                SetClosest();
        }

    }

}
