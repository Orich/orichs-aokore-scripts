﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using UnityEngine;
using System.Diagnostics;

namespace Script
{
    public static class Log
    {
        private static System.Object _lock = new System.Object();
        public static bool Verbose = false;

        public static void Debug2(string formatted, params object[] arguments)
        {
            if(Verbose)
            {
                Debug(formatted, arguments);
            }
        }

        public static void Debug2(string message)
        {
            if (Verbose)
            {
                _debug(message);
            }
        }

        public static void Debug(string formatted, params object[] arguments)
        {
#if !OTEST
            if (!Program.Settings.Debugging)
                return;
#endif
            var message = String.Format(formatted, arguments);

            _debug(message);
        }

        public static void Debug(string message)
        {
#if !OTEST
            if (!Program.Settings.Debugging)
                return;
#endif

            _debug(message);
        }

        public static void KoreLog(string log, string trace, LogType type)
        {
            Write("KDB> " + log);
            Write("KDB> " + trace);
            Write("KDB> " + type.ToString());
        }

        private static void _debug(string message)
        {
            StackTrace trace = new StackTrace();

            String caller = trace.GetFrame(2).GetMethod().Name;

            //if (!caller.Equals("_move") && !caller.Equals("MoveNext"))
            Write("[D] [C={0}] [M={1}]", caller, message);
            //Write("[D] [M={0}]", message);
        }

        public static void Write(string formatted, params object[] arguments)
        {
            try
            {
                Write(String.Format(formatted, arguments));
            }
            catch(Exception ex)
            {
                Write("Log Write Failed: " + ex.Message);
            }
        }

        public static void Write(string message)
        {
            if (String.IsNullOrEmpty(message))
                Output.Log("{Empty}", Constants.ScriptName);
            else
                Output.Log(message, Constants.ScriptName);
        }

        public static void Clear()
        {
            Output.ClearLog(Constants.ScriptName);
        }
    }
}
