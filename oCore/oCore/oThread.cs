﻿using System.Threading;

namespace Script
{
    public static class oThread
    {

        public static void EndScript()
        {
            Harvester.Dispose();
            Combat.Dispose();
            Mover.Dispose();
            Thread.CurrentThread.Abort();
        }
    }


}
