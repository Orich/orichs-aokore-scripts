using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;


namespace Script
{
    public static class MyExtensions
    {
        public static Vector3 Copy(this Vector3 loc)
        {
            return new Vector3(loc.X, loc.Y, loc.Z);
        }
    }

    public static class GameData
    {
        private static volatile bool isonline = false;
        private static volatile bool isknockeddown = false;
        private static volatile bool ismounted = false;
        private static volatile bool ismountcasting = false;
        private static volatile bool ismountcasted = false;
        private static volatile bool ismoving = false;
        private static volatile float playerweight = 0f;
        private static volatile bool isharvesting = false;
        private static volatile bool isincity = false;
        private static volatile bool isbankopen = false;

        private static volatile float myhp;
        private static volatile float mymaxhp;
        private static volatile float myep;
        private static volatile float mymaxep;
        private static volatile float attackrange;

        private static volatile Vector3 mountlocation = null;
        private static Object _lockmountlocation = new Object();

        private static volatile Vector3 playerlocation = null;
        private static Object _lockplayerlocation = new Object();

        private static List<ItemInstance> items = null;
        private static Object _lockitems = new Object();

        private static List<HarvestableInstance> harvestables = null;
        private static Object _lockharvestables = new Object();

        private static volatile Vector3 banklocation = null;
        private static Object _lockbanklocation = new Object();

        private static List<MobInstance> mobs = null;
        private static Object _lockmobs = new Object();

        private static List<RplayerInstance> rplayers = null;
        private static Object _lockrplayers = new Object();

        private static List<Vector3> findpath = null;
        private static Object _lockpath = new Object();

        private static volatile Vector3 _repairlocation = null;
        private static Object _lockrepairlocation = new Object();

        private static List<Object> _npclist = null;
        private static Object _locknpclist = new Object();

        private static List<string> _spells = null;
        private static Object _lockspells = new Object();

        private static long? playerid = null;
        public static volatile bool hasplayer = false;

        private static volatile string territorytype = null;
        private static volatile string territorytier = null;
        private static volatile string instancetype = null;
        private static volatile string mapname = null;

        public static string MapName
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { mapname = Map.Name; });
                return mapname;
            }
        }

        public static string InstanceType
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { instancetype = Map.InstanceType; });
                return instancetype;
            }
        }

        public static string TerritoryType
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { territorytype = Map.TerritoryType; });
                return territorytype;
            }
        }

        public static string TerritoryTier
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { territorytier = Map.TerritoryTier; });
                return territorytier;
            }
        }

        public static bool HasPlayer
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { hasplayer = Player.HasPlayer; });
                return hasplayer;
            }
        }

        public static bool IsOnline
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isonline = (Client.ClientState.Contains("Connected") && Player.HasPlayer); });
                return isonline;
            }
        }

        public static Vector3 BankLocation
        {
            get
            {
                lock (_lockbanklocation)
                    ConcurrentTaskManager.GameRun(() => { banklocation = Bank.Location.Copy(); });

                return banklocation;
            }
        }

        public static bool IsKnockedDown
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isknockeddown = Player.IsKnockedDown; });
                return isknockeddown;
            }
        }

        public static bool IsMounted
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismounted = Mount.IsMounted; });
                return ismounted;
            }
        }

        public static bool IsMountCasting
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismountcasting = Mount.IsCasting; });
                return ismountcasting;
            }
        }

        public static bool IsMountCasted
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismountcasted = (bool)Mount.IsCasted; });
                return ismountcasted;
            }
        }

        public static bool IsMoving
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { ismoving = Player.IsMoving; });
                return ismoving;
            }
        }

        public static bool IsHarvesting
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isharvesting = Player.IsHarvesting; });
                return isharvesting;
            }
        }

        public static bool CanMove(Vector3 myvec)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Player.CanMove(myvec); });
            return ret;
        }

        public static bool CanMount
        {
            get
            {
                bool ret = false;
                return ret;
            }
        }

        public static bool IsInCity
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isincity = Player.IsPlayerInCity; });
                return isincity;
            }
        }

        public static bool IsBankOpen
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { isbankopen = Npc.BankIsOpen; });
                return isbankopen;
            }
        }

        public static void CloseBank()
        {
            ConcurrentTaskManager.GameRun(() => { Bank.Close(); });
        }

        public static bool OpenBank()
        {
            bool ret = false;

            ConcurrentTaskManager.GameRun(() => { ret = Bank.Open(); });

            return ret;
        }

        public static bool MoveItemToBank(long id)
        {
            bool ret = false;

            ConcurrentTaskManager.GameRun(() => { var myid = id;  ret = Item.MoveToBank(myid); });

            return ret;
        }

        public static Vector3 MountLocation
        {
            get
            {
                lock (_lockmountlocation)
                    ConcurrentTaskManager.GameRun(() => { mountlocation = Mount.Location.Copy(); });

                return mountlocation;
            }
        }

        public static List<Object> NPCList
        {
            get
            {
                lock(_locknpclist)
                    ConcurrentTaskManager.GameRun(() => { _npclist = Npc.List; });
                return _npclist;
            }
        }

        public static object GetObjectByID(long id)
        {
            object obj = null;
            ConcurrentTaskManager.GameRun(() => { obj = Player.GetObjectByID(id); });
            return obj;
        }

        public static long? RepairID
        {
            get
            {
                long? ret = null;
                ConcurrentTaskManager.GameRun(() => { ret = Npc.GetRepairId; });
                return ret;
            }
        }
            

        public static Vector3 RepairLocation
        {
            get
            {
                lock(_lockrepairlocation)
                    ConcurrentTaskManager.GameRun(() => { _repairlocation = Repair.Location; });
                return _repairlocation;
            }
        }

        public static bool RepairOpen()
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Repair.Open(); });
            return ret;
        }

        public static bool RepairAll()
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Repair.RepairAll(); });
            return ret;
        }

        public static bool RepairClose()
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Repair.Close(); });
            return ret;
        }
        

        public static Vector3 PlayerLocation
        {
            get
            {
                lock (_lockplayerlocation)
                    ConcurrentTaskManager.GameRun(() => { playerlocation = Player.Location.Copy(); });

                return playerlocation;
            }
        }

        public static float PlayerWeight
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { playerweight = (float)Player.Weight; });
                return playerweight;
            }
        }

        public static long PlayerID
        {
            get
            {
                if (playerid != null)
                    return (long)playerid;

                ConcurrentTaskManager.GameRun(() => { playerid = (long)Player.ID; });
                return (long)playerid;
            }
        }

        public static bool CanHarvest(Object obj)
        {
            bool ret = false;

            ConcurrentTaskManager.GameRun(() => { object myobj = obj; ret = Harvest.CanHarvest(myobj); });

            return ret;
        }

        public static List<Vector3> FindPath(Vector3 start, Vector3 end)
        {
            lock (_lockpath)
                ConcurrentTaskManager.GameRun(() => { var s = start; var e = end; findpath = WorldPath.FindPath(s, Helpers.NoZeroes(e)).ToList(); });
            
            return findpath;
        }
            
        public static List<ItemInstance> Items
        {
            get
            {
                lock (_lockitems)                
                    ConcurrentTaskManager.GameRun(() => { items = Item.Items.ToList(); });
                return items;
            }
        }

        public static List<HarvestableInstance> Harvestables
        {
            get
            {
                lock (_lockharvestables)
                    ConcurrentTaskManager.GameRun(() => { harvestables = Harvest.Harvestables.ToList(); });
                return harvestables;
            }
        }

        public static float MyHP
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { myhp = (float)Player.CurrentHP; });
                return myhp;
            }
        }

        public static float MyMaxHP
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { mymaxhp = (float)Player.MaxHP; });
                return mymaxhp;
            }
        }

        public static float MyEP
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { myep = (float)Player.CurrentEnergy; });
                return myep;
            }
        }

        public static float MyMaxEP
        {
            get
            {
                ConcurrentTaskManager.GameRun(() => { mymaxep = (float)Player.MaxEnergy; });
                return mymaxhp;
            }
        }

        public static RplayerInstance GetRPlayer(long id)
        {
            return Players.SingleOrDefault(x => x.Id.Equals(id));
        }

        public static List<MobInstance> Mobs
        {
            get
            {
                lock (_lockmobs)
                    ConcurrentTaskManager.GameRun(() => { mobs = Mob.Mobs.ToList(); });
                return mobs;
            }
        }

        public static List<RplayerInstance> Players
        {
            get
            {
                lock (_lockrplayers)
                    ConcurrentTaskManager.GameRun(() => { rplayers = RPlayer.RPlayers.ToList(); });
                return rplayers;
            }
        }

        public static bool IsAttacked
        {
            get
            {
                var pid = PlayerID;

                var Hostile = Mobs
                    .Where(i => i.TargetId == pid)
                    .Where(i => !i.IsDead)
                    .FirstOrDefault();

                return (Hostile != null);
            }
        }

        public static bool IsMountEquipped
        {
            get
            {
                var Check_Mount = Items
                        .Where(i => (int)i.SlotOfInventory == 7)
                        .Where(i => !String.IsNullOrEmpty(i.Name))
                        .Where(i => (bool)i.IsEquiped)
                        .Where(i => (long)i.CurrDurability > 0)
                        .FirstOrDefault();


                return (Check_Mount != null);
            }
        }

        public static bool IsOverweight
        {
            get
            {
                return (playerweight > 99f);
            }
        }

        public static List<PortalInstance> Portals
        {
            get
            {
                List<PortalInstance> ret = null;

                ConcurrentTaskManager.GameRun(() =>
                {
                    ret = new List<PortalInstance>(Portal.Portals);
                });

                return ret;
            }
        }

        public static PortalInstance FindMapExit(string mapname)
        {
            PortalInstance ret = null;

            ConcurrentTaskManager.GameRun(() =>
            {
                ret = Portal.Portals
                            .Where(x => x.MapId.Equals(Map.Name))
                            .Where(x => x.DestinationName.Equals(mapname))
                            .OrderBy(x => x.Location.DistanceXZ(Player.Location))
                            .First();

            });
            return ret;
        }

        public static HarvestableInstance GetResource(long id)
        {
            return Harvestables.Where(i => i.Id == id).FirstOrDefault();
        }

        public static MobInstance GetMob(long id)
        {
            return Mobs.FirstOrDefault(i => i.Id.Equals(id));
        }

        public static bool Interact(Object obj)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { var myobj = obj;  ret = Player.ObjectInteraction(myobj); });
            return ret;
        }

        public static bool StopAllActions()
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() => { ret = Player.StopAllActions(); });
            return ret;
        }

        public static void SetMapWaypoint(Vector3 wp)
        {
            ConcurrentTaskManager.GameRun(() => { Map.CreatWayPointMark(wp); });
        }


        public static List<string> Spells
        {
            get
            {
                lock (_lockspells)
                {
                    _spells = new List<string>();
                    ConcurrentTaskManager.GameRun(() =>
                    {
                        for (byte i = 0; i < 6; i++)
                        {
                            //var spl = AOKore.Game.Spell.CharacterSpellSlot(i);
                            //var spl = AOKore.Game.Spell.Name(i);
                            var spl = AOKore.Game.Spell.SourceName(i);
                            _spells.Add(spl);
/*
                            if (spl.Equals("Deadly Shot"))
                                _spells.Add("DEADLYSHOT");
                            else if (spl.Equals("Explosive Arrows"))
                                _spells.Add("BURNINGARROWS");
                            else if (spl.Equals("Magic Arrow"))
                                _spells.Add("SKILLSHOT_STUN");
                            else if (spl.Equals("Mend Wounds"))
                                _spells.Add("OUTOFCOMBATHEAL");
                            else
                                _spells.Add(spl);
                                */
                        }
                    });
                }
                return _spells;
            }
        }

        public static string SpellCategory(string spellname)
        {
            string cat = String.Empty;
            ConcurrentTaskManager.GameRun(() =>
            {
                cat = AOKore.Game.Spell.Category(spellname);
            });
            if (cat == null)
                return String.Empty;
            return cat;
        }

        public static string SpellCategory(byte category)
        {
            string cat = String.Empty;
            ConcurrentTaskManager.GameRun(() =>
            {
                cat = AOKore.Game.Spell.Category(category);
            });
            if (cat == null)
                return String.Empty;
            return cat;
        }

        public static bool CanCast(byte Slot)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Spell.CanCast(Slot);
            });
            return ret;
        }

        public static bool CastSpell(byte Slot)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Spell.CastSlot(Slot);
            });
            return ret;
        }

        public static bool CastSpellSelf(byte Slot)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Spell.CastSelf(Slot);
            });
            return ret;
        }

        public static bool CastSpell(byte Slot, object Target)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Spell.CastSlotObj(Slot, Target);
            });
            return ret;
        }

        public static bool CastSpell(byte Slot, Vector3 Target)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                //ret = AOKore.Game.Spell.CastSlotVector(Slot, Target);
                ret = AOKore.Game.Spell.CastSlotXZ(Slot, Target.X, Target.Z);
            });
            return ret;
        }

        public static bool CastSpell(byte Slot, long Id)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Spell.CastSlotObjId(Slot, Id);
            });
            return ret;
        }

        public static bool IsCasting
        {
            get
            {
                bool ret = false;
                ConcurrentTaskManager.GameRun(() =>
                {
                    ret = AOKore.Game.Player.IsCasting;
                });
                return ret;
            }
        }

        public static bool IsChanneling
        {
            get
            {
                bool ret = false;
                ConcurrentTaskManager.GameRun(() =>
                {
                    ret = AOKore.Game.Player.IsChanneling;
                });
                return ret;
            }
        }

        public static bool IsCastingOrChanneling
        {
            get
            {
                return (IsCasting || IsChanneling);
            }
        }

        public static bool SelectTarget(long ID)
        {
            bool ret = false;
            ConcurrentTaskManager.GameRun(() =>
            {
                ret = AOKore.Game.Player.Select(ID);
            });
            return ret;
        }

        public static void Attack()
        {
            ConcurrentTaskManager.GameRun(() =>
            {
                AOKore.Game.Player.DefaultAttack();
            });
        }

        public static float AttackRange
        {
            get
            {
                ConcurrentTaskManager.GameRun(() =>
                {
                    attackrange = (float)AOKore.Game.Player.AttackRange;
                });
                return attackrange;
            }
        }

        public static bool IsAttacking
        {
            get
            {
                bool ret = false;
                ConcurrentTaskManager.GameRun(() =>
                {
                    ret = AOKore.Game.Player.IsAttacking;
                });
                return ret;
            }
        }
    }

}
