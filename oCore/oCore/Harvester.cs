﻿using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Script
{
    public static class Harvester
    {
        private static volatile bool _killThread = false;

        private static List<HarvestableInstance> _resources = new List<HarvestableInstance>();
        private static List<KeyValuePair<DateTime, HarvestableInstance>> _blacklist = new List<KeyValuePair<DateTime, HarvestableInstance>>();
        private static object _resourcelock = new object();


        private static bool HarvestMove(HarvestableInstance resource)
        {
            float _origdist = GameData.PlayerLocation.DistanceXZ(resource.Location);

            var timeout = DateTime.Now.AddMilliseconds(1000);

            if (GameData.IsMoving)
                GameData.StopAllActions();

            if (Combat.IsAttacked || GameData.IsAttacked)
                return false;

            while (DateTime.Now < timeout)
            {
                if (!GameData.IsMounted &&
                    (GameData.PlayerWeight > 99f || GameData.MountLocation.DistanceXZ(resource.Location) >= 14f)
                    )
                {
                    DateTime mountime = DateTime.Now;
                    Mounter.Mount();
                    timeout = timeout.AddMilliseconds((DateTime.Now - mountime).TotalMilliseconds);
                    continue;
                }

                var loc = GameData.PlayerLocation;
                var dist = loc.DistanceXZ(resource.Location);

                if ((_origdist - dist) >= 0.450f)
                {
                    _origdist = dist;
                    timeout = DateTime.Now.AddSeconds(1);
                }
                
                Mover.MoveTo(resource.Location);
                if (!GameData.IsMoving)
                {
                    GameData.Interact(resource.ObjectView);
                }
                
                /*
                Mover.PathfindTo(resource.Location);
                if(!GameData.IsMoving && dist <= 14f)
                {
                    GameData.Interact(resource.ObjectView);
                }
                */

                if (Combat.IsAttacked || GameData.IsAttacked)
                    return false;

                if (GameData.IsHarvesting)
                    return true;

                Thread.Sleep(100);
            }
            if (GameData.IsHarvesting || _origdist <= 2f)
                return true;

            return false;
        }

        public static void HarvestOld()
        {
            if (GameData.IsAttacked || !GameData.IsOnline)
                return;

            List<HarvestableInstance> resources = new List<HarvestableInstance>();
            List<KeyValuePair<DateTime, HarvestableInstance>> blacklist = new List<KeyValuePair<DateTime, HarvestableInstance>>();

            HarvestableInstance resource;
            KeyValuePair<DateTime, HarvestableInstance> blacklisted;

            lock (_resourcelock)
            {
                while (_resources.Any())
                {
                    resource = _resources.First();
                    blacklisted = _blacklist.SingleOrDefault(r => r.Value.Id.Equals(resource.Id));

                    var _tmpresource = resource;


                    if (!blacklisted.Equals(default(KeyValuePair<DateTime, HarvestableInstance>)))
                    {
                        if (blacklisted.Key < DateTime.Now)
                        {
                            Log.Debug("Blacklist: Skipping Blacklisted Resource {0},ID={1}", resource.Name, resource.Id);
                            _resources.Remove(resource);
                            continue;
                        }
                        Log.Debug("Blacklist: {0}'s Blacklisting has expired.", resource.Id);
                        _blacklist.Remove(blacklisted);
                    }

                    _tmpresource = GameData.GetResource(resource.Id);

                    if (resource.Charges == 0 || _tmpresource == null || _tmpresource.Charges == 0)
                    {
                        Log.Write("Harvest: Resource is out of charges.");
                        _resources.Remove(resource);
                        continue;
                    }

                    //if (!GameData.CanMove(resource.Location) || !Mover.MoveTo(resource.Location))
                    if (!HarvestMove(resource))
                    {
                        Log.Write("Harvest: Resource is unreachable, skipping it.");

                        try
                        {
                            if (blacklisted.Equals(default(KeyValuePair<DateTime, HarvestableInstance>)))
                            {
                                Log.Debug("Blacklist: Blacklisting Resource {0},ID={1}", resource.Name, resource.Id);
                                _blacklist.Add(
                                    new KeyValuePair<DateTime, HarvestableInstance>(DateTime.Now.AddSeconds(Program.Settings.BlacklistTimeout), resource)
                                    );
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Debug("Blacklist Fatal: {0}", ex.Message);
                        }
                        _resources.Remove(resource);
                        break;
                    }

                    Mover.EndTemporaryPath();

                    _tmpresource = GameData.GetResource(resource.Id);

                    while (_tmpresource != null && _tmpresource.Charges > 0)
                    {
                        if (!GameData.IsHarvesting)
                            GameData.Interact(resource.ObjectView);

                        Thread.Sleep(1000);

                        _tmpresource = GameData.GetResource(resource.Id);

                        if (GameData.IsAttacked || !GameData.IsOnline)
                            break;
                    }

                    Log.Write("Harvest: Successfully harvested {0}", resource.Name);

                    _resources.Remove(resource);
                }
            }

        }

        public static void Harvest()
        {
            if (GameData.IsAttacked || !GameData.IsOnline)
                return;

            List<HarvestableInstance> resources = new List<HarvestableInstance>();
            List<KeyValuePair<DateTime, HarvestableInstance>> blacklist = new List<KeyValuePair<DateTime, HarvestableInstance>>();

            HarvestableInstance resource;
            KeyValuePair<DateTime, HarvestableInstance> blacklisted;

            while (IsHarvestables)
            {
                HarvestableInstance _tmpresource;

                lock (_resourcelock)
                {
                    resource = _resources.First();
                    blacklisted = _blacklist.SingleOrDefault(r => r.Value.Id.Equals(resource.Id));

                    _tmpresource = resource;


                    if (!blacklisted.Equals(default(KeyValuePair<DateTime, HarvestableInstance>)))
                    {
                        if (DateTime.Now < blacklisted.Key)
                        {
                            Log.Debug("Blacklist: Skipping Blacklisted Resource {0},ID={1}", resource.Name, resource.Id);
                            _resources.Remove(resource);
                            continue;
                        }
                        else
                        {
                            Log.Debug("Blacklist: {0}'s Blacklisting has expired.", resource.Id);
                            _blacklist.Remove(blacklisted);
                        }
                    }

                    _tmpresource = GameData.GetResource(resource.Id);

                    if (resource.Charges == 0|| _tmpresource == null || _tmpresource.Charges == 0)
                    {
                        Log.Write("Harvest: Resource is out of charges.");
                        _resources.Remove(resource);
                        continue;
                    }
                }

                if (!HarvestMove(resource))
                {
                    Log.Write("Harvest: Resource is unreachable, skipping it.");

                    try
                    {
                        if (blacklisted.Equals(default(KeyValuePair<DateTime, HarvestableInstance>)))
                        {
                            Log.Debug("Blacklist: Blacklisting Resource {0},ID={1}", resource.Name, resource.Id);
                            lock (_resourcelock)
                                _blacklist.Add(
                                    new KeyValuePair<DateTime, HarvestableInstance>(DateTime.Now.AddSeconds(Program.Settings.BlacklistTimeout), resource)
                                    );
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Debug("Blacklist Fatal: {0}", ex.Message);
                    }
                    lock (_resourcelock)
                        _resources.Remove(resource);
                    break;
                }

                Mover.EndTemporaryPath();

                _tmpresource = GameData.GetResource(resource.Id);

                while (_tmpresource != null && _tmpresource.Charges > 0)
                {
                    if (!GameData.IsHarvesting)
                        GameData.Interact(resource.ObjectView);

                    if (Combat.IsAttacked || GameData.IsAttacked || !GameData.IsOnline)
                        break;

                    Thread.Sleep(1000);

                    _tmpresource = GameData.GetResource(resource.Id);
                }

                if (Program.Settings.CampRareNodes)
                {
                    while (_tmpresource.RareState == 3 && !_tmpresource.ResourceType.Contains("HIDE"))
                    {
                        _tmpresource = GameData.GetResource(resource.Id);

                        if (_tmpresource == null)
                            break;

                        if (_tmpresource.Charges == 0)
                        {
                            Thread.Sleep(1000);
                            Log.Write("Harvest:  Camping Tier 3 Node.");
                            continue;
                        }

                        if (!GameData.IsHarvesting)
                            GameData.Interact(_tmpresource.ObjectView);

                        Thread.Sleep(1000);
                    }
                }

                Log.Write("Harvest: Successfully harvested {0}", resource.Name);

                lock (_resourcelock)
                    _resources.Remove(resource);
            }
        }   

        public static void FlushHarvestables()
        {
            lock(_resourcelock)
            {
                _resources.Clear();
                _blacklist.Clear();
            }
        }

        public static bool IsHarvestables
        {
            get
            {
                lock(_resourcelock)
                {
                    return _resources.Any();
                }
            }
        }

        #region Scan Service
        public static void CreateScanService()
        {
            ConcurrentTaskManager.CreateService(() =>
            {
                if (!_killThread && GameData.IsOnline)
                {
                    try
                    {
                        Scanner();
                    }
                    catch(Exception ex)
                    {
                        Log.Debug(ex.Message);
                    }
                }
                else if(_killThread)
                {
                    Thread.CurrentThread.Abort();
                }
            }, Constants.ScriptName + "_HarvestScanner", 500);
        }

        private static void Scanner()
        {
            if (_killThread || !GameData.IsOnline || GameData.IsInCity)
                return;

            List<HarvestableInstance> resources = null;
            List<HarvestableInstance> Filtered = null;
            List<HarvestableInstance> Keepers = new List<HarvestableInstance>();

            var pLoc = GameData.PlayerLocation;

            try
            {
                resources = GameData.Harvestables;
            }
            catch(Exception ex)
            {
                Log.Debug("Harvestables: {0}", ex.Message);
                throw ex;
            }

            if (resources == null || !resources.Any())
            {
                Log.Debug2("Scanner Idle.");
                Thread.Sleep(5000);
                return;
            }

            try
            {
                Filtered = resources
                    .Where(i => pLoc.DistanceXZ(i.Location) <= Program.Settings.RangeSearch)
                    .Where(i => i.Charges > 0)
                    .Where(i => Math.Abs(pLoc.Y - i.Location.Y) < 4f)
                    .OrderByDescending(i => i.Tier)
                    .ThenByDescending(i => i.RareState) // if lower tiers are first, they get harvested last :-D
                    .ThenBy(i => pLoc.DistanceXZ(i.Location))
                    .ToList();
            }
            catch(Exception ex)
            {
                Log.Debug("Filtered: {0}", ex.Message);
                throw ex;
            }

            if (Filtered == null || !Filtered.Any())
                return;

            foreach (var resource in Filtered)
            {
                if (_killThread)
                    break;

                bool Match = false;

                if (Program.Settings.WoodHarvestCondition && resource.ResourceType.Contains("WOOD")
                    && Program.Settings.WoodHarvestTier.Contains((float)(resource.Tier + (float)(resource.RareState / 10f))))
                {
                    Match = true;
                }
                else if (Program.Settings.FiberHarvestCondition && resource.ResourceType.Equals("FIBER")
                    && Program.Settings.FiberHarvestTier.Contains((float)(resource.Tier + (float)(resource.RareState / 10f))))
                {
                    Match = true;
                }
                else if (Program.Settings.OreHarvestCondition && resource.ResourceType.Equals("ORE")
                    && Program.Settings.OreHarvestTier.Contains((float)(resource.Tier + (float)(resource.RareState / 10f))))
                {
                    Match = true;
                }
                else if (Program.Settings.RockHarvestCondition && resource.ResourceType.Equals("ROCK")
                    && Program.Settings.RockHarvestTier.Contains((float)(resource.Tier + (float)(resource.RareState / 10f))))
                {
                    Match = true;
                }
                else if (Program.Settings.HideHarvestCondition && resource.ResourceType.Equals("HIDE")
                    && Program.Settings.HideHarvestTier.Contains((float)(resource.Tier + (float)(resource.RareState / 10f)))
                    && GameData.CanHarvest(resource.ObjectView))
                {
                    Match = true;
                }


                if (Match == true)
                {
                    KeyValuePair<DateTime, HarvestableInstance> blacklisted = default(KeyValuePair<DateTime, HarvestableInstance>);
                    lock (_resourcelock)
                    {
                        blacklisted = _blacklist.SingleOrDefault(r => r.Value.Id.Equals(resource.Id));
                    }

                    if (blacklisted.Equals(default(KeyValuePair<DateTime, HarvestableInstance>)))
                    {
                        //var _tmpresource = GameData.Harvestables.Where(i => i.Id == resource.Id).FirstOrDefault();
                        //if (_tmpresource.Charges > 0)
                            Keepers.Add(resource);
                    }
                    else
                    {
                        if (DateTime.Now > blacklisted.Key)
                        {
                            Log.Debug("Blacklist timed out on ID {0}", blacklisted.Value.Id);
                            lock (_resourcelock)
                            {
                                _blacklist.Remove(blacklisted);
                            }
                            //var _tmpresource = GameData.Harvestables.Where(i => i.Id == resource.Id).FirstOrDefault();
                            //if (_tmpresource.Charges > 0)
                                Keepers.Add(resource);
                        }
                    }
                }
            } // foreach resource

            lock (_resourcelock)
            {
                _resources.Clear();
                _resources.AddRange(Keepers);
            }

        }
        #endregion

        #region Dispose
        public static void Dispose()
        {
            _killThread = true;
            _resources.DisposeAll();
            _resources = null;
            _blacklist.DisposeAll();
            _blacklist = null;
        }

        private static void DisposeAll(this IEnumerable set)
        {
            foreach (Object obj in set)
            {
                IDisposable disp = obj as IDisposable;
                if (disp != null) { disp.Dispose(); }
            }
        }
        #endregion

    }
}
