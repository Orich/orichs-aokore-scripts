using System;
using System.Collections.Generic;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using System.Linq;
using System.Collections;

namespace Script
{    

    public static class Mounter
    {
        public static bool Dismount()
        {
            bool ret = false;
            if (GameData.IsMounted)
            {
                ConcurrentTaskManager.GameRun(() =>
                {
                    //Log.Debug("Mount speed: {0}", AOKore.Game.Mount.CastTimer);
                    ret = AOKore.Game.Mount.MountDismount();
                });
            }
            return ret;
        }

        public static bool Mount()
        {
            //if (!GameData.CanMount)
            //    return false;

            if (!GameData.IsMounted)
            {
                if (GameData.IsMountCasted && !GameData.IsMountCasting)
                {
                    ConcurrentTaskManager.GameRun(() =>
                    {
                        AOKore.Game.Mount.MountIfCasted();
                    });

                    for (var timeout = DateTime.Now.AddSeconds(3); DateTime.Now < timeout && !GameData.IsMounted;)
                        Thread.Sleep(10);

                }

                // If we aren't mounted, and the mount is NOT casted, and we're not yet casting it
                if (!GameData.IsMountCasting && !GameData.IsMounted)
                {
                    ConcurrentTaskManager.GameRun(() =>
                    {
                        //Log.Debug("Mount speed: {0}", AOKore.Game.Mount.CastTimer);
                        AOKore.Game.Mount.MountDismount();
                    });

                    for (var timeout = DateTime.Now.AddSeconds(3); DateTime.Now < timeout && !GameData.IsMounted;)
                        Thread.Sleep(10);
                }

            }

            if (GameData.IsMounted)
                return true;

            Log.Debug("Mount Failed.");
            return false;
        }

        public static void CastMount()
        {
            ConcurrentTaskManager.GameRun(() =>
            {
                if (!AOKore.Game.Mount.IsCasting)
                    AOKore.Game.Mount.MountDismount();
            });
        }

        public static void PanickedMount()
        {
            if (!GameData.IsMounted)
            {
                if (GameData.IsMountCasted && !GameData.IsMountCasting)
                {
                    var timeout = DateTime.Now.AddMilliseconds(1500);

                    do
                    {
                        ConcurrentTaskManager.GameRun(() =>
                        {
                            if(!AOKore.Game.Mount.IsCasting && !AOKore.Game.Mount.IsMounted)
                                AOKore.Game.Mount.MountIfCasted();
                        });

                        while(GameData.IsMountCasting)
                            Thread.Sleep(100);

                    } while (DateTime.Now < timeout && !GameData.IsMounted);

                    if (!GameData.IsMounted)
                    {
                        Log.Debug("Re-Mounting Failed.");
                        return;
                    }
                }

                // If we aren't mounted, and the mount is NOT casted, and we're not yet casting it
                else if (!GameData.IsMountCasting)
                {
                    var timeout = DateTime.Now.AddSeconds(5);

                    do
                    {
                        ConcurrentTaskManager.GameRun(() =>
                        {
                            if (!AOKore.Game.Mount.IsCasting && !AOKore.Game.Mount.IsMounted)
                                AOKore.Game.Mount.MountDismount();
                        });

                        while (GameData.IsMountCasting)
                            Thread.Sleep(100);

                    } while (DateTime.Now < timeout && !GameData.IsMounted);

                    if (!GameData.IsMounted)
                    {
                        Log.Debug("Mount Casting Failed");
                        return;
                    }
                }
            }
        }
    }

}
