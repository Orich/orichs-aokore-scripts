using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System;
using System.Collections.Generic;


namespace Script
{
    public enum PathLabels
    {
        MainPath = 0
    };


    public class Program
    {        
        [Configuration("oMover")]
        public static Configuration Settings { get; set; }

        public static string ScriptName = "oMover";
        public static int moveTries = 0;

        public static void Main()
        {
            Log.Verbose = true;

            Log.Clear();
            Log.Write("Log Cleared");

            Log.Write("oMover: oMover v0.1alpha by Orich");
            Log.Write("oMover: -------------------------");
            Log.Write("oMover: Welcome to oMover.  This script loads a single Waypoint file and moves across it");
            Log.Write("oMover: This script utilizes AOKORE's Pathfinding routines. As of this release, they are");
            Log.Write("oMover: still very buggy and can fail. When AOKORE gets updated, things will be fixed.");

            if(Settings.Debugging)
            {
                Log.Write("WARNING: !!!!!!!!!!!!!!!!!!!");
                Log.Write("WARNING: You have debugging enabled. This script VOMITS debug text.");
                Log.Write("WARNING: Don't say I didn't warn you 8^) ");
                Log.Write("WARNING: !!!!!!!!!!!!!!!!!!!");
            }

            if (!GameData.IsOnline)
            {
                Log.Write("Exiting:  Not logged into a character.");
                return;
            }

            Log.Write("Initializing: " + "Loading Waypoints ...");
            var _wplist = Settings.GetWaypoints();
            var _wpmaplist = Settings.GetWaypointsMap();

            if(_wplist.Count() != _wpmaplist.Count())
            {
                Log.Write("Initializing: " + "{0} has {1} Waypoints.  {2} has {3} Map Points.", Settings.WPFile, _wplist.Count(), "Map_" + Settings.WPFile, _wpmaplist.Count());
                Log.Write("Initializing: " + "They need to match.  Exiting.");

                return;
            }

            Log.Write("Initializing: " + "Waypoints loaded from: {0}.", Settings.WPFile);
            Log.Write("Initializing: " + "Mappoints loaded from: {0}.", "Map_"+Settings.WPFile);

            try
            {
                Mover.AddPath(PathLabels.MainPath, _wplist, _wpmaplist, false);
            }
            catch(Exception ex)
            {
                Log.Write("Fatal: " + ex.Message);
                Log.Write("NOTE : This happens because AOKORE doesn't properly dispose of old threads.");
                Log.Write("NOTE : You can either wait a bit to run it again or re-start AOKORE.");
                Log.Write("NOTE : I am ending the script now.");
                oThread.EndScript();
            }
            Log.Debug("Setting Path Focus");
            Mover.SetFocusPath(PathLabels.MainPath);
            Log.Debug("Finding closest waypoints.");
            if(!Mover.SetClosest())
            {
                Log.Write("Fatal: Can't find a waypoint for this map ({0})", GameData.MapName);
                oThread.EndScript();
            }

            Log.Debug("Initializing: " + "Entering primary Script thread.");

            ConcurrentTaskManager.CreateService(() =>
            {
                if (!GameData.IsOnline)
                {
                    Log.Debug("Waiting for player.");
                    Thread.Sleep(500);
                    return;
                }

                if(Mover.IsCompleted)
                {
                    Log.Write("We reached the end!");
                    oThread.EndScript();
                }

                if(!Program.Settings.Debugging)
                    Log.Write("Moving.");

                bool Success = Mover.MoveNext();

                if (!Success)
                    moveTries++;
                if (Success)
                    moveTries = 0;

                if (moveTries > 5)
                {
                    Log.Write("Moves keep failing.  Aborting script.");
                    oThread.EndScript();
                }

            }, Constants.ScriptName, 50);

            Log.Debug("Initializing: " + "Script Service successfully initiated.");
            _wplist = null;
            _wpmaplist = null;
        }



    }
}
