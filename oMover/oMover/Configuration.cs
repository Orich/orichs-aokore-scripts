﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;


namespace Script
{
    public class Configuration
    {
        [Category("Waypoints")]
        [DisplayName("Waypoint File")]
        public string WPFile { get; set; }

        [Category("Logging")]
        [DisplayName("Show Debug Info")]
        public bool Debugging { get; set; }


        public class PathData
        {
            public Vector3[] Path { get; set; }
        }

        public class PathData2
        {
            public String[] Path { get; set; }
        }

        public List<Vector3> GetWaypoints()
        {
            List<Vector3> ret;

            try
            {
                ret = new List<Vector3>(FromXml<PathData>(File.ReadAllText(Constants.WaypointPath + WPFile)).Path);
            }
            catch(Exception ex)
            {
                Log.Debug(ex.Message);
                ret = null;
            }
            return ret;
        }

        public List<String> GetWaypointsMap()
        {
            List<String> ret;

            try
            {
                ret = new List<String>(FromXml<PathData2>(File.ReadAllText(Constants.WaypointPath + "Map_" + WPFile)).Path);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                ret = null;
            }
            return ret;
        }



        public static T FromXml<T>(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }
        


    }
}
