#define OGATHER

using System.Linq;
using System.Threading;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace Script
{
    public enum Intentions
    {
        Initializing,
        Searching,
        Harvesting,
        ToBank,
        ToHarvest,
        DecideAction,
        Escaping,
        Hunting,
        Defending
    }

    public class Program
    {
        [Configuration(Constants.ScriptName)]
        public static Configuration Settings { get; set; }


        public static Intentions Intention = Intentions.Initializing;
        public static Intentions PreviousIntention;
        public static int IntentionTries = 0;


        public static void Main()
        {
            Log.Clear();
            Log.Write("Log Cleared");

            Spells.PopulateSpells();

            //Application.logMessageReceived += Log.KoreLog;

            Log.Write("Initializing Script.");        

            Mounter.Dismount();
            Thread.Sleep(1000);
            if (Spells.MySpells.Any())
            {
                var spells = Spells.MySpells;
            }
            Mounter.Mount();

            Log.Verbose = false;

            if (!GameData.IsOnline)
            {
                Log.Write("Exiting:  Not logged into a character.");
                return;
            }

            #region Load Waypoints
            try
            {
                Mover.AddPath(PathLabels.HarvestPath, Settings.GetWaypoints(Program.Settings.PathWayPoint), Settings.GetWaypointsMap(Program.Settings.PathWayPoint), true);
                Log.Write("Harvest   : Waypoints from {0} were loaded.", Settings.PathWayPoint);

                if (Program.Settings.GoToCity)
                {
                    Mover.AddPath(PathLabels.ToCityPath, Settings.GetWaypoints(Program.Settings.PathWayPointCity), Settings.GetWaypointsMap(Program.Settings.PathWayPointCity), false);
                    Log.Write("To City   : Waypoints from {0} were loaded.", Settings.PathWayPointCity);
                }

                if (Program.Settings.GoToGatherArea)
                {
                    Mover.AddPath(PathLabels.ToHarvestPath, Settings.GetWaypoints(Program.Settings.PathWayPointGatherArea), Settings.GetWaypointsMap(Program.Settings.PathWayPointGatherArea), false);
                    Log.Write("From City : Waypoints from {0} were loaded.", Settings.PathWayPointGatherArea);
                }
            }
            catch (Exception ex)
            {
                Log.Write("Fatal: " + ex.Message);
                oThread.EndScript();
            }

            if (!Mover.SetClosestPath())
            {
                Log.Write("Error: Could not locate a valid Waypoint.");
                oThread.EndScript();
            }

            Log.Write("Initializing: " + "Path Chosen is {0}", Mover.CurrentPath.ToString());
            #endregion


            Log.Debug("Initializing: Mover Bot");
            Mover.CreateScanService();

            Log.Debug("Initializing: Combat Bot");
            Combat.CreateScanService();

            Log.Debug("Initializing: Harvest Bot");
            Harvester.CreateScanService();

            Log.Debug("Initializing: " + "Entering primary Script thread.");


            ConcurrentTaskManager.CreateService(() =>
            {
                if (!GameData.IsOnline)
                {
                    Log.Debug("Waiting for player.");
                    Thread.Sleep(500);
                    return;
                }

                if (Intention == Intentions.Initializing)
                {
                    Harvester.FlushHarvestables();
                    Mounter.Mount();

                    if (Mover.CurrentPath == PathLabels.HarvestPath)
                    {
                        Log.Write("Initialized Intention: Start Harvesting.");
                        Intention = Intentions.Searching;
                    }
                    else if (Mover.CurrentPath == PathLabels.ToCityPath && Program.Settings.GoToCity)
                    {
                        Log.Write("Initialized Intention: Go back to City Area.");
                        Intention = Intentions.ToBank;
                    }
                    else if (Mover.CurrentPath == PathLabels.ToHarvestPath && Program.Settings.GoToGatherArea)
                    {
                        Log.Write("Initialized Intention: Go to Harvest Area.");
                        Intention = Intentions.ToHarvest;
                    }

                    PreviousIntention = Intention;
                    return;
                }

                if (Combat.IsAttacked && Intention != Intentions.Escaping && Intention != Intentions.Defending)
                {
                    if (GameData.IsMounted)
                    {
                        PreviousIntention = Intention;
                        Log.Write("Intention: We're being attacked; escaping until we're safe.");
                        Intention = Intentions.Escaping;
                    }
                    else
                    {
                        PreviousIntention = Intention;
                        Log.Write("Intention: We're being attacked, defending.");
                        Intention = Intentions.Defending;
                    }
                }

                if (Program.Settings.GoToCity)
                {
                    if (GameData.IsMounted && GameData.PlayerWeight > 99f
                        && Intention != Intentions.ToBank)
                    {
                        Log.Write("Intention: Going to City for Drop Off.");
                        Intention = Intentions.ToBank;
                        Mover.SetFocusPath(PathLabels.ToCityPath, false);
                        IntentionTries = 0;
                    }

                }

                if (Program.Settings.GoToGatherArea)
                {
                    if (Intention == Intentions.ToHarvest)
                    {
                        Mounter.Mount();
                        if (!GameData.IsMounted)
                            return;

                        Mover.MoveNext();

                        if (Mover.IsCompleted && !Mover.IsTemporary)
                        {
                            Log.Write("Intention: Arrived at Harvest location. Begin Searching.");
                            Mover.SetFocusPath(PathLabels.HarvestPath, false);
                            Intention = Intentions.Searching;
                            IntentionTries = 0;
                        }
                    }
                }


                if (Intention == Intentions.Escaping)
                {
                    Mounter.PanickedMount();

                    if (!GameData.IsMounted)
                        return;

                    if (!Combat.IsAttacked)
                    {
                        Log.Write("Intention: No longer being attacked.");
                        Intention = PreviousIntention;
                        return;
                    }
                    else
                    {
                        Harvester.FlushHarvestables();
                        Combat.FlushMobs();
                        Mover.MoveNext();
                    }
                }

                else if (Intention == Intentions.DecideAction)
                {

                }

                if (Intention == Intentions.Searching)
                {
                    if (Combat.IsAttacked)
                        return;

                    if (Harvester.IsHarvestables)
                    {
                        Log.Write("Intention: Harvestables found.  Attempting to gather.");
                        Intention = Intentions.Harvesting;
                    }
                    else if(Combat.IsHuntmobs)
                    {
                        Log.Write("Intention: Found mobs to kill.");
                        Intention = Intentions.Hunting;
                    }
                    else
                    {
                        if (!Mounter.Mount())
                            return;

                        Mover.MoveNext();
                    }
                }

                if(Intention == Intentions.Defending)
                {
                    Combat.Defend();

                    if (!Combat.IsAttacked)
                    {
                        Log.Write("Intention: We're done defending.");
                        Spells.Recover();
                        Intention = PreviousIntention;
                        return;
                    }
                }

                if (Intention == Intentions.Hunting)
                {
                    Log.Write("Hunting: Going on the hunt.");
                    Combat.Hunt();

                    if(!Combat.IsHuntmobs)
                    {
                        Log.Write("Hunting: No more hunting mobs.");
                        bool ret = Mover.SetClosest();
                        Intention = Intentions.Searching;
                        return;
                    }
                }

                if (Intention == Intentions.Harvesting)
                {
                    if (Combat.IsAttacked)
                    {
                        Mounter.PanickedMount();
                        //Mounter.Mount();
                        return;
                    }
                    
                    if (Harvester.IsHarvestables)
                    {
                        Harvester.Harvest();
                    }
                    else
                    {
                        bool ret = Mover.SetClosest();
                        //Log.Write("Harvesting:  Unable to Harvest, going back to Search mode.");
                        Intention = Intentions.Searching;
                    }
                }

                if (Intention == Intentions.ToBank)
                {
                    Mounter.Mount();
                    if (!GameData.IsMounted)
                        return;

                    if(Settings.TryRepair)
                    {
                        var repairItems = GameData.Items
                                .FirstOrDefault(x => x.CurrDurability < x.MaxDurability);

                        if(GameData.IsInCity && repairItems != null)
                        {
                            var RepairLoc = GameData.RepairLocation;

                            if(RepairLoc.X != 0f && RepairLoc.Z != 0f)
                            {
                                Log.Write("Repair: I spotted a repair NPC.  Let's go repair.");

                                /*
                                var Path = GameData.FindPath(GameData.PlayerLocation, RepairLoc)
                                            .Select(x => new oVector(x, GameData.MapName))
                                            .ToList();
                                            */

                                if(GameData.MapName.Equals("Bridgewatch"))
                                    Mover.SetTemporaryPath(Constants.BridgeWatchRepair, 1.0f);
                                else if(GameData.MapName.Equals("Steppe Cross"))
                                    Mover.SetTemporaryPath(Constants.SteppeCrossRepair, 1.0f);

                                else
                                {
                                    Log.Write("Repair: Sorry, I don't support repairing in {0} yet.", GameData.MapName);
                                    Settings.TryRepair = false;
                                    return;
                                }

                                Log.Write("Repair: I see a path to walk, let's go!");

                                while (!Mover.IsCompleted && Mover.IsTemporary)
                                {
                                    Mover.MoveNext();
                                    while (GameData.IsMoving)
                                        Thread.Sleep(100);
                                }

                                bool ret = false;

                                ret = GameData.RepairOpen();
                                Log.Write("Repair: Opened = {0}", ret);
                                if (!ret)
                                    return;
                                Thread.Sleep(1000);

                                Log.Write("Repair: Repairing ...");
                                ret = GameData.RepairAll();
                                byte timeout = 0;
                                do
                                {
                                    repairItems = GameData.Items.FirstOrDefault(x => x.CurrDurability < x.MaxDurability);
                                    Thread.Sleep(1000);
                                    timeout++;
                                } while (repairItems != null && timeout < 6);

                                Log.Write("Repair: {0}", ret);
                                Thread.Sleep(1000);
                                if(ret == false)
                                {
                                    if (GameData.MapName.Equals("Bridgewatch"))
                                        Mover.MoveTo(Constants.BridgeWatchRepair.Last());

                                    return;
                                }

                                ret = GameData.RepairClose();
                                Log.Write("Close: {0}", ret);
                                Thread.Sleep(1000);

                                Mover.SetFocusPath(Mover.CurrentPath, false);
                            }

                        }
                    }

                    Mover.MoveNext();

                    if (Mover.IsCompleted && !Mover.IsTemporary)
                    {
                        Log.Write("Banker: We're at the bank.");
                        var bankloc = GameData.BankLocation;

                        if (bankloc.DistanceXZ(GameData.PlayerLocation) < 25f)
                        {
                            Mover.MoveTo(bankloc);
                        }

                        GameData.OpenBank();

                        if (GameData.IsBankOpen)
                        {
                            Log.Write("Banker: Bank is open.");
                            IEnumerable<ItemInstance> Items;

                            Log.Write("Banker: Transferring items ...");
                            do
                            {
                                Items = GameData.Items
                                    .Where(x => x.InInventory == true)
                                    .Where(x => x.Count > 5)
                                    .Where(x => x.IsEquiped != true);

                                foreach (var item in Items)
                                {
                                    bool ret = false;
                                    if (item.Id != null)
                                        ret = GameData.MoveItemToBank((long)item.Id);
                                    Log.Write(String.Format("[{0}]: Move Qty:{1} of {2} to Bank.", (ret) ? "Success" : "Failed", item.Count, item.Name));

                                    Thread.Sleep(500);
                                }

                            } while (Items.Any());

                            Log.Write("Banker: Items successfully transferred.");

                            while(GameData.IsBankOpen)
                            {
                                GameData.CloseBank();
                                Thread.Sleep(1500);
                            }

                            if (Program.Settings.GoToGatherArea)
                            {
                                Log.Write("Banker: GoToGatherArea is toggled on, back to work!.");
                                Intention = Intentions.ToHarvest;
                                Mover.SetFocusPath(PathLabels.ToHarvestPath, false);
                                Mover.MoveNext();
                                return;
                            }
                            else
                            {
                                Log.Write("Banker: Since you have GoToGatherArea toggled off, Exiting script.");
                                oThread.EndScript();
                            }
                        }
                    }


                }




            }, Constants.ScriptName + "_Main", 100);



            Log.Debug("Initializing: " + "Script Service successfully initiated.");

        }



    }
}
