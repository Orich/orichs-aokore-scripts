﻿using System.Threading;

namespace Script
{
    public static class oThread
    {
        public static void EndScript()
        {
            Combat.Dispose();
            Harvester.Dispose();
            Mover.Dispose();
            Thread.CurrentThread.Abort();
        }
    }


}
