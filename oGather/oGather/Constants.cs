﻿using System;
using System.Collections.Generic;

namespace Script
{
    public static class Constants
    {
        public const string ScriptName = "oGather";

        public static string WaypointPath = System.Environment.CurrentDirectory + "\\Plugins\\Cache\\WayPoint\\";

        public static List<oVector> BridgeWatchRepair = new List<oVector>();

        public static List<oVector> SteppeCrossRepair = new List<oVector>();

        static Constants()
        {
            BridgeWatchRepair.Add(new oVector(new AOKore.Game.Vector3(-20.32f, -4.27f, -9.38f), "Bridgewatch"));
            BridgeWatchRepair.Add(new oVector(new AOKore.Game.Vector3(-6.45f, -4.27f, 5.28f), "Bridgewatch"));
            BridgeWatchRepair.Add(new oVector(new AOKore.Game.Vector3(4.25f, -4.00f, 12f), "Bridgewatch"));

            SteppeCrossRepair.Add(new oVector(new AOKore.Game.Vector3(88.15f, -0.36f, 122.09f), "Steppe Cross"));
            SteppeCrossRepair.Add(new oVector(new AOKore.Game.Vector3(88.51f, -0.23f, 134.25f), "Steppe Cross"));
        }
    }
}
