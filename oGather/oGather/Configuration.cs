﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;


namespace Script
{
    public class Configuration
    {
        internal int BlacklistTimeout = 20;
        internal float[] WoodHarvestTier = new float[] { };
        internal float[] RockHarvestTier = new float[] { };
        internal float[] OreHarvestTier = new float[] { };
        internal float[] FiberHarvestTier = new float[] { };
        internal float[] HideHarvestTier = new float[] { };
        internal bool Debugging { get { return SDebugger; } }

        [Category("1. CONFIG")]
        [DisplayName("Search Range")]
        public float RangeSearch { get; set; }

        [Category("1. CONFIG")]
        [DisplayName("Action Go To City")]
        public bool GoToCity { get; set; }

        [Category("1. CONFIG")]
        [DisplayName("Repair if needed?")]
        public bool TryRepair { get; set; }

        [Category("1. CONFIG")]
        [DisplayName("Camp x.3 Nodes?")]
        public bool CampRareNodes { get; set; }

        [Category("1. CONFIG")]
        [DisplayName("Action Go To Gather Area")]
        public bool GoToGatherArea { get; set; }

        [Category("1. CONFIG")]
        [DisplayName("Debugger")]
        public bool SDebugger { get; set; }

        [Category("2. WAYPOINTS")]
        [DisplayName("(Gather area)")]
        public string PathWayPoint { get; set; }

        [Category("2. WAYPOINTS")]
        [DisplayName("(Gather area > City)")]
        public string PathWayPointCity { get; set; }

        [Category("2. WAYPOINTS")]
        [DisplayName("(City > Gather area)")]
        public string PathWayPointGatherArea { get; set; }

        //WOOD
        [Category("3. WOOD")]
        [DisplayName("Tier")]
        public string _woodharvestTier
        {
            get { return string.Join("; ", WoodHarvestTier.Select(i => i.ToString()).ToArray()); }
            set { WoodHarvestTier = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); }
        }

        [Category("3. WOOD")]
        [DisplayName("Collect")]
        public bool WoodHarvestCondition { get; set; }

        //ROCK
        [Category("4. ROCK")]
        [DisplayName("Tier")]
        public string _rockharvestTier
        {
            get { return string.Join("; ", RockHarvestTier.Select(i => i.ToString()).ToArray()); }
            set { RockHarvestTier = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); }
        }

        [Category("4. ROCK")]
        [DisplayName("Collect")]
        public bool RockHarvestCondition { get; set; }

        //ORE
        [Category("5. ORE")]
        [DisplayName("Tier")]
        public string _oreharvestTier
        {
            get { return string.Join("; ", OreHarvestTier.Select(i => i.ToString()).ToArray()); }
            set { OreHarvestTier = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); }
        }

        [Category("5. ORE")]
        [DisplayName("Collect")]
        public bool OreHarvestCondition { get; set; }

        //FIBER
        [Category("6. FIBER")]
        [DisplayName("Tier")]
        public string _fiberharvestTier
        {
            get { return string.Join("; ", FiberHarvestTier.Select(i => i.ToString()).ToArray()); }
            set { FiberHarvestTier = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); }
        }

        [Category("6. FIBER")]
        [DisplayName("Collect")]
        public bool FiberHarvestCondition { get; set; }

        //HIDE
        [Category("7. HIDE")]
        [DisplayName("Tier")]
        public string _hideharvestTier
        {
            get { return string.Join("; ", HideHarvestTier.Select(i => i.ToString()).ToArray()); }
            set { HideHarvestTier = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); }
        }

        [Category("7. HIDE")]
        [DisplayName("Collect")]
        public bool HideHarvestCondition { get; set; }

        [Category("7. HIDE")]
        [DisplayName("Hunt")]
        public bool HideHuntCondition { get; set; }

        public class PathData
        {
            public Vector3[] Path { get; set; }
        }

        public class PathData2
        {
            public String[] Path { get; set; }
        }

        public List<Vector3> GetWaypoints(string WPFile)
        {
            List<Vector3> ret;

            try
            {
                ret = new List<Vector3>(FromXml<PathData>(File.ReadAllText(Constants.WaypointPath + WPFile)).Path);
            }
            catch(Exception ex)
            {
                Log.Debug(ex.Message);
                ret = null;
            }
            return ret;
        }

        public List<String> GetWaypointsMap(string WPFile)
        {
            List<String> ret;

            try
            {
                ret = new List<String>(FromXml<PathData2>(File.ReadAllText(Constants.WaypointPath + "Map_" + WPFile)).Path);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                ret = null;
            }
            return ret;
        }

        public static bool ValidateMap(List<Vector3> a, List<string> b)
        {
            return (a.Count == b.Count);
        }


        public static T FromXml<T>(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(data))
            {
                object obj = s.Deserialize(reader);
                return (T)obj;
            }
        }
        


    }
}
