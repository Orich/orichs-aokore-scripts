﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using AOKore.Concurrent;
using AOKore.Game;
using AOKore.Script;
using System.ComponentModel;

namespace Script
{

    /*
    public class Configuration
    {
        [Category("1. CONFIG")]
        [DisplayName("File Waypoint(DG)")]
        public string PathWayPoint { get; set; }

    }
    */

    public static class GameState
    {
        public static MobInstance currMob { get; set; }

        public static bool Valid
        {
            get
            {
                bool ret = true;

                ConcurrentTaskManager.GameRun(() =>
                {
                    if (!Client.ClientState.Contains("Connected") ||
                        !Player.HasPlayer || Player.IsPlayerInCity ||
                        Player.IsDead)
                        ret = false;
                });

                return ret;
            }
            set
            {
                return;
            }
        }

        public static bool IsAttacked
        {
            get
            {
                bool ret = false;

                ConcurrentTaskManager.GameRun(() =>
                {
                    var Hostile = Mob.Mobs
                    .Where(i => i.TargetId == Player.ID)
                    .Where(i => !i.IsDead)
                    .FirstOrDefault();

                    if (Hostile != null)
                        ret = true;

                    currMob = Hostile;
                });

                Thread.Sleep(10);

                return ret;
            }
            set
            {
                return;
            }
        }

        public static bool IsMounted
        {
            get
            {
                bool ret = false;

                ConcurrentTaskManager.GameRun(() =>
                {
                    ret = Mount.IsMounted;
                });

                return ret;
            }
            set
            {

            }
        }

        public static void Dismount()
        {
            ConcurrentTaskManager.GameRun(() =>
            {
                if (Mount.IsMounted)
                {
                    Mount.MountDismount();
                }

            });
        }

    }


    public class Program
    {
        const string ScriptName = "Aggro Handler";

        public static void Main()
        {
            ClearLog();

            ConcurrentTaskManager.CreateService(() => // Main Loop
            {

                if (!GameState.Valid)
                    return;


                if (GameState.IsAttacked)
                {
                    GameState.Dismount();
                    Thread.Sleep(150);

                    if(!GameState.currMob.IsDead)
                        oAttack(GameState.currMob);
                    Thread.Sleep(150);
                    
                }


                // End Main Loop
            }, ScriptName, 1500);
        }

        public static void oAttack(MobInstance mob)
        {
            ConcurrentTaskManager.GameRun(() =>
            {
                if (Player.IsCasting || Player.IsChanneling)
                {
                    Log("Currently casting.  skipping.");
                    return;
                }

                if (Spell.CanCast(4))
                {
                    Log("Casting 4");
                    Spell.CastSlot(4);
                    return;
                }

                if (Spell.CanCast(2) && Player.Location.DistanceXZ(mob.Location) <= Spell.Range(2))
                {
                    Log("Casting 2");
                    Spell.CastSlotXZ(2, mob.Location.X, mob.Location.Z);
                    return;
                }

                if (Spell.CanCast(0) && Player.Location.DistanceXZ(mob.Location) <= Spell.Range(0))
                {
                    Log("Casting 0");
                    Spell.CastSlotXZ(0, mob.Location.X, mob.Location.Z);
                    return;
                }

                if (!Player.IsCasting && !Player.IsChanneling)
                {
                    Log("Auto attacking");
                    Player.DefaultAttack();
                    return;
                }
            });
        }





        public static void Log(string message)
        {
            Output.Log(message, ScriptName);
        }
        public static void ClearLog()
        {
            Output.ClearLog(ScriptName);
        }
    }
}
